---
title: Xing Shi Cai's home on the Internet
---

Hello! My name is 蔡醒诗 or Xing Shi Cai in English.
(See [here](https://translate.google.com/?sl=zh-CN&tl=en&text=%E8%94%A1%E9%86%92%E8%AF%97&op=translate) if you want to know how to say my name.)
Welcome to my new home on the Internet! (Here is my [old home](https://newptcai.github.io).)

## About me

{{<rawhtml>}}
<img src="/assets/authors/xing-shi-cai.jpg" class="heading"></img>
{{</rawhtml>}}

I am a mathematician/computer scientist who is interested in probability, combinatorics,
experimental mathematics and programming.
In recent years I also got interested in applying AI to research and teaching.

Recently, I moved Kunshan, China for a position of 
Assistant Professor of Mathematics at [Duke Kunshan University](https://dukekunshan.edu.cn/).

I am a [vegan](https://newptcai.github.io/what-have-i-done-in-2020-part-1-becoming-a-vegan.html),
committed to a plant-based lifestyle,
and I regularly donate to [effective charities](https://newptcai.github.io/what-have-i-done-in-2020-part-3-donating-to-charities.html).
I'm also deeply concerned about
[climate change](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html).
These values have led me to serve as an advisor for
[Plant Futures DKU](https://dku-plant-futures.github.io/),
a student club [dedicated to promoting plant-based diets and products](https://dku-plant-futures.github.io/about/).

In my spare time, I like to read. 
Some of my favourite authors are
[Epictetus](https://en.wikipedia.org/wiki/Epictetus),
[Marcus Aurelius](https://en.wikipedia.org/wiki/Marcus_Aurelius),
[Seneca](https://en.wikipedia.org/wiki/Seneca_the_Younger), 
[Peter Singer](https://en.wikipedia.org/wiki/Peter_Singer),
[Massimo Pigliucci](https://en.wikipedia.org/wiki/Massimo_Pigliucci), 
[Mary Oliver](https://en.wikipedia.org/wiki/Mary_Oliver),
and [Stephen Batchelor](https://en.wikipedia.org/wiki/Stephen_Batchelor_\(author\)).
I write a [blog]({{< ref "/blog/" >}}) (mostly) about [books]({{< ref path="/tags/book/" >}}) which I like.

## Contact

You can find me via

* Email 
    * Personal: [xingshi.cai@tutanota.com](mailto:xingshi.cai@tutanota.com)
    * University: [xingshi.cai@dukekunshan.edu.cn](mailto:xingshi.cai@dukekunshan.edu.cn)
* Phone 
    * China: [+86-512-30657370](tel:+86-512-30657370)
    * Canada: [+1-514-251-2855](tel:+1-514-251-2855)

## Office

Office Address: Academic Building 3221 

How to find it:

{{<figure src="/assets/images/route-to-office/campus.png" caption="Location of my office">}}

{{<figure src="/assets/images/route-to-office/01.jpg" caption="Take the elevator at south most side of the building to the third floor.">}}

{{<figure src="/assets/images/route-to-office/04.jpg" caption="Turn right after you get off the elevator.">}}

{{<figure src="/assets/images/route-to-office/02.jpg" caption="Go through this door, turn left and go through some office cubicles.">}}

{{<figure src="/assets/images/route-to-office/03.jpg" caption="Turn right when you reach the glass windows of the building.">}}
 
## A short biography

* March 15, 2021 -- Started working for Duke Kunshan University.
* Feb 1, 2021 -- Moved back to Montreal.
* Dec 31, 2020 -- Finished my postdoctoral position at [the Department of Mathematics of Uppsala
  University](https://www.uu.se/en) in Sweden, with [Cecilia
  Holmgren](http://katalog.uu.se/profile/?id=N5-824) as my supervisor.
* Oct 2016 -- Got my PhD at [McGill University](http://mcgill.ca) with [Luc Devroye](http://luc.devroye.org/) as my supervisor. 
* May 2010 -- Moved to Canada from China.
* Before Canada, I had been a software engineer  in China for a long time.
* My undergraduate years was spent for a Computer Science  degree at [Xi'an Jiaotong
  University](http://www.xjtu.edu.cn/en/) ([西安交通大学](http://www.xjtu.edu.cn/)) in
  [Xi'an](https://en.wikipedia.org/wiki/Xi%27an) (西安) China.
* I grew up in [Sichuan province](http://en.wikipedia.org/wiki/Sichuan) (四川), which is famous for
  its spicy [Szechuan cuisine](http://en.wikipedia.org/wiki/Szechuan_cuisine)  and [panda
  bears](https://en.wikipedia.org/wiki/Sichuan_Giant_Panda_Sanctuaries).
* For more information, check my [cv](assets/doc/mycv.pdf).
