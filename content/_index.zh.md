---
title: 蔡醒诗的网络家园
---

你好！我的名字是蔡醒诗，英文名为Xing Shi Cai。
（如果你想知道如何念我的名字，可以点击[这里](https://translate.google.com/?sl=zh-CN&tl=en&text=%E8%94%A1%E9%86%92%E8%AF%97&op=translate)）
欢迎来到我的新网络家园！ （这是我的[旧家园](https://newptcai.github.io)。）

## 关于我

{{<rawhtml>}}
<img src="/assets/authors/xing-shi-cai.jpg" class="heading"></img>
{{</rawhtml>}}

我是一位数学家/计算机科学家，兴趣涵盖概率论、组合学、实验数学和编程。
在最近几年中，我也开始对AI在科研和教学中的应用感兴趣。

最近，我搬到了中国昆山，成为[昆山杜克大学](https://dukekunshan.edu.cn/)的数学助理教授。

我是一名[素食者](https://newptcai.github.io/what-have-i-done-in-2020-part-1-becoming-a-vegan.html)，
致力于植物性饮食生活方式，
并且我定期向[有效慈善组织](https://newptcai.github.io/what-have-i-done-in-2020-part-3-donating-to-charities.html)捐款。
我还深切关注[气候变化](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html)。
这些价值促使我担任了[昆山杜克大学植物未来社](https://dku-plant-futures.github.io/)的顾问。
这是一个致力于[推广植物性饮食和产品的学生俱乐部](https://dku-plant-futures.github.io/about/)。

闲暇时，我喜欢读书。
我最喜欢的一些作者包括：
[爱比克泰德](https://en.wikipedia.org/wiki/Epictetus),
[马可·奥勒留](https://en.wikipedia.org/wiki/Marcus_Aurelius),
[塞内加](https://en.wikipedia.org/wiki/Seneca_the_Younger),
[彼得·辛格](https://en.wikipedia.org/wiki/Peter_Singer),
[马西莫·皮利乌奇](https://en.wikipedia.org/wiki/Massimo_Pigliucci),
[玛丽·奥利弗](https://en.wikipedia.org/wiki/Mary_Oliver),
和[斯蒂芬·巴切勒](https://en.wikipedia.org/wiki/Stephen_Batchelor_\(author\))。
我写了一个[博客]({{< ref "/blog/" >}})，
大部分是关于我喜欢的[书籍]({{< ref path="/tags/book" lang="en">}})的内容。

## 联系方式

你可以通过以下方式找到我

* 邮箱
    * 个人邮箱: [xingshi.cai@tutanota.com](mailto:xingshi.cai@tutanota.com)
    * 学校邮箱: [xingshi.cai@dukekunshan.edu.cn](mailto:xingshi.cai@dukekunshan.edu.cn)
* 电话
    * 中国: [+86-512-30657370](tel:+86-512-30657370)
    * 加拿大: [+1-514-251-2855](tel:+1-514-251-2855)

## 办公室

办公室地址：学术大楼3221室

如何找到：

{{<figure src="/assets/images/route-to-office/campus.png" caption="我的办公室位置">}}

{{<figure src="/assets/images/route-to-office/01.jpg" caption="在大楼最南侧乘电梯到三楼。">}}

{{<figure src="/assets/images/route-to-office/04.jpg" caption="下电梯后向右转。">}}

{{<figure src="/assets/images/route-to-office/02.jpg" caption="穿过此门，左转并穿过一些办公隔间。">}}

{{<figure src="/assets/images/route-to-office/03.jpg" caption="到达建筑物玻璃窗后再向右转。">}}

## 简短简介

* 2021年3月15日 -- 开始在昆山杜克大学工作。
* 2021年2月1日 -- 搬回蒙特利尔。
* 2020年12月31日 -- 结束了在瑞典[乌普萨拉大学数学系](https://www.uu.se/en)的博士后工作，导师为[Cecilia Holmgren](http://katalog.uu.se/profile/?id=N5-824)。
* 2016年10月 -- 在[麦吉尔大学](http://mcgill.ca)获得博士学位，导师为[Luc Devroye](http://luc.devroye.org/)。
* 2010年5月 -- 从中国搬到加拿大。
* 在加拿大之前，我在中国做了很长时间的软件工程师。
* 本科在[西安交通大学](http://www.xjtu.edu.cn/) ([西安交通大学](http://www.xjtu.edu.cn/)) 攻读计算机科学学位，位于[西安](https://en.wikipedia.org/wiki/Xi%27an)（西安）中国。
* 我成长于[四川省](http://en.wikipedia.org/wiki/Sichuan)（四川），那里以辛辣的[川菜](http://en.wikipedia.org/wiki/Szechuan_cuisine)和[熊猫](https://en.wikipedia.org/wiki/Sichuan_Giant_Panda_Sanctuaries)而闻名。
* 欲了解更多信息，请查看我的[简历](assets/doc/mycv.pdf)。
