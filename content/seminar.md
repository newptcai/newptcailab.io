---
ShowToc: true
date: "2021-10-03"
maxtoclevel: 2
tags:
- research
- mathematics
- seminar
- women-in-math-cs
title: Women in Math and Computer Science Seminar
---

## About the seminar

The seminar has three purposes --

* Introduce research areas in mathematics and computer science to students of DKU (Duke Kunshan University) who would like to study in graduate school.
* Give student a sense of what is like to purse a PhD degree in a foreign country.
* Encourage female students to consider studying math and computer science.

The seminar is designed to be accessible to students with only basic college
level mathematics and computer science knowledge.

## \#4 -- What is Model Risk and why is it important?

![poster 4](/assets/images/seminar/woman-in-math-cs-talk-04.jpg)

* Date: Fri 22/04/2022
* Time: 09:30-10:15 (Beijing Time)
* Zoom: [914 4849 6103](https://duke.zoom.us/j/91448496103)

### The speaker

Dr. Elena Rivera-Mancia holds a PhD in Mathematics and Statistics from McGill University.
She has more than 11 years of experience in the financial sector, 
where she has held different positions, 
in both the public and private sector, 
in Mexico and Canada. 
She is currently a VP Model Validation with [BNP Paribas](https://www.bnpparibas.ca/en/)

In addition to her professional experience,
Elena has worked as a lecturer at the 
National Autonomous University of Mexico and McGill University.
She has also participated as a speaker in numerous international conferences.
Her main areas of interest include risk management,
extreme value theory and applications of Bayesian methods in the financial sector.

### The talk

The use of models in decision-making has significantly increased in
recent years, bringing multiple benefits. However, errors within
models or the incorrect use of models can have a significant impact
on businesses, resulting in severe financial
and/or reputational damage.
As many institutions are highly reliant on models for their daily
activities, the development of solid model risk management
frameworks has become crucial to handle model risk. Some of the
new challenges include the growing use of artificial intelligence
models and regulatory changes.
In this talk, we will discuss the main aspects of model risk, as well
as some new perspectives to overcome its challenges in a changing
world.


## \#3 -- When ducks and rabbits are the same

![poster 3](/assets/images/seminar/woman-in-math-cs-talk-03.jpg)

* Date: Fri 8/04/2022
* Time: 09:00-09:45 (Beijing Time)
* Zoom: [914 4849 6103](https://duke.zoom.us/j/91448496103)

### The speaker

Professor [Laura Eslava](http://sigma.iimas.unam.mx/laura/) is a
member of the Probability and Statistics department of IIMAS-UNAM (Mexico City).

She obtained her bachelor degree, with honors, from FC,UNAM in Mexico City under the
supervision of Gerónimo Uribe.
Both her Master (2012) and PhD (2017) thesis were supervised by Louigi Addario-Berry
at McGill University (Montreal).

From 2017-2019 she worked with Lutz Warnke in the School of Mathematics in Georgia
Tech (Atlanta).

Her research interests are Random Structures and Probabilistic Combinatorics; in
particular, random networks. 

### The talk

What happens when both the essence of a 🦆 and a 🐰 are exactly the same? Can
we ask the 🦆 questions to learn about the 🐰?
Bijections between equivalent combinatorial classes help us better understand the
structure of each class and both Catalan and factorial numbers are filled with
🦆-🐰 examples.
Learning to recognize the 🐰 within a 🦆 allows us for translations and
simplifications where combinatorial classes are involved. 

## \#2 -- When Alice Talks with Bob

![poster 2](/assets/images/seminar/woman-in-math-cs-talk-02.jpg)

* Date: Fri 18/02/2022
* Time: 09:00-09:45 (Beijing Time)
* Zoom: [914 4849 6103](https://duke.zoom.us/j/91448496103)

### The speaker

Dr. [Lianna Hambardzumyan](https://www.cs.mcgill.ca/~lhamba/) (McGill University) is a lecturer at McGill University, Montreal, Canada. She got her PhD
degree in Fall, 2021 from McGill University.

Her research interests include complexity theory, analysis of Boolean functions, and
combinatorics.

### The talk

Lianna will talk about her research in the field of communication complexity.
Communication complexity studies how much two people have to “talk” to each other to
compute a common function. This turns out to be a bottleneck question for problems in
VLSI design, auction theory, circuit complexity, linear programming,
pseudo-randomness, data structures, distributed computing and more.
Lastly, Lianna will share her experiences as a PhD student in Canada and will be able
to give advice about choosing PhD as a next step. 


## \#1 -- What is Graph Theory?

![poster 1](/assets/images/seminar/woman-in-math-cs-talk-01.png)

### The speaker

[Dr. Yelena Yuditsky](https://sites.google.com/view/yuditsky/home) (Université libre de Bruxelles) is a postdoctoral researcher in the Department of Mathematics at
Université libre de Bruxelles with Samuel Fiorini and Gwenaël Joret. Before
arriving in Brussels, she was a postdoctoral researcher at  Ben-Gurion University
of the Negev and Karlsruhe institute of technology.

Dr. Yuditsky did her PhD at McGill University under the supervision of Bruce Reed and Sergey Norin.

Her research interests are in extremal combinatorics and structural graph theory.
She is also interested in discrete geometry.

### The talk

In this talk, Dr. Yuditsky will present some of her research in graph theory. 
Graph is an abstract way to describe a network and can be easily found in the real world, for example, social networks, road maps, the internet and much more. 

Dr. Yuditsky will also talk about her experiences as a PhD student in Canada. 
Dr. Yuditsky will try to share as much advice as possible regarding doing a PhD. 
