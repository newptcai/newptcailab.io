---
date: "2021-02-11"
tags:
- climate-crisis
title: Resources On Climate Change
---

Over the Christmas holiday of 2020, I tried to bring myself up to speed on climate change. What I
have learned made me [gravely concerned](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html).
It was much much worse than I had thought. 
In fact, one reason that I decided to go back to work in China is
to avoid flying every year to visit my family.

This page lists resources which helped me to realize how serious the situation is. 
Hopefully it can also wake you up.

## Books

If you only have time to read one book about climate change, then you should read [Our Final
Warning: Six Degrees of Climate Emergency by Mark
Lynas](https://www.goodreads.com/book/show/51471435-our-final-warning). It describes in chilling
details the different scenarios given different levels of warming and why our action today is
crucial for the continuation of humanity.

![Our Final Warning](/assets/images/what-have-i-done-2020/final.jpg)

Other good books include

* [The Heat Will Kill You First: Life and Death on a Scorched Planet](https://www.goodreads.com/book/show/63251778-the-heat-will-kill-you-first)
* [The Uninhabitable Earth: Life After Warming](https://www.goodreads.com/book/show/41552709-the-uninhabitable-earth)
* [The Weather Makers: How Man Is Changing the Climate and What It Means for Life on Earth](https://www.goodreads.com/book/show/48463.The_Weather_Makers)
* [No One Is Too Small to Make a Difference](https://www.goodreads.com/book/show/51764686-no-one-is-too-small-to-make-a-difference)
* [The Sixth Extinction: An Unnatural History](https://www.goodreads.com/book/show/17910054-the-sixth-extinction)

## Documentary Films

My new favourite documentary film on climate change is 
[Greta Thunberg: A Year to Change the World](https://en.wikipedia.org/wiki/Greta_Thunberg:_A_Year_to_Change_the_World).

* [BBC -- Climate Change -- The Facts](https://www.bbc.co.uk/programmes/m00049b1)
* [BBC -- Extinction -- The Facts](https://www.bbc.co.uk/programmes/m000mn4n)
* [BBC -- Horizon -- 7.7 Billion People and Counting](https://www.reddit.com/r/overpopulation/comments/eshih9/bbc_two_horizon_2020_chris_packham_77_billion/)
* [Netflix -- David Attenborough: A Life on Our Planet](https://www.imdb.com/title/tt11989890/)
* [Endgame 2050](https://www.endgame2050.com/)
* [BBC -- Horizon -- Should I Eat Meat? - How to Feed the Planet](https://www.bbc.co.uk/programmes/b04fhbrt)
* [Cowspiracy: The Sustainability Secret](https://www.cowspiracy.com/)

## Podcasts

* [Climate One Podcast](https://www.climateone.org/watch-and-listen/podcasts)
* [GIMLET -- How to Save a Planet](https://gimletmedia.com/shows/howtosaveaplanet)
* [BBC -- The Climate Question](https://www.bbc.co.uk/programmes/w13xtvb6)

## Websites

* [SkepticalScience](https://skepticalscience.com/argument.php?f=taxonomy) -- Learn the scientific evidences of climate change.
* If you want to do something about climate change, check [GivingGreen](https://www.givinggreen.earth), [this](https://www.vox.com/future-perfect/2019/12/2/20976180/climate-change-best-charities-effective-philanthropy) and [this](https://www.theatlantic.com/science/archive/2020/12/how-to-donate-to-fight-climate-change-effectively/617248/).
