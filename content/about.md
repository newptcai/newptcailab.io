---
title: About this website
---

The name of the website comes from a quote
by [Marcus Aurelius](https://en.wikipedia.org/wiki/Marcus_Aurelius) --

> Throwing away then all things,
> hold to these only which are few
> and besides bear in mind
> that every man lives only this present time,
> which is an indivisible point,
> and that all the rest of
> his life is either past or it is uncertain.
> [Meditations, III.10.]
