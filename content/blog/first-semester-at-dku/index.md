---
pubdate: "2022-08-18"
tags:
- advice
- Duke-Kunshan-University
title: Welcome to DKU!
---


## DKU bulletin: Embark on an Exciting Journey!

[Download the DKU (Duke Kunshan University)
bulletin](https://ugstudies.dukekunshan.edu.cn/academics/) and explore your path
ahead. Delve into *Part 3: The Curriculum* and *Part 6: Academic Procedures and
Information*. Don't miss *Part 10: Majors and Courses* as it may spark
inspiration for your future passions.

## Course Selections: Your Path to Success

Your first term is filled with opportunities:
- In the first 7-week session, you can embrace **up to 8 credits** including an
engaging four-credit course, an exciting language course, and a PE course to
keep you energized!
- In the subsequent 7-week session, you can go beyond with **up to 10 credits**.

Consider these guidelines to make the most of it:
1. **A Four-Credit Course:** Choose something welcoming like a foundational
   course, or a core subject.
2. **A Two-Credit Course:** Discover something beyond your main interest to
   broaden your mind.

Remember, it's your first semester! Embrace the joy of learning without
overwhelming yourself.

## Your Advising Meeting: We're Here for You

When it's time to register for each semester, we'll schedule a meeting to
explore the courses that pique your interest. Bring your thoughts and dreams;
let's build a plan together.

Have questions, concerns, or just need to chat? Feel free to contact me and set
up a meeting anytime. Your success and well-being are my priority, and I'm
thrilled to support you on this incredible journey!
