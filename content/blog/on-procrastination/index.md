---
pubdate: "2021-12-30"
tags:
- health
- advice
- book
title: On Procrastination
---


I procrastinate, a lot.
And when I think about all the things I could have done 
had I not procrastinated so much, 
e.g., all the papers I could have written,
I am really regretful :cry:.

One of the books which I found very helpful is [How to Beat Procrastination](https://www.goodreads.com/book/show/36664616-stop-procrastinating) by Nils Salzgeber. 
Here are some tips from the book and some of my own thoughts.
Hope it help you to not to have the same regret which I have.

## Just Get Started

While some people can think about difficult tasks with no problems,
procrastinators think about certain tasks and immediately start feeling bad.

Research shows that there's an easy way to get rid of the pain associated
with certain tasks: Just. Get. Started.
As soon as you start engaging in a task, the pain evaporates.
Once you get started, you realize it's not nearly as bad as you thought.

Every time you overcome the motivational surface tension
and move from non-doing to doing,
you get better at it.
Every time you manage to get started on difficult tasks,
you build up that muscle of bursting through resistance
and doing what needs to get done whether you feel like it or not.

## How to Get Started

First, create a list of all the things you'll need to get done.
Second, create a plan --
which tasks are you tackling first
and in which order?
Third, stop worrying about the steps further down the list
and start focusing only on the very next step.
Fourth, just get started on that very next step.

## Take Small Steps

Aim to meditate for one minute a day, not 20 minutes.
Aim to go for a walk a few times a week, not exercise daily.
Aim to do five pushups, not a one-hour full body workout.
Aim to work on writing your Kindle book for 20 minutes,
not a couple of hours.

## Get Rid of Distractions

We live in *The Age of Dramatic Distraction*.
Many shiny toys to chase every waking moment
yet so few of those pursuits create real value
and grow a life brilliantly lived.

*Proximity to temptation* is one of the deadliest determinants of procrastination.
If we want to procrastinate less,
we need to make distractions less readily accessible.
We need to either completely eliminate a distraction,
or complicate its access.

Declutter your browser. 
Every visible hotlink or bookmark can trigger unwanted goals 
that distract you from doing what needs to get done. 
Your browser should be as empty as possible. No bookmark bar. 
No other visible bookmarks. No visible hotlinks. 
No website suggestions when you open a new tab. 
Your browser should not have any visible triggers at all.

Put that phone away. 
Turn it off completely, or at least put it in airplane mode 
(remember, the harder you make distractions to access, 
the less you'll procrastinate). 
And please put your phone out of sight somehow.

## My Own Two Cents

I found that blocking certain websites very helpful.
If you use Firefox, try [LeechBlock NG](https://addons.mozilla.org/en-US/firefox/addon/leechblock-ng/).
For Google Chrome, you can use [Web Blocker](https://chrome.google.com/webstore/detail/website-blocker-beta/hclgegipaehbigmbhdpfapmjadbaldib).
iPhone/iPad has a function [Screen Time](https://support.apple.com/en-ca/guide/iphone/iphbfa595995/ios)
which allows you to limit your access to them.
For Android, there is [Digital Wellbeing](https://support.google.com/android/answer/9346420?hl=en).

:bomb: But *non* of these technologies can stop us from wasting time,
because we can always *turn them off*.
In fact, nothing external, like a book, bad consequences, pressure from our mums, etc.,
can stop us from procrastination.
(May be for the exception of pistol pointing at our heads.)
What technologies offer, 
is a little bit delay between 
having the impulse of spending another hour in a way which we will regret
and actually wasting that hour.
That may give us a fighting chance to 
remember what we really want to do with our lives.
