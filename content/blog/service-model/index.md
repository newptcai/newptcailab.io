---
images:
- robot-valet-in-doomed-world.webp
pubdate: "2025-02-15"
tags:
- book
- literature
- science-fiction
title: "Service Model by Adrian Tchaikovsky (2024)"
---

{{<figure 
    src="robot-valet-in-doomed-world.webp"
    caption="A robot valet wondering in doomed world" 
    class="small"
>}}

Adrian Tchaikovsky, the science fiction writer, is a master at crafting bleak,
hellish future worlds.
But in *Service Model*,
a 2024 science-fiction satire telling the story of a robot valet
wandering in a doomed world,
he has truly outdone himself,
conjuring an absurd realm where human societies have crumbled,
and humanity teeters on the brink of extinction.

Now, that scenario isn't entirely novel.
But what renders the book both tear-inducing and hilarious,
is the presence in this world of numerous sophisticated robots,
designed to eliminate the slightest discomfort from human existence.
Yet, they adhere so strictly to their programmed rules,
that it only leads to endless absurdities,
and meaningless ordeals for both robots and humans alike.

Science fiction writers,
effective altruists,
and Silicon Valley billionaires have long cautioned,
that the rise of sentient,
super-human artificial intelligence might herald the downfall of our own species.
However, Tchaikovsky suggests a different,
perhaps more mundane, and even more depressing scenario.
He proposes that precisely because robots,
no matter how advanced,
lack free will,
and cannot exercise their own volition in decision-making,
they will not only fail to rescue us from impending environmental,
political, and economic crises,
but they will also be incapable of replacing us,
by creating a better world of their own.

And, I believe, that is Tchaikovsky's final warning to humanity.
I hope that future historians,
if they still exist
--- since there aren't any left in *Service Model*
--- will regard him as a mere novelist,
one who tries to capitalise on the general unease
concerning advancements in artificial intelligence.
Yet, I fear he may indeed be onto something.
