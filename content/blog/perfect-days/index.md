---
ShowToc: true
image:
- perfect-days-poster.jpg
pubdate: "2024-12-23"
tags:
- film
title: "What is a Perfect Day for You?"
---

{{<
    figure
    src="perfect-days-poster.jpg"
    caption="A poster for the film Perfect Days"
>}}

On some dating websites,
you might find yourself confronting that deceptively profound question:
"What is a perfect day for you?"
Like most seemingly simple queries about the human condition,
its answers are as numerous and varied as the stars.

For Ayelet Waldman, author of [*A Really Good Day*](https://www.goodreads.com/book/show/30212082-a-really-good-day),
a perfect day arrives through carefully measured doses of LSD —
a chemical key to unlock the door to contentment.
In contrast, the 2023 film [*Perfect Days*](https://en.wikipedia.org/wiki/Perfect_Days), directed by Wim Wenders and starring Kōji Yakusho,
perfection whispers in a different language:
it speaks through the sacred ordinary of each passing day.

The film follows Hirayama,
a public toilet cleaner in Tokyo whose life moves with the precise poetry of ritual.
He rises with the sun, nurtures his plants with tender attention,
retrieves his daily coffee from a faithful vending machine,
drives through Tokyo's streets while cassette tapes recall their musical memories,
cleanses public toilets with monk-like dedication,
breaks bread in his chosen park,
and drifts to sleep with literature as his lullaby.

Hirayama greets each sunrise with the same gentle smile.
He approaches repetition not as a prison, but as a prayer.
There's no shadow of tedium in his routine, only illumination.
Every gesture —
the careful angling of a mirror beneath a toilet bowl to check its cleanliness,
the mindful savouring of lunch beneath his "tree-friend",
the measured sips of ice water at the same modest food stand —
radiates with the quiet glow of contentment.

The effect is hypnotic, almost meditative.

Just when the rhythm of Hirayama's days has lulled you into a contemplative trance,
life does what life invariably does: it changes.
The ripples come in both gentle and challenging waves.
Hirayama meets these moments with the steady grace of a tree bending in the wind,
his tranquil presence touching those who enter his orbit.
Then, as if to underscore life's perpetual motion,
he rises again to drive through Tokyo's streets,
while Leslie Bricusse and Anthony Newley's *Feeling Good* fills his small truck:

```
Birds flying high, you know how I feel
Sun in the sky, you know how I feel
Breeze driftin' on by, you know how I feel
It's a new dawn, it's a new day, it's a new life for me
```

He listens with such pure presence that tears gather in his eyes.
And in that moment, my own eyes mirror his.

{{<
    figure
    src="feeling-good.jpg"
    caption="Hirayama listening to *Feeling Good*"
>}}

If this humble custodian of public spaces
can discover perfection in life's steady rhythms,
what stops the rest of us?
The recipe seems remarkably simple —
a willingness to live quietly, to notice deeply, to embrace what exists before us.

By some cosmic serendipity, on the day I watched the film,
I was reading Thich Nhat Hanh's [*No Mud, No Lotus*](https://www.goodreads.com/book/show/20949716-no-mud-no-lotus).
His words resonate:

> The main affliction of our modern civilization is that we don't know how to
> handle the suffering inside us, and we try to cover it up with all kinds of
> consumption. Retailers peddle a plethora of classic and novel devices to help
> us cover up the suffering inside.

Hirayama knows suffering too, but he doesn't seek refuge in distraction.
He doesn't anaesthetize himself with screens or retail therapy.
He simply continues, one moment flowing into the next.
And perhaps therein lies his secret to finding perfection in every day.

*Perfect Days* evokes the same meditative beauty I found in films like 
[*Drive My Car*](https://en.wikipedia.org/wiki/Drive_My_Car_(film)),
[*Paterson*](https://en.wikipedia.org/wiki/Paterson_(film)),
and [*Columbus*](https://en.wikipedia.org/wiki/Columbus_(2017_film)).
It arrives like an unexpected gift —
another perfect (or as close as our imperfect world allows) film.

For a glimpse into the song that moved Hirayama, here's "Feeling Good" performed
by Anthony Newley:

{{< youtube aYF__H4PSUA >}}
