---
pubdate: "2021-07-18"
tags:
- philosophy
- book
- virtue-ethics
title: On Virtue Ethics by Rosalind Hursthouse -- Chapter 09 Naturalism
---


A summary of Chapter 09 Naturalism.

----

This chapter is about the 2nd Plato's requirements of 
virtue in the [last chapter]({{< ref "/blog/on-virtue-ethics-08/" >}}) --

> The virtues make their possessor a good human being. 

Virtue ethics is a type of ethical naturalism, i.e., 
basing ethics on considerations of human nature,
or on what is involved in being a good human.

The objective of such an approach is that an account of human nature 
may be too broad for making moral judgement,
or too strong to the extend that it is just a restatement of our ethics.
Gary Watson asks

> Can an objective theory really establish that being a gangster is incompatible with
> being a good human being?

But first, what is an objective theory of human nature?

An Aristotelian naturalism is not an argument from "neutral point of view" 
and does not seek to convince anyone with a very different ethical outlook.
But it may still serve as the starting point for validate what we believe is virtue.

Philippa Foot points out that 
we can call something good by whatever standard we want,
but we say something is a good X,
we must mean it is an instance of X with very specific traits.
She uses plants and animals as examples.
A dead cactus can not be called a good cactus.
A free-riding wolf can not be called a good wolf.

## Evaluating Plants and Animals

In naturalism, we evaluate plants and animals as specimens of their kind.

A plant is a good specimen according to if its 
1. parts 
2. operations (reactions)
with respect to two ends
1. the survival of itself 
2. the continuance of its species

For animals, two more aspects
3. actions
4. emotion and desires
and two more ends are considered
3. characteristic freedom from pain and characteristic pleasure.
4. the good functioning of a social group

The evaluation of living things according to these criteria is objective,
in the sense that botany and zoology are science.
But such evolutions are only true for the most part, and they are imprecise.
Also note that good *X*s are not always the same
and may need to be evaluated in subspecies.
The evaluations identifies if *X* live well in a characteristic *X*'s way.

## Evaluating Ourselves

The criteria of goodness in human must be related to what human beings are and do.

The difference in ethical evaluation and that if *X* is a healthy specimen of its
kind is that we leave the physical part to biology and medicine.
What is left is that if we are ethically good human depends on
if we are well-endowed with respect to reactions that are not merely physical,
our actions from inclinations,
our emotions and desires, and our action from reason.

Does this list look plausible in validating our standard list of virtues?
Certainly they do. 
Courage is necessary in defending oneself and one's children.
Charity helps a group survive better.
Without honesty we would not be able to cooperate and pass knowledge to the
next generation.

Doest naturalism requires all good humans are alike?
But these naturalist criteria is meant to judge if a character trait is virtue,
not if a particular action is good.
Determine what is virtue is one thing.
Determine how to apply a virtue in each case is an entirely different matter.
And being able to manifest one virtue may lead to weakness in other virtues.

There are exceptions that a good human may not have all virtues to at least some
minimal degree.
We may say that being a good parent is a virtue distinctive from the standard ones.
But for people without children, lacking this virtue does not necessarily make them bad humans.

Does it follow from the naturalist view that being a monastic contemplative
is incompatible with being a good human, since they do not reproduce.
Not necessarily. Virtue and vice are character traits.
Whether practising celibacy, or any sex related activity, is good or bad,
depends on if doing so is compatible with virtues.
For some this may be selfish or foolish choice.
For others this may be a good human life.
Virtue allows a wide variety of lives.

----

## My comments

In Stoicism, this is only requirement for what character traits
are considered virtue.
The piratical benefits of virtue discussed in the 
[last chapter]({{< ref "/blog/on-virtue-ethics-08/" >}})
are secondary.
In other words, they are the icing on the cake,
not the cake itself.

The 2nd end "the continuance of its species" perhaps should be "the survival of one's
children and kins". There is no evidence that social animals care about their species
as a whole.
