---
cover:
  caption: Can LLM break lines?
  image: bunny-line-breaks.webp
pubdate: "2024-10-04"
tags:
- AI
title: Testing using LLM to do automatic line breaks
---

I have a habit of manually inserting line breaks when I write.  
Usually, I do this at punctuation marks.  
When that's not possible, I try to break at logical points.  

Since I am experimenting with what LLMs can do, I thought this could be a good test.  
I tried many models using the same system prompt:

---

```
Please:
- Adjust line breaks so that lines are neither too short nor too long.
- Make sure each line has no more than 90 characters.
- Preferably, add line breaks after punctuation marks such as periods or commas.
- Always start a new line for a new sentence, i.e., after a period that ends a sentence.
- If it's not possible to break lines at punctuation marks, break at logical points to maintain readability.
- Preserve the original text structure but make the line breaks more readable.
- Do not alter the content of the text, only adjust formatting with line breaks.
- Return only the text with improved line breaks.

Example:
Original: The quick brown fox jumps over the lazy dog. The dog, being lazy, does not respond. The journey took about thirty minutes and covered more than five kilometers without stopping.

Corrected:
The quick brown fox jumps over the lazy dog.  
The dog, being lazy, does not respond.  
The journey took about thirty minutes  
and covered more than five kilometers without stopping.
```

---

Note that I only tested each model once, so this is not scientific research.  

The results are as follows:

- Llama 3.2:3b works as well as GPT-4o and Claude-3.5-Sonnet for this task.  
- Google's Gemini models all performed poorly.  
- Gemma 2:2b made one mistake but was otherwise acceptable.  
- Phi3.5 failed completely.

What follows are the detailed test results:


```

# Original Test

As I sit here in my chambers, surrounded by the trappings of power, I am reminded of the impermanence of all things. Like the fleeting seasons, our lives too are subject to change and uncertainty.  It is in these moments of introspection that I come to realize how much I rely on external sources for guidance and wisdom, rather than trusting my own inner compass.  The mind is a fragile thing, susceptible to the whispers of doubt and fear.

# latexindent (Perl with regex)

As I sit here in my chambers,
surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons,
our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize how much I rely on
external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing,
susceptible to the whispers of doubt and fear.

# Meta

## llama3.2:1b

Here's the corrected version with improved line breaks:
As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons,
Our lives too are subject to change and uncertainty.
It is in these moments of introspection
that I come to realize how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing,
susceptible to the whispers of doubt and fear.

## llama3.2

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize
how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear,
and it is here that I must confront the limitations of my own understanding.

# Microsoft

## phi3.5

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things. Like
the fleeting seasons, our lives too are subject to change
and uncertainty.
It is in these moments of introspection that I come to realize:
how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing,
susceptible to the whispers of doubt and fear.

# OpenAI

## gpt-4o-mini

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize how much I rely
on external sources for guidance and wisdom, rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear.

## gpt-4o

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize
how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear.

# Google

## gemma2:2b

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear.

## gemini-1.5-flash

As I sit here in my chambers, surrounded by the
trappings of power, I am reminded of the
impermanence of all things. Like the fleeting
seasons, our lives too are subject to change and
uncertainty. It is in these moments of introspection
that I come to realize how much I rely on external
sources for guidance and wisdom, rather than
trusting my own inner compass. The mind is a
fragile thing, susceptible to the whispers of doubt
and fear.

## gemini-1.5-flash:8b

As I sit here in my chambers,
surrounded by the trappings of power,
I am reminded of the impermanence
of all things.
Like the fleeting seasons,
our lives too are subject to change
and uncertainty.
It is in these moments of introspection
that I come to realize how much
I rely on external sources for guidance
and wisdom, rather than trusting
my own inner compass.
The mind is a fragile thing,
susceptible to the whispers of doubt
and fear.

## gemini-1.5-pro

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things. Like the fleeting
seasons, our lives too are subject to change and uncertainty. It is in
these moments of introspection that I come to realize how much I rely
on external sources for guidance and wisdom, rather than trusting my
own inner compass. The mind is a fragile thing, susceptible to the
whispers of doubt and fear.

## gemini-1.5-flash-002

As I sit here in my chambers, surrounded by the
trappings of power, I am reminded of the
impermanence of all things. Like the fleeting seasons,
our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come
to realize how much I rely on external sources for
guidance and wisdom, rather than trusting my own
inner compass. The mind is a fragile thing,
susceptible to the whispers of doubt and fear.

## gemini-1.5-pro-002

As I sit here in my chambers, surrounded by the
trappings of power, I am reminded of the
impermanence of all things.
Like the fleeting seasons, our lives too are subject
to change and uncertainty.
It is in these moments of introspection that I come
to realize how much I rely on external sources for
guidance and wisdom, rather than trusting my own
inner compass.
The mind is a fragile thing, susceptible to the
whispers of doubt and fear.

# Anthropic

## claude-3-haiku-20240307

As I sit here in my chambers, surrounded by the trappings of power, I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize how much I rely on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear.

## claude-3.5-sonnet-20240620

As I sit here in my chambers, surrounded by the trappings of power,
I am reminded of the impermanence of all things.
Like the fleeting seasons, our lives too are subject to change and uncertainty.
It is in these moments of introspection that I come to realize how much I rely
on external sources for guidance and wisdom,
rather than trusting my own inner compass.
The mind is a fragile thing, susceptible to the whispers of doubt and fear.

```
