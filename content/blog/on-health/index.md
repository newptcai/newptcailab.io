---
pubdate: "2021-11-02"
tags:
- health
- advice
title: On Taking Care of Your Physical and Mental Health
---


Here are some things which I wish I had been told when I first went to college.

1. Sleep well should be your top priority. ([Why lack of sleep is bad for your health](https://www.nhs.uk/live-well/sleep-and-tiredness/why-lack-of-sleep-is-bad-for-your-health/) 
   and [Why Bill Gates thinks sleep is important](https://www.goodreads.com/review/show/3079578485).)
3. Exercise regularly for the sake of [both your mind and your body](https://www.goodreads.com/review/show/1156208019).
4. Learn [how to beat procrastination](https://www.goodreads.com/book/show/36664616-stop-procrastinating).
5. Avoid getting [addicted to tech](https://www.goodreads.com/review/show/2649289238) by practising [digital minimalism](https://www.goodreads.com/review/show/2720398640).
6. Get some tips for [socialising successfully](https://succeedsocially.com).
7. [Fail-safe](https://programs.clearerthinking.org/fail-safing_your_plans.html) your important plans.
8. Ask yourself some [life-changing questions](https://programs.clearerthinking.org/lcq.html).
9. Don't feel ashamed for asking for [help](https://dukekunshan.edu.cn/en/caps) when you need.
