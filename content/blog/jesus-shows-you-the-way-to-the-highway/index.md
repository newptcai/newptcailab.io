---
pubdate: "2021-03-13"
tags:
- film
title: Jesus Shows You the Way to the Highway (2019)
---


[Jesus Shows You the Way to the Highway (2019)](https://www.imdb.com/title/tt8550514/) 
is perhaps one of the absurdest films which I have ever watched.
Yet it is also one of funniest films I have watched.

The film is absurd for several reasons. 
First, it looks very much like a low-budget film from the 1970s.
So the set and the ascetics looks ridiculously rough.
Second, the actors are far from what we are used to see on screens.
Third, the acting are horrible by normal standard.
Actors play their part with seriousness 
as if they do not know that they are in a very "bad" bad film.
And they either speak English with ludicrous accent,
or with an monotonicity as if the film was poorly dubbed.
Fourth, the plot does not make any sense and mixes tropes from 
James Bond, cold-war thriller, Science fiction, romance and Kong-Fu films.
I can go on for a while, but you get the idea.

However, once you realized that all these "flaws" are intentional,
you will see where the jokes are.
All the illogic plots, all the ridiculous lines, the exaggerated seriousness in the acting, 
all become very funny. 

But a more important reason why I love the film is that, 
it asks a question -- 
who plays an more important role in making a film great? 
Is it the audience, or the film? 

I am asking this question, because, 
if you see through the absurdities in this film and focus on the plot,
you can see that it actually has a story with the usual elements of a reasonable story,
even quite a sophisticated one. 
It is easy to imagine that with a budget of a few million dollars, 
Hollywood can make blockbuster with more or less the same storyline as this film.
Hire some stars, add some visual effect, make the lines a bit normal.
Plenty of people would enjoy watching such a remake.
But do we really need that?
We as audience, with a little push, 
we can already immerse ourselves into this film, 
and have 1.3 hours unexpected fan.
