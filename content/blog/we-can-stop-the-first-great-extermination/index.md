---
ShowToc: false
date: "2024-11-27"
images:
- desolate-landscape-of-grazing-sheep.webp
tags:
- book
- environmentalism
title: "We Can Stop the First Great Extermination by George
  Monbiot"
---

## 🙏 Acknowledgement 

This essay by [George Monbiot](https://www.monbiot.com/), a columnist for [The
Guardian](https://www.theguardian.com/profile/georgemonbiot), is part of his
book [This Can't Be Happening](https://www.goodreads.com/book/show/55663836-this-can-t-be-happening),
a collection of his writings on environmental issues. We are grateful to Monbiot
for allowing us to [translate it to Chinese]({{< ref path="/blog/we-can-stop-the-first-great-extermination/" lang="zh">}})
and republish his essay here.

{{< figure src="desolate-landscape-of-grazing-sheep.webp" alt="Desolate landscape of grazing sheep" >}}

## We Can Stop the First Great Extermination 

George Monbiot

**October 2019**

*The Launch of Animal Rebellion, Smithfield Market, London*

I have spent my life learning and unlearning. We cannot navigate the world in which we live without unlearning some of its deepest myths.

One of the most enduring stories is the pastoral myth: the idea that the city is corrupt and evil, while the shepherd and his flock are innocent and pure. This story is thousands of years old. It recurs in so many cultures that I wonder if it originated among the nomads of Central Asia, who occupied both India and south-eastern Europe. It was told by the ancient Greeks and by the scribes who wrote the Old Testament. By the Roman poets and Elizabethan writers-by Spenser, Marlowe, and Shakespeare. It was revived by the Romantics and lives on to this day in hundreds of television programs. If the BBC were any keener on sheep, it would be illegal.

A version of the pastoral myth appears in some of the first books that any child encounters: the farmyard tales that occupy our moral imagination from our earliest glimmerings of consciousness. The story they tell is remarkably consistent: a farm with one cow, one pig, one horse, one sheep, one chicken, and one rosy-cheeked farmer, living together in bucolic harmony. There is, of course, no hint of why animals might be kept on a farm or what fate awaits them. These farms bear no relation to any farm I have ever seen, yet somehow the old, old story still governs our perception of where our food comes from. It can take a lifetime to unlearn. I suspect many people never do unlearn it.

It took me long enough, even though I worked on livestock farms when I was a teenager. I didn't challenge the pastoral myth until I moved to Mid Wales. I lived in the Dyfi Valley, between Snowdonia and the Cambrian mountains.

At first, I was excited. I could walk all day in any direction and scarcely cross a road or see a house. But soon, my excitement gave way to puzzlement. My puzzlement gave way to disappointment. My disappointment gave way to despair. The land was not just empty of people; it was empty of wildlife.

Above around 200 meters, there were no trees. There were no birds. I could walk all day and see two crows and a pipit. I was lucky. I could get down on my hands and knees in the middle of summer and find no insects in the sward. It was a dead land.

Only one kind of animal was in abundance: sheep.

Slowly, it began to dawn on me that the two things might be connected. I started to understand that the landscape was empty because of the sheep. I discovered that sheep selectively browse out tree seedlings: when the trees that covered these mountains died, there were no young ones to replace them. The entire ecosystem, across hundreds of years, had been wiped out by livestock.

Then I realized that what I was seeing in Mid Wales was almost universal: across Britain, sheep have reduced once-thriving uplands to bare monocultures. Obvious as it now was, I had failed to see it because I had not yet unlearned what I was taught as a child.

Farmers insist that they are feeding the nation. But is this really true? I discovered, through my own research, as there are no official figures, that sheep farming in the uplands occupies roughly four million hectares in the UK. That’s more land than we use for growing grain. It’s twenty-four times as much land as we use for growing fruit and vegetables. Yet sheep in this country, upland and lowland, provide just 1% of our food.

This, in other words, is a classic example of agricultural sprawl: huge areas of land used to yield a tiny amount of food. All over the world, livestock grazing, while producing just a small proportion of our food, wipes out wildlife and habitats on an enormous scale.

Well-meaning TV chefs and food writers tell us we should switch from intensively reared meat to pasture-fed meat. But all this does is swap a system that causes astonishing cruelty for one that causes astonishing destruction. If everyone did as they suggest, we would quickly run out of planet. There simply isn't enough land.

Free-range chicken farming can also be extremely damaging to the living world. Take a look at what is happening to the beautiful River Wye and its tributaries on the Welsh border. They are being turned, at staggering speed, into open sewers. In high summer, they stink-literally. The primary reason is the proliferation of free-range chicken farms in the area. Chickens kept in large numbers outdoors lay down a scorching carpet of reactive phosphate. Rain washes it into the rivers, causing algal blooms and wiping out much of their wildlife.

We often hear about ‘wildlife-human conflicts.’ But almost all of them, in reality, are conflicts between wildlife and livestock. Why are badgers being persecuted in this country? Why are wild boar being slaughtered across Europe? Why are wolves, coyotes, and pumas massacred in North America? Because of their impact on livestock.

Some livestock farmers say that, through ‘regenerative farming,’ they are mimicking nature. It is true that, in some respects, their practices are better than conventional livestock farming, though their most arresting claims, especially concerning the amount of carbon they store, are grossly exaggerated. But they don’t mimic nature: they reduce it to a parody of its former abundance and diversity. Where are the wild predators? Where are the wild herbivores? Where are the trees?

Slowly and reluctantly, I have come to see that there is no good way of raising animals for food. However you farm livestock-for meat, milk, or egg production-it imposes too great an environmental load for ecosystems to absorb.

Some people respond to the twin crises of animal cruelty and environmental destruction by giving up meat while continuing to eat fish. But commercial fishing is causing cascading ecological collapse across the blue planet. Fishing fleets are ripping apart marine ecosystems, tearing up the complex and fragile communities of life on the seafloor, wiping out not only the target species but turtles, dolphins, sharks, albatrosses, and whales. Dead dolphins are now washing up on the coasts of Britain and western Europe in such numbers that it is hard to see how their populations can be sustained.

Here, too, we have been misled by seductive myths. When you hear the word ‘fisherman,’ what image comes to mind? Someone who looks like Captain Birdseye: white beard, twinkly eyes, sitting on a little red boat chugging merrily across a sparkling sea?

This, again, is what our earliest children’s stories show us. But it is just as far from reality as the picture-book farm. To give one example, 29% of the UK’s fishing quota is owned by five families, all of whom feature on the *Sunday Times* Rich List. Much of the fish people eat is now caught by vast ships, operating with gigantic nets and at great speed, scooping up everything in their path.

If we stop eating animals, we not only reduce the cruelty humanity inflicts. We can also start restoring living systems both on land and at sea. Without livestock farming, huge tracts of land could be returned to nature. We could bring back our forests, bring back our wetlands, bring back our peat bogs, bring back our living wonders. If we stop trawlers and dredgers from ploughing the seabed, we can allow its complex living crust to reform, and the suppressed populations of fish and other animals to recover.

Not only could this shift help stop the sixth great extinction in its tracks, but it could also draw down carbon from the atmosphere, helping us to prevent runaway global heating.

We need to leave fossil fuels in the ground. We need a huge and rapid shift in the industrial economy. But we now know this is not enough. To prevent 1.5 or even 2°C of heating, we also need to extract carbon from the air. The restoration of ecosystems seems to be the most effective means of doing so.

Our aim should be to reverse agricultural sprawl, to reverse the great damage done by the fishing industry, to minimize the scale of our presence on earth, and to rewild the land and the sea. And this means retelling our stories.

Let us challenge the old myths. Let us restore our world of wonders. Let us make this the rich and beautiful world we want our children to inherit.
