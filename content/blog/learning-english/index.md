---
pubdate: "2021-10-27"
tags:
- language-learning
- advice
title: On Learning English
---


Whatever your life plan is, being able to use English smoothly
will offer you many practical benefits, such as studying abroad, 
travelling around the world,
making friends from another country,
etc.
But more importantly,
it opens up an intellectual gateway which, if you choose to take it, 
will profoundly change how you see the world.
Learning English is one of the best ways that you can invest in yourself.

## How I learned English

When I moved to Canada, my English was so bad 
that I had trouble to order food in restaurants. 
But I was already there and I planned to go to graduate school in half a year.
So I had no choice but to get over English.

I did a couple of things to force myself to do it:

1. I changed the language of my cellphone and computer to English.
2. I blocked Chinese websites which I used to visit most often. If I want to check
   news or browse the Internet, I have to visit English websites.
3. I made a pact with myself -- If I want to read a novel, or watch a TV/film, it
   must be in English. Otherwise I don't allow me to do it.
4. Whatever I write, an email, a diary entry, a shopping list, I will do it in
   English.

It was tough at the beginning, 
and I broke my pledge several times.
But as time went by, 
these rules stuck with me and I got used to *use* English in my daily life.

Once I passed the initial barrier, it became much easier and enjoyable.
I could have basic conservations with Canadian friends.
I could order food in English.
I could read newspapers.
There are numerous high-quality English books to read, TV shows to watch, podcasts to
listen to.
And the more I used it, the better I got.
I was even surprised that I waited for so long to actually learn English.

A lot things may seem hard to us, not because they are hard, 
but because we are not truly committed.
I think learning English is one of such things.

## Some Tips in Learning English

### DuoLingo

[DuoLingo](https://www.duolingo.com/) is website which gamifies language learning
process. I found it useful in building up a basic vocabulary.
But if you already have a large vocabulary,
it won't help you much.

### Listen to a podcast

Pick a good podcast, such as [Planet Money](https://player.fm/series/planet-money-1324387), to listen.  You can learn
fascinating things while learning English.

### Read a book

Pick a *short* book from your favourite genre, SF, thriller, crime, fantasy, etc., and read
it on your phone or iPad -- it is very easy to look up words on these devices.
However slow, keep going. Five pages a day is OK. Two pages a day is OK.  One page a
day is OK.  The point is to finish it.  Once you have read one book, reading in
English will seem much less a challenge in your mind.

### Grammarly

[Grammarly](https://app.grammarly.com/) offers an online grammar checker for free.
I found it very useful in finding small mistakes.
But use it only when you have done your own proofread.
Like spellcheck, it is a useful tool but that it can't 
replace your own revision and editing.
