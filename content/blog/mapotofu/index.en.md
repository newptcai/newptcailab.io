---
pubdate: "2023-04-30"
tags:
- recipe
- veganism
title: 'Vegan Mapo Tofu: A Swedish-Chinese Recipe'
---


While living in Sweden, a friend asked me, "Do you know if this recipe is
authentic? I found it on the Internet, and I'm not sure if it's reliable." She
likely assumed a Chinese person would know more about Chinese cooking than
random food bloggers on the Internet. But I playfully replied with feigned
seriousness, "Where do you think I learned to cook?" "From the Internet? 😲" "Of
course!"

It was a joke, but it was also the truth. I barely cooked when I lived in China.
It was only when I started studying in Canada that I had to cook for myself. So,
most of the recipes I know, Chinese or otherwise, I learned from the Internet.

Mapo Tofu, also known as Mapo Doufu, is a spicy and savory dish from my home
province of Sichuan, which boasts a rich culinary tradition. The name "Mapo" is
derived from the pockmarked (麻子 mázi) face of the elderly woman (婆婆 pópo)
who is said to have first created the dish, while "Tofu" refers to the bean curd
used as the primary ingredient. Mapo Tofu is characterized by its unique
combination of spicy, numbing, and savory flavors, resulting from the use of
Sichuan pepper, chili peppers, and fermented bean paste.

![A wonderful Mapo Tofu](08.jpg)

Although I was familiar with the dish, my Swedish friend Tilo, a connoisseur of
Sichuan cuisine, taught me how to perfect it. He has a particular fondness for
Sichuan peppers, which give your tongue a distinct numbing sensation. As a
result, Mapo Tofu, with its abundance of Sichuan peppers, quickly became one of
his favourites. Tilo shared several tips on how to elevate the dish, and it
became a staple at our weekend gatherings, where it was always a hit with our
friends.

Having been away from Sweden for a few years, I began to miss the time spent
there. With my mom's help, I attempted to recreate Tilo's recipe and sent him a
photo. He recognized it instantly — "Mapo Tofu?" "Yes, with your recipe!" "Did
it work?" "Yes, 😃."

This easy, delicious dish can be made vegan with minimal effort. Here's Tilo's
recipe.

## The ingredients for a serving of two ---

* 350 grams of soft tofu
* 1 cm of ginger
* 2 garlic cloves
* a handful of Sichuan pepper
* 1 green onion
* 2 tablespoons of [Pixian Doubanjiang 郫县豆瓣](https://en.wikipedia.org/wiki/Doubanjiang) (A hot sauce made from fermented broad beans. Also made in Sichuan.)
* a pinch of pepper
* 5 to 10 dried shiitake mushrooms
* a teaspoon of starch
* 5 tablespoons of cooking oil

![The Ingredients of Vegan Mapo Tofu](00.jpg)

## Step 1 - The Shiitake Mushrooms

Traditionally, Mapo Tofu uses ground pork to add texture alongside the tofu.
Since I am a vegan, I typically use dried shiitake mushrooms as an alternative.
They have a meaty aroma and chewy texture, making them excellent meat
substitutes. Soak them in cold water for 30 minutes or until they are soft
enough to chop. If you're in a rush, you can soak the mushrooms in boiling
water, reducing the waiting time to 15 minutes.

(My mom strongly believes it's unwise to strictly adhere to time constraints for
such things. We had a spirited debate about what numbers we should provide. So,
don't take the times too seriously. 😉)

## Step 2 - Chopping and Cutting

Finely chop the garlic, ginger, green onion, and mushrooms. This part is easy.

![Finely chopped garlic, ginger, green onion, and mushrooms](01.jpg)

The more challenging part is cutting the soft tofu. When you open a box of soft
tofu, slice along the four sides of it, following the walls of the box. Turn the
box upside-down over the chopping board. Gently knock the bottom of the box
until the tofu and the box are separated.

⚠️  This step requires patience. Rushing may result in broken tofu, as shown in
the picture below. (The appearance of the tofu does not affect its taste, so
unless you are a perfectionist, this isn't a significant concern.)

Gently slice the tofu into 1.5 cm cubes.

![Not very beautifully sliced tofu](02.jpg)

## Step 3 - Prepping the Tofu

This is Tilo's first trick.  Soft tofu is often too delicate and can easily
break into a mess. Boiling it for a minute in water makes it slightly firmer and
also removes some of the bitterness, while keeping the interior as tender as
possible.

![Let the tofu taking a shower](03.jpg)

## Step 4 - Frying the Ingredients

Heat 5 tablespoons of cooking oil in a large wok (use less if you're concerned
about your waistline). Add Sichuan peppers, garlic, and ginger to the wok and
fry them until their aromas are released. Add shiitake mushrooms and fry them
until they turn brown.

![Fry Sichuan peppers, garlic and ginger](04.jpg)

Push everything away from the bottom of the wok. Add two tablespoons of Pixian
Doubanjian and fry it briefly until you can smell its aroma.

![Fry Pixian Doubanjiang](05.jpg)

## Step 5 - Adding the Tofu

Gently add the tofu to the wok. Pour in enough water so that the tofu is
half-covered. Add most of your green onions and sprinkle the pan with a pinch of
pepper. Cover the pan and let it boil for five minutes. Then, dissolve a
teaspoon of starch and thicken the dish with it.

![Adding the tofu](06.jpg)

Transfer the dish to a large bowl and cover it. Let it sit for 15 minutes so that the tofu absorbs even more flavors.

## Step 6 - The Magic of Sichuan Pepper

Here's Tilo's second trick. Right before you serve the dish, heat a few Sichuan
peppers in a frying pan without adding any oil to make them crispy. Then, grind
them into a powder using either a food processor or a mortar and pestle. (If you
don't have either, cover the peppers with a paper towel, place it on a chopping
board, and crush them with the side of a kitchen knife.) Sprinkling the freshly
made Sichuan pepper powder onto the dish gives it an especially mouth-watering
aroma.

![Freshly make Sichuan pepper powder](07.jpg)

Garnish with the remaining green onion. Now you have a wonderful Mapo Tofu.

![美味的麻婆豆腐](08.jpg)
