---
pubdate: "2021-09-20"
tags:
- environmentalism
title: A Pledge for a Low Waste Life
---


## A Reckoning

![Plastic Waste](plastic-waste.jpg)

In recent years, plastic waste has got some of my attention.
BBC's hit documentary [Blue Planet II](https://www.bbcearth.com/blue-planet-ii) (2017) shows me the horrific plastic pollution in our oceans.
From another documentary [The Story of Stuff](https://www.storyofstuff.org/storyofplastic/),
I learned how how the oil and plastic industry intentionally made us addicted to plastics.
I also learned from a [podcast](https://www.npr.org/2020/09/11/897692090/how-big-oil-misled-the-public-into-believing-plastic-would-be-recycled) by NPR (National Public Radio)
that plastic recycling is more or less scam to keep us feeling OK about keep using plastics 
because they cannot really be recycled on a large scale.

However, after acquiring all these information, 
I changed nearly nothing in how I consume plastics.
My excuse for long time has been 
this [tweet](https://twitter.com/dwallacewells/status/1199733252505919488?lang=en) by the author and journalist [David Wallace-Wells](https://en.wikipedia.org/wiki/David_Wallace-Wells),
who quoted [an article](https://www.wired.co.uk/article/climate-change-plastic-pollution) by the magazine Wired UK --

> "The war on plastic might be more about making us feel better than actually
> tackling the climate crisis." Might? 

Wallace-Wells wrote [The Uninhabitable Earth](https://www.goodreads.com/book/show/41552709-the-uninhabitable-earth), 
one of my favourite books on climate change.
His thinks that although plastic pollution is a big problem,
it pales comparing to the damage climate change will do to marine lives and ocean
ecological systems.
Spending vital resources on addressing plastic pollution 
is essentially a trick to divert our attentions from the most important problem, i.e., 
how to cut our carbon emissions.

When I thought about this problem again recently,
I realized that I was just making an excuse 
for my own addiction to the little bit of convenience offered by plastics.
Climate change is indeed a much larger issue.
But how does cutting plastics usage in my own life impede my 
ability to care about climate change?
In fact, it probably will make me feel much better for living in a way that is
consistent with my beliefs and values.

So here are things I pledge to do to live a "low-waste" life style,
most of which I learned from the excellent (and short) book [No More Plastics](https://www.goodreads.com/book/show/40030019-no-more-plastic)
by the British anti-plastic activist [Martin Dorey](http://martindorey.com/).

## I will never buy bottled water again

> over 35 million plastic bottles are used every day in the UK and around 23 million of them won't get recycled.
> -- No More Plastics

And that is only UK alone.
In many developed countries, such as UK, tap water is free and as healthy as, if not heartier than, bottled water.
Even though I live in China, where you are supposed to drink tap water after boiling it, 
it is quite easy to avoid bottled water.
I have two kettles to boil waters, one in my office and one at home.
I also get free ice water from my university cafeteria.
All I need to change is to remember carrying my cute little water bottle when I go out on a trip.

![My water bottle](bottle.jpg)

## I will never buy plastic bags anymore

I have a reuse-able shopping bag which travelled with me from Sweden to Canada and
then to China. It always sits in my backpack. So avoiding plastic shopping bags is
not a problem for me.

![My bag](bag.jpg)

What is tricky is buying fruit and vegetables in China.
Most of the time you will find them wrapped plastic bags in grocery stores.
I think I will just go to shops where this can be avoided.

## I will never order coffee in paper cups anymore

> The UK gets through 2,500,000,000 coffee cups every year. That's around 79 every second.
> -- No More Plastics

Paper cups are mostly coated with plastics so they cannot be recycled.
What is the solution?
I will just bring my one of own mugs, 
which I bought many from IKEA to remind my days in Sweden.

![My mug](cup.jpg)

## I will never use plastic drinking straw/plastic cutlery

Plastic cutlery is much less a problem in China since most restaurants do offer
reusable chopsticks and spoon.
But just in case, I bought a set of mobile cutlery.

![My cutlery](cutlery.jpg)

I don't drink cold beverages like sodas anymore. 
So straws are also easy to delete in my life.

## I will not order food delivery anymore

It is extremely convenient and cheap in China to order food delivery.
Click a few buttons on your phone and your dinner will appear by your door step in
less than hour.
The catch is that the food almost always comes in plastic containers.

My university has a pretty good cafeteria which offers three meals a day, seven days
a week. There is also a food court 5 minutes from my home.
What excuse do I have for ordering food delivery?  "It sometimes rains." Don't you have an
umbrella? 😅️


## Final Thoughts

It would be more difficult to live a (near) zero-waste life like [Lauren Singer](https://www.youtube.com/watch?v=2BMWQZkBgsM) does,
Her trash in two whole years can fit into a single mason jar!
I have no plan at the moment to make my own tooth paste and shampoo.
But that does not mean I have an excuse to not to reduce most of the plastic waste in my life.
I do not want to be perfect.
I just want to be better.
