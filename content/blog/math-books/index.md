---
pubdate: "2023-10-17"
status: draft
tags:
- mathematics
- book
title: Books That Got Me Interested in Math
---

I always feel that I became a mathematician accidentally.
But in retrospect, since I was a kid, 
I did read serial books about mathematics and mathematicians and has always been
drawn to a career and life in mathematics.
Below are the books which ignore my interest.

## Nature's Numbers: The Unreal Reality Of Mathematics

{{<figure src="nature-number.jpg" caption="Nature's Numbers">}}

[This book](https://www.goodreads.com/book/show/287896.Nature_s_Numbers) by Ian
Stewart has some interesting facts about the nature of mathematics.  It has been
decades since I read it, but I still remember that I was awe-filled when
I learned form the book that Fibonacci sequences appear the arrangement of a pine cone.

## Archimedes' Revenge

{{<figure src="Archimedes-revenge.jpg" caption="Archimedes' Revenge">}}

Another of my favourite book as a child is 
[Archimedes' Revenge](https://www.goodreads.com/book/show/1044778.Archimedes_Revenge)
by Paul Hoffman.
It is a collection of stories about several mathematicians, including Archimedes,
Euler, and Pythagoras.
