---
pubdate: "2021-07-18"
tags:
- philosophy
- book
- virtue-ethics
title: On Virtue Ethics by Rosalind Hursthouse -- Chapter 08 The Virtues Benefit Their
  Possessor
---


A summary of Chapter 08 The Virtues Benefit Their Possessor.

----

Can we **objectively** justify which character traits are the virtues?
We can only do this from some already acquired ethical framework,
instead from some external neutral point of view.
Doing so risk just rationalize what we already believe.
But if we think critically, 
little by little we many radically change our entire ethical outlook.

> The philosopher's task was well compared by Neurath to that of a mariner who must
> rebuild his ship on the open sea. We can improve our conceptual scheme, our
> philosophy, bit by bit, while continuing to depend on it for support; but we cannot
> detach ourselves from it and compare it objectively with an unconceptualized
> reality.

Plato's requirement on the virtues

1. The virtues benefit their possessor. (They enable her to live a life that is
   eudaimon.)
2. The virtues make their possessor a good human being.
3. The above two features of the virtues are interrelated.

(Note that here the author uses eudaimonia to mean flourishing. 
It is a bit different from Stoic who mostly use eudaimonia for the second point
above.)


### Objections to the Very Idea

Being virtuous does not necessarily mean it benefits a person in every particular occasion.
However, people also think virtue is not enough or is unnecessary for eudaimonia.
It is not necessary because bad people can flourish like a green bay tree.
It is not sufficient because being virtue may lead an early death or ruin of life.

But if we take point 1 as a probability statement which implies the most likely way
to eudaimon, then it avoids these problems.
Exercising, eat well, avoiding cigarettes and alcohol will not guarantee nor is
necessary that we live to 100 years old,
but it is the most likely way to achieve that.

### Different Contexts

Not being able to convincing the wicked about point 1 does not make it untrue.
We usually think about it in the context of child education and self-reflection.

Most readers of the book would agree that virtue is the most reliable bet to prepare
children for life.
Even in time of great evil, there is still no other reliable bet for children to have
a good life. Eudaimonia will be just impossible.
Parents will teach children virtue in hope that a better time will come.

When we think ‘Why should I be virtuous/moral?’ we may answer 
‘I want to be—that's the sort of life I want to live, 
the sort that I think is a good and successful and rewarding one.’
and ‘If only I could be less selfish and self-centred, 
more thankful for what I have,
more concerned with the good of others and the good in them, 
how much happier I would be,’ 
This is to say, that we think virtue benefits ourselves in some practical manner.

### No Neutral Viewpoint

Hare says that ‘by far the easiest way of seeming upright is to be upright’,
and he claims this is an empirical fact.
This is wrong because 
the immoralist can declare high rewards merits high risks.

There is no possibility of finding a neutral ground for us and the wicked,
because when we say their life does not count as eudaimon,
we are, **in part**, restate our definition of eudaimon,
and this is not an empirical fact.

Phillips and McDowell think virtuous action should be chosen for their own sakes,
and the benefit justification of virtue is unnecessary,
because the virtuous identify virtue as equal to eudaimonia or benefit.
This is no neutral ground for discussion between us and the wicked.

According to them, if virtue necessitates your losing everything,
that is not a disaster.
But there exists cases when pointless suffering happens,
and there is moral dilemmas without a good solution.
How can we claim these are not tragedies and not loss at all?
An early death or loss of health can also renders you useless
for helping other people.
The Stoic view of the world is unrealistic,
a masculine yearning for the ideal of ‘the short life with glory’

So this view is very hard to teach our children.
To inoculate virtue into them, 
it is necessary to emphasize that if the act well (virtuously),
things will go well for them.

Our conceptions of loss, harm, disaster though distinctively different from those of
the immoralist, overlap with his with respect to such things as death, physical
injury, suffering, and helplessness.
Our difference is that we think virtue **usually** brings eudaimonia 
instead of tragedies, 
and that when eudaimonia is impossible it is because of bad luck.

We also share the concept of enjoyability, or the smile-factor of things, 
with the wicked.

All in all, if human nature (discussed later) means that a virtuous life is more
likely a life that is more desirable,
then our list of virtues can be justified from empirical facts.

### Facts Within Ethical Outlooks

Since both the virtuous and the wicked share the concept of enjoyment,
the virtuous may claim that wicked is wrong, 
that their enjoyments and satisfactions are better than his—more
readily come by, safer, longer lasting, less subject to the vagaries of luck.
However, the immoralist can simply say the examples are atypical.

There is problem of how do we classify these problems.
Are they philosophical or are they empirical?
The beliefs on human nature are part and parcel of our ethical outlook,
but they are quite different from evaluative beliefs.

Children lack of moral wisdom (prohonesis), 
not only because their values are crude,
but because they do not know human nature 
and how human life works.

Virtue ethics offers a distinctive version of the view that
morality is a form of enlightened self-interest.
It is impossible to justify virtues from a neutral point of view,
but starting from an ethical outlook does mean values are fixed.

----

## My comments

I am not convinced that objectivity of morality is impossible and
moral reflect must start from an ethical outlook.
But this chapter is a good reminder that living an ethical life
is the best bet to live a happy (in the everyday sense) life.
More often than not, 
it is not that virtue requires us to sacrifice and live miserably,
it is that we are not able to choose the virtuous action in every moment,
and suffer from the consequences.
