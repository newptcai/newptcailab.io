---
pubdate: "2022-12-16"
tags:
- mathematics
title: Stein's Method
---


I recently give two seminars for DKU's
[Discrete Math Seminar](https://sites.duke.edu/kits_team_101_48585) 
on Stein's method.
Here are my slides for 
the [1st talk](/assets/doc/stein.pdf)
and
the [2nd talk](/assets/doc/stein-1.pdf).
