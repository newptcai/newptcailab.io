---
pubdate: "2021-07-20"
tags:
- book
- philosophy
- virtue-ethics
title: On Virtue Ethics by Rosalind Hursthouse -- Chapter 11 Objectivity
---


A summary of Chapter 11 Objectivity

----

Virtue ethics rejects the sort moral objectivity which Kant aspires to.
The naturalism describe in the last three chapters also rejects
the type of objectivity based on empirical facts 
accessible from a neutral point of view.
However, it also does not assume our standard list of virtues is correct
without need of validation.
This is the type of objectivity naturalism can offer.

But the study of objectivity should also give an account of ethical disagreement.

> The fundamental issue concerning ‘objectivity’ in ethics is whether ethical
> disagreements, when traced painstakingly to their source, turn out to be rooted in
> disagreements about facts, or differences in values about which nothing can be
> said.

This chapter will only address ethical disagreement within the framework of virtue
ethics and naturalism.

## Ethical disagreement

The first example of ethical disagreement is religious beliefs.
There is really nothing more can be said when there is disagreements
in metaphysics.

In virtue ethics, disagreements can arise from wrong application of virtue
and vice terms, or incorrect judgement of what a virtuous agent would do.
Such disagreements can be resolved in principle, if not in practice.
However, both sides also have to realize that what the virtuous agent does
cannot be completely determined.
Also maybe people are simply describing different aspects of the same action.

Another example is our disagreements with our sexist ancestors,
which are rooted in disagreements on female human natures.
The are many facts about women were not accessible to our ancestors,
but they are for us,
and they are not merely ideologies.

What about sex? 
Some people think human appetites are separated from
reason, so we need the virtue of self-control in sex. 
Hursthouse disagrees.
She think temperance (moderation of sex desire) is the relevant virtue.

> If the disagreement can be traced to that source, we know that that is just a
> mistake.  The very well-known facts of cultural variety in what is found immensely
> enjoyable and utterly disgusting show, from a neutral point of view, that our
> appetites can be trained in such a way that they are somewhat harmonized with
> judgements about whether eating, or drinking, or having sex with, this in these
> circumstances would be an innocent pleasure or a bad (terrible, degrading, dirty,
> undignified, or childish) thing to do.

But deeper disagreement arises from different takes of human natures.
For example, some may say that temperance in sex is simply advanced self-deception.
Can this be resolved by facts?
It would be hard to come up with these facts.
But in a prolonged and detailed discussion, may be the intemperate can be led out of
her errors.

## The Third Thesis: Human Nature as Harmonious

Among the four ends discussed in [chapter 9]({{< ref "/blog/on-virtue-ethics-09/" >}}),
the third one (characteristic pleasure and free from pain) 
might seem to conflict with the fourth one
(the well-functioning of a social group),
as hinted in the opinion of the proponent of self-control in the above disagreement.

However, Hursthouse maintains that Plato's third thesis, that can develop character
traits that both (1) benefit ourselves and (2) make us good humans.

> ‘We have the virtues neither by nor contrary to [our] nature,’ Aristotle says, ‘ we
> are fitted by [our] nature to receive them.’

In other words, through moral education, we can acquire virtues and enjoy the
exercise of virtues without any inner conflicts.

Some one objects this view may say

> that human nature is, biologically, so intrinsically egoistic that a
> ‘conventional’ moral upbringing which inculcates the virtues as second nature
> distorts and perverts it, producing defective human beings instead of good ones.

But Hursthouse does not take this as a serious objection.

A more unsettling objection which cannot be dismissed easily is Nietzsche idea that

> Good weak human beings might, perhaps, have the virtues as we know them; but good
> strong human beings have something quite different, appropriate to their different
> nature, namely at least some character traits that occur on the standard list of
> the vices.

Maybe we will have to update our ethics a bit after seriously considering it.
But not yet.

## The Supposed Threat from Darwinism

Bernard Williams has a view of human nature that in adding rationality to our social
animality, nature has produced a sadly flawed and divided creature,
which amounts moral nihilism.

He casts doubt on naturalism on the ground that 
Aristotle's conception of nature is teleological (natural entities have intrinsic purposes), 
whereas our modern, scientific one is not.
More specifically, naturalist ethics is a philosophy according to which 
there is inherent in each natural kind of thing 
an appropriate way for things of that kind to behave, i.e., Aristotelian. 
However, Darwinism tells us that 
there is no orchestral score provided from anywhere 
according to which human beings have a special part to play.

Note that the non-ethical evaluations discussed in previous chapters are
Aristotelian instead of Darwinian (reproducer of genes).
It has its own standard so Darwinism cannot say it is wrong.
There lies the difference between non-ethical and ethical evaluations --
other animals cannot form their own conceptions of living well but we can.

That is why there is a problem with ethical evaluation.
It must assume a good human, 
with respect to the 1st (survival) and 3rd (pleasure and pain) ends,
must be inclined to living well (eudaimonia).
It takes for granted that there is a strong harmony
of human capacities and needs.
This is an open questions in light of Darwinism.

However, we have argued in [chapter 8]({{< ref "/blog/on-virtue-ethics-08/" >}})
that virtue benefits its possessors without any appeal to scientific evidences.
This view implies human nature is harmonious and 
has existed and been debated for a long time.

Williams' objection is not a new idea. Many people thought that history
showed that humans are just a mess.

> If we really are, by nature, just a mess, then we are beings for whom no form of
> life is likely to prove satisfactory at all. Any individuals who flourish
> individually and socially are an extraordinary accident and so (please note) are
> those who flourish individually and anti-socially. ...
> There is no point in looking for a set of character traits that benefit their
> possessor, and no point in looking for a set of character traits which are the
> good-making characteristics of human beings. There aren't any.  Expressed in these
> terms, the view amounts to complete moral nihilism. (Note that

The belief that harmony is possible for human beings is the minimal requirement for
a virtuous ethical outlook.
That we try to better ourselves manifests this belief.

The idea that we are just a mess is a type of total moral nihilist similar to
completely rejection of the possibility of scientific discovery.

The conclusion of the book is that

> human beings are not by nature just a mess, but can, at best, through the correct
> moral education in their youth and then reflective, rational, self-modification,
> achieve a harmony that would enable us to live well, individually and socially.

Subject this to scrutiny by reading human history, some believe it is false.
However, many of us read the same history and do not think so.

We read history not to reveal human nature, 
but to confirm our belief that human nature which is waiting to be realized.
We read history and realize that so many people lived bad and miserable life
because they failed to possess and to exercise virtues.
Darwinism cannot imply that such a reading is just myth-making.

Believing that human nature is harmonious is part of the virtue hope.
To doubt it is to fall prey to the vice of despair.
But hope as a virtue needs to be validated.
The justification is that hope is necessary for our (ethical) practice,
just like believing the universe is intelligible is necessary
for practising science.
There is no other way.

The book ends with an inspiring note.

> Alternatively (or perhaps as well) we could stick with what we have—those facts
> about human nature and the way human life goes that support the claim that the
> virtues on the standard list benefit their possessor, and the reading of human
> history that ascribes our persisting failure to achieve eudaimonia in anything but
> very small patches to our vices. True, it is not easy to hold on to them sometimes;
> despair and misanthropy are temptations. But we should.

----

## My comments

The conclusion of the book is in many ways unsatisfying.
The author has chosen to reject evolution biology as essentially irrelevant
for naturalist ethics,
instead of attempting to update it in light of new scientific evidences.
In this respect, Peter Singer's [The Expanding Circle]({{< ref "/blog/singer/" >}})
is much more convincing book.

The defence against Williams' ethical nihilism amount to say "I can not prove it is
wrong. But it is too depressing so I will reject it."

Overall, the book shows that it is a pretty complicated enterprise to validate
virtues from a naturalist point of view.
The arguments put forward in defence of naturalist virtue ethics is quite
unsatisfying in my view.
This, together with the academic writing style, 
is perhaps why it has not reached a wider audience.
