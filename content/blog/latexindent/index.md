---
pubdate: "2021-02-20"
tags:
- programming
- LaTeX
title: Install latexindent on Ubuntu Linux
---


[latexindent](https://github.com/cmhughes/latexindent.pl) is a perl script that can help you format LaTeX files. It is shipped with texlive (at
least 2020 version). To use it you have to install some other perl packages.

@@colbox-blue
*Warning!!* Don't try to install latexindent in a [conda](https://docs.conda.io/en/latest/)
environment. It does not work at the moment of writing. Some perl packages cannot be installed.
@@

Step 1. Install perl
```bash
sudo apt install perl
```
Step 2. Install the perl package management tool [cpanm](https://stackoverflow.com/a/38257781).
```bash
sudo cpan -i App::cpanminus
```
Step 3. Install the needed [perl packages](https://github.com/cmhughes/latexindent.pl/blob/master/.travis.yml)
```bash
sudo cpanm YAML::Tiny
sudo cpanm File::HomeDir
sudo cpanm Unicode::GCString
sudo cpanm Log::Log4perl
sudo cpanm Log::Dispatch::File
```
Step 4. If everything worked, you should be able to see the help information of latexindent by
```bash
latexindent -h
```
