---
pubdate: "2022-08-26"
tags:
- climate-crisis
- environmentalism
title: The Heatwave of Summer 2022
---


A student recently told me, 
when they first read my post on [climate change](/climate/),
they found the issue very far away from them.
But when the recent heatwave (summer 2022) hits,
they realized that climate change might be the *defining issue of their generation*.

Here I share a few articles which may
hopeful wake-up more young people.

* [Heatwave in China is the most severe ever recorded in the world](https://www.newscientist.com/article/2334921-heatwave-in-china-is-the-most-severe-ever-recorded-in-the-world/)
* [China heatwave: scorching temperatures and severe drought – in pictures](https://www.theguardian.com/environment/gallery/2022/aug/24/china-heatwave-scorching-temperatures-and-severe-drought-in-pictures)
* [China's Heat Wave Is Hitting Its Agricultural Sector Hard](https://www.sixthtone.com/news/1011068/chinas-heat-wave-is-hitting-its-agricultural-sector-hard?source=channel_home)
* [A Shocking Visualization of the Heatwave](https://twitter.com/ScottDuncanWX/status/1561040349535113217?s=20&t=n_1kSWugh1NlmPdjtPRCGA)

![Heatwave 2022 Image](heatwave-2022.jpg)
