---
ShowToc: false
cover:
  caption: Refuse plastic!
  image: bunny-refuse-plastic.jpg
date: "2024-05-31"
tags:
- environmentalism
- book
title: 'No More Plastic: A Journey Towards Sustainability'
---

On April 21, 2024, I organized a book club to discuss the book [No. More. Plastic.](https://www.goodreads.com/book/show/40030019-no-more-plastic) by [Martin Dorey](https://martindorey.com/). To prepare for the event, I spent quite some time drafting a slide deck. Now I have translated the slides into Chinese. You can download the slides here:

* [English Version](no-more-plastic.pdf)
* [Chinese Version](no-more-plastic-zh.pdf)

## Summary of My Slides

## A Book Recommendation

Martin Dorey, an author, surfer, swimmer, cyclist, and environmentalist, wrote the book "No More Plastic." He was inspired to take action after discovering an area knee-deep in plastic bottles during his move to a quiet beach in 2009. His book aims to raise awareness about the plastic pollution crisis.

## Plastic Wastes Are Everywhere

Plastic waste has become a global menace, polluting every corner of our planet. From oceans and beaches to cities, national parks, and even remote mountain tops, plastic litter is omnipresent. One to two million tonnes of plastic enter our oceans yearly, and the Great Pacific Garbage Patch covers an area as large as 15% of China's total area.

{{<figure src="plastic-waste.jpg" caption="Plastic waste polluting the environment">}}

{{<figure src="plastic-ocean.jpg" caption="Plastic pollution in the ocean">}}

This massive accumulation of debris disrupts the marine ecosystem by obstructing
photosynthesis and harming algae and plankton. Moreover, plastic does not biodegrade but
breaks down into microplastics, tiny particles that persist in the environment for
thousands of years.

{{<figure src="north-pacific-subtropical-convergence-zone.jpg" caption="The Great Pacific Garbage Patch">}}

## Microplastics Are Everywhere

Microplastics have infiltrated every corner of our world, from oceans and mountains to the very air we breathe. Alarmingly, a 2021 study even found microplastics in human blood samples, highlighting the pervasiveness of this invisible threat.

{{<figure src="microplastics.jpg" caption="Microplastics in a sample of water collected off the coast of Hawaii">}}

## Is Recycling the Solution to Plastic Pollution?

While recycling is often touted as a solution to plastic pollution, the reality is far from encouraging. In the US, the overall plastic recycling rate has never exceeded 10% over the past 30 years. Even for polyethylene terephthalate (PET), the most recycled plastic, only a fraction is genuinely recycled into new bottles or fibers, with the majority ending up in landfills or the environment.

{{<figure src="PET-recycling.jpg" caption="The PET recycling process">}}

## What Can We Do to Reduce Plastic Pollution?

As consumers, we often find ourselves with limited choices when it comes to plastic-free packaging. However, there are simple steps we can take to reduce our plastic footprint:

1. Use reusable straws, cups, produce bags, shopping bags, and water bottles.
2. Opt for shampoo bars instead of plastic bottles.
3. Dine in at restaurants instead of ordering takeout.

Every piece of plastic we refuse contributes to a cleaner environment for generations to come.

{{<figure src="reuse-bags.jpg" caption="Using reusable bags is an easy way to reduce plastic waste">}}

By making conscious choices and embracing sustainable alternatives, we can collectively reduce plastic pollution and protect our planet for a better future.
