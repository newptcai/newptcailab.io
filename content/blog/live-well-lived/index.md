---
ShowToc: false
pubdate: "2024-12-26"
tags:
- Peter-Singer
- podcast
- advice
title: "Live Well Lived by Peter Singer and Kasia de Lazari Radek"
---

My favourite philosopher, [Peter Singer]({{< ref path="/tags/peter-singer" lang="en">}}),
has, like many in today's world, ventured into the realm of podcasting with [Live Well Lived](https://boldreasoningwithpetersinger.substack.com/p/season-1-of-lives-well-lived),
alongside his collaborator, Kasia de Lazari Radek.
Together, they have engaged in conversations with experts from diverse fields.
For instance, they [spoke with psychiatrist Murali Doraiswamy](https://boldreasoningwithpetersinger.substack.com/p/new-podcast-release-murali-doraiswamy)
about tackling global mental health challenges.
I found the following exchange both succinct and remarkably pertinent to the
podcast's theme ---

> Kasia de Lazari Radek: Okay, so now I should ask you, do you believe that we
> can train our brain to be happier on its own without any drugs, any
> psychedelics and any psychiatric drugs?
>
> Murali Doraiswamy: I think so, a hundred per cent. I think Eastern wisdom,
> again, the yin and the yang and other similar philosophies are good. You want
> to accept both the positives and the negatives, the good and the bad, the sad
> and the happy, you know, they go together. They are part of one whole and they
> are what unite to make you. I think meditation helps, being in nature helps,
> kindness helps, gratitude helps. I believe really focusing on collective
> happiness is the source of lasting happiness. I see patients who are very
> wealthy dying alone in the hospital. I see poor illegal immigrants surrounded
> by 30 family members who are clutching them and praying for them. And they are
> far more happy in their last moments of life than the super wealthy
> billionaire who has built his whole wing.
> 
The secret to living a well-lived life, it would seem, lies in these simple practices:
- Accept what is
- Meditate
- Go to nature
- Be kind
- Be grateful
- And focus on the greater good

If one embraces these actions,
one might feel they have truly lived a well-lived life.
