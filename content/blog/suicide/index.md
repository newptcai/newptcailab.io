---
pubdate: "2022-06-01"
tags:
- advice
- philosophy
- mental-health
title: On Suicide
---

⚠️  
This article is about suicide.
If you have suicidal thoughts, 
please seek help immediately from services such as

* [Counseling and Psychological Services (CAPS)](https://dukekunshan.edu.cn/en/caps) of DKU
* US's National Suicide Prevention [chat](https://suicidepreventionlifeline.org/chat/) or [hotline](https://suicidepreventionlifeline.org/)
* UK's Samaritan [hotline](https://www.samaritans.org/)

Someone I know recently committed suicide.
I was shocked, saddened, and confused.
It is one thing to know that many people die of suicide each year around the world.
It is another to be personally affected by it.
So I read and thought a bit on this topic. 
Here are some resources 
which I found helpful for understanding suicide.

## What to Know About Suicide

This is a [20-minute program](https://programs.clearerthinking.org/what_to_know_about_suicide.html)
offered by [Clearer Thinking](https://clearerthinking.org)
aiming at providing some basic facts about suicide.

Some of things which I learned include

* Suicide is far more common than people usually believe.
* The easiest way to prevent suicide is to remove the means (such as firearms).
* It reduces the risk of suicide if you ask someone if they are thinking about it.
* The majority of people who tried to suicide once but failed will not try it again.
* The time span from deciding to commit suicide to actually taking the action is
  often just a few minutes.

My take-away is that there are a lot things 
we can do to help people who are having suicidal thoughts.

## The Counsellor

I first read about Ittetsu Nemoto, 
a Japanese Buddhist monk who dedicated his life to suicide prevention,
in the book
[Strangers Drowning]({{< ref "/blog/strangers-drowning/#helping_people_should_not_be_the_ultimate_end" >}}).
Then I found an interview of Nemoto titled
[The Counsellor](https://web.archive.org/web/20210422110526/https://tricycle.org/magazine/ittetsu-nemoto/).
Nemoto's take on suicide is insightful, empathetic, and hopeful.
When he was asked why he wanted to do this line of work, 
Nemoto said --

> People who are in crisis and feel that they want to die have had many negative
> experiences. In actuality, I think that they have become full to the brim with
> accumulated experiences and ways of thinking, and they are on the verge of a sudden
> transformation. They are like caterpillars about to become butterflies, about to
> take flight, but because it is painful they try to suppress the pain with medicine,
> and they often believe something bad is happening. But I think that the self that
> has taken them through life up till now is in the process of being killed, and a
> new self, their real self, is being born. I want to be there at that moment of
> transformation and understanding, because through that I also understand myself.

Normally I don't write articles in Chinese.
But I translated this interview to [Chinese]({{< ref path="/blog/counselor/" lang="zh">}})
so it can reach a wider audience.

## Stoicism and Suicide

[Socrates vs Epictetus: on suicide](https://archive.is/20201228043610/https://medium.com/socrates-cafe/socrates-vs-epictetus-on-suicide-f7549a76a101)
by Massimo Pigliucci
compares the view on suicide of Socrates and that of Epictetus,
an ancient-Roman Stoic philosopher.
After reading it, I agree with Pigliucci that Socrates' opposition to suicide
is not so convincing.
In particular, 
he more or less committed suicide himself 
by refusing to escape from his prison.

On the contrary, although Epictetus holds Socrates as a role model,
his take seems to be more reasonable ---

> “Don’t believe your situation is genuinely bad — no one can make you do that. Is
> there smoke in the house? If it’s not suffocating, I will stay indoors; if it
> proves too much, I’ll leave. Always remember — the door is open.” (Discourses I,
> 25.17–18)

Here, the "open door" is a metaphor of suicide.
Epictetus is telling his students that 
they should evaluate their own situations
and decide if it is too difficult to stay in the house (stay alive).

We are not in other people's shoes.
How can we know that if life has become unbearable for them?
As much as we miss these who have taken the open-door,
at least we can take comfort in that they are not suffering anymore.

Of course, this is not say that Stoics take suicide lightly.
In [Stoicism and Suicide](https://web.archive.org/web/20210510022956/https://modernstoicism.com/stoicism-and-suicide-by-justin-vacula/),
Justin Vacula explores 
many advice from the ancient-Roman Stoic philosopher
Seneca on how to deal with difficulties in life.
For example, Seneca believes that we have the responsibility to help ourselves,
when helps are available, 
and that we should do it as soon as possible.

> Let us postpone nothing. Let us balance life’s account every day. The greatest flaw
> in life is that it is always imperfect, and that a certain part of it is postponed.

## Some Final Thoughts

We should try to help people around us to 
deal with life as much as possible,
whether they are suicidal or not.
Life can be hard for anyone.

Learn a bit more suicide.
It helps.

Take care or yourself.

And don't judge people who have commit suicide.
They must have suffered a lot.
