---
pubdate: "2021-07-31"
tags:
- philosophy
- book
- Buddhism
title: Eat Sleep Sit
---


![Eat Sleep Sit](eat-sleep-sit.jpg)

When I read the book [Strangers Drowning]({{< ref "/blog/strangers-drowning/" >}}),
I was very much impressed by the work of Ittetsu Nemoto, 
a Japanese Zen Buddhist monk, who dedicated his life to helping suicidal people.
The book also briefly described his four-year extremely harsh ascetic training 
in an especially strict monastery.
My interest was aroused.
"Why does someone willingly go through such unimaginable hardship willingly?"

This is why I picked up [Eat Sleep Sit: My Year at Japan's Most Rigorous Zen Temple](https://www.goodreads.com/book/show/5138505-eat-sleep-sit) by Kaoru Nonomura.
At the age of 30, Nonomura left everything behind and enrolled in Eiheiji, 
one of the most rigorous Zen monasteries in Japan to train as a monk.
The book is his memory of his time at Eiheiji.

## The hard life in Eiheiji

Nonomura and his fellow trainees' experience in Eiheiji 
can probably be described as abuse or even torture,
especially during the first three months.

First, they are deprived of their basic freedom and nearly all worldly comforts.
The food they were offered was so meagre that
some of the trainees have to be hospitalized.
In night, they are allowed only 5 and a half hours to sleep
so they are chronically sleep deprived.

Then there is the long sitting.
They have to spend hours prating sitting in a full lotus position
(the left foot on the right thigh and vice versa),
which induces tremendous physical pain after sitting for a long time.

But what is most unbearable
is that they have to follow many very strict and complicated rituals, 
including eating, sleeping, sitting, washing, bathing, excreting, and son on.
If a trainee make the slightest mistakes,
he would receive shouting, scolding, kicking, slapping from their instructors.

It is hard to believe that anyone sane would accept such ill-treatment voluntarily.
But after reading the book,
I come to appreciate Nonomura's motivation and 
the necessities of such a hellish training.

## The rules

The strict monastic rules at Eiheiji is not meant to torture trainees,
but is an important part of their training.

> Dogen, Eiheiji’s thirteenth-century founder, 
> laid down specific rules covering every aspect of monastic life.
> Monastic discipline consists in the scrupulous observance of those rules,
> and the least effort expended in doing so is, itself, 
> nothing less than the Dharma—Buddhist truth.

> Monastic discipline is not something done in order to gain enlightenment;
> rather, the faithful observance of monastic discipline is enlightenment,
> in and of itself.
> It cannot therefore be left to others,
> but must be performed with one’s own body and mind.
> In the words of Dogen: 
> “Dignity is itself the Dharma. Propriety is itself the essence of the house.”

Naturally Nonomura sometimes questions these rules.
But he soon realizes that this is pointless.

> Among all the thinking that human beings do, the question “Why?” has always been
> predominant. ... But in the course of each day’s round of activities at
> Eiheiji, the question “Why?” is virtually meaningless. Delving into the rationale
> for every single action would mean that nothing ever got done smoothly. What is
> essential is to accept without question what you are taught to do, and throw
> yourself into it entirely.

Modern people hate rules.
Take, for example, the opposition to mask mandate in many countries during COVID-19
pandemic.
Consequentialist moral philosophers, such as utilitarians,
think following rules blindly is stupid,
because doing so cannot bring the best consequence.

However, there is an advantage of rules --

> Adherence to simple moral rules,
> the import of which cannot be twisted or mistaken,
> relieves us from the burden of judging 
> when we are not in a fit state to judge.
> -- Peter Singer, The Expanding Circle

I *think* that is a good reason why Eiheiji enforcing these rules strictly.
It makes it much easier for the monks to live in a particular state of mind.
Maybe we can all establish a bit more rules and rituals in our own lives
to make them flow more smoothly.

## The "abuse"

How instructors treat trainees at Eiheiji does seem abusive for an outsider like me.
I doubt that I will be able to endure it for a day.

Such harsh treatment no doubt is to drill disciplines in new monks.
However, it severe another function -- to facilitate self-annihilation,
which is very hard for modern people.

> From the beginning, self-annihilation has been an important task imposed on Zen
> monks in everyday discipline. To cast aside the ego means to cast aside your
> selfhood, determinedly reducing yourself to nothing, all the while revering and
> obeying your seniors and carrying out your daily chores in perfect silence. Yet
> mere awareness of this requirement does not enable anyone to easily set aside
> something so important as himself.

> Those bound up in the self are broken down unrelentingly at Eiheiji through
> name-calling and thrashing. All the baggage people bring with them—academic
> achievement, status, honor, possessions, even character—is slashed to bits, leaving
> them to sink to rock bottom and thus cast everything aside.

The surprising result of such training is a profound relief.

> Every time I was pummeled, kicked, or otherwise done over, I felt a sense of
> relief, like an artificial pearl whose false exterior was being scraped away—an
> exterior that previously I had struggled fiercely to protect, determined not to let
> it be damaged or broken. Now that it was gone and I had nothing left to cover up or
> gloss over, I knew that whatever remained, exposed for all to see, was nothing less
> than my true self. The discovery of my own insignificance brought instant,
> indescribable relief.

When Nonomura have stayed for nearly a year in Eiheiji,
a new batch of trainees arrive.
Seeing them going through the same "abuse" which he received a year before,
Nonomura came to appreciate his instructors.

> Now that we were in the position of the instructors who had slapped us around and
> yelled at us when we first came, it was easy to see the strain they must have been
> under. It’s always easier to be a nice guy, someone with a perpetual smile on his
> face. Reflecting back on the events of the last year, I felt renewed respect for
> the senior trainees who had been so uncompromisingly hard on us, and inwardly I
> bowed my head to them.

I do not agree Buddhism's teaching that "Self is an illusion".
But as a [Stoic]({{< ref "/blog/stoic-singer/" >}}),
I do think that humans, including myself of course,
are too attached to our desires and fears.
So next time when someone says something rude
or jumps the queue to get ahead of you,
maybe we try to see it from the view point of an Eiheiji monk.
In other word, treat it as nothing but a training 
that wipes out one or two things that we cannot let go of.

## The hunger

New comers at Eiheiji get very little food. This takes a heavy toll on their health.

> First, the body would swell. ... 
> Urination increased abnormally in frequency.
> Wounds sustained to the knees or the soles of the feet during sitting took forever to heal.
> ...
> These symptoms are the result of beriberi, 
> a disease caused by excessive carbohydrate intake and vitamin B1 deficiency.

Such a diet is also a way to cure a type of modern "illness".

>  Of course, the hunger we felt was not the kind of starvation 
>  that leaves you hovering on the brink of death.
>  It was rather a kind of hunger or starvation of the spirit; 
>  brought up in an age of plenty, we were easy prey.

However, Nonomura made a very insightful observation about hunger.

> Every day, several trainee monks would gather when the trays came in, and proceed
> to fight over the leftovers.  I stood dazedly by, watching others snatch up morsels
> and cram them into their mouths by the fistful, feeling troubled and guilty for
> having seen something I should not have. To think that these were human beings—it
> was all inexpressibly sad. 
> ...
> The next moment, I too had grabbed a fistful of food and was stuffing it into my mouth.
> A sense of fullness spread inside me, yet at the same time I felt bleak and empty.

Most schools of moral philosophy, utilitarianism, deontology, virtue ethics,
invariably emphasizes the role of rationality in ethics.
However, as Nonomura notices, rationality alone is far from sufficient for one to 
act ethically, especially in harsh situations.

> Once you get away with something bad, without suffering even a reprimand, it often
> happens that you develop a new set of values accordingly.  In time, I forgot that
> initial sense of emptiness. Rationality didn’t enter into it. When people are
> locked into a world of unrelenting pressure, their sense of reason, I found, is all
> too vulnerable. And no amount of reason could fill an empty belly. Everyone was
> left with that most primitive of instincts fully exposed—the lust for food.

> Ascetic discipline at Eiheiji suppressed our raw desires to the point that the
> divide between body and spirit stood out inescapably, forcing us to face this
> dilemma head on.

I think an important lesson here is that without experiencing hardship,
there is no way for us to find out our true colours.
We may think we are smart, kind, just and brave.
But most likely we just deluding ourselves.
So, maybe from time to time,
those of us who live comfortably (in terms of material goods),
should willingly endure some pain and suffering for the sake of our character.

## The sitting

Why do monks at Eiheiji do a lot of sitting to the point that it incurs excruciating
pains. Nonomura's answer is this

> True, the longer I sat, the more my legs hurt, but in time I came to grasp the
> importance of this and of all else that happens in the course of sitting. Devoting
> oneself to sitting, getting used to sitting, and conquering the pain of sitting are
> all equally pointless. The only point of sitting is to accept unconditionally each
> moment as it occurs. This is the lesson of “just sitting” that I had absorbed after
> one year.

> I found great freedom in this way. Freedom in Zen means liberation from
> self-interest, from the insistent voice that says “I, me, my.” Liberation not from
> any external circumstance but from a host of internal mental or psychological
> states, including desire: herein lies genuine, untrammeled freedom.

In modern western countries,
Buddhist meditation is often considered a way to increase happiness.
For example, 
the first time I heard about meditation is from a popular book 
[10% Happier](https://www.chapters.indigo.ca/en-ca/books/10-happier-how-i-tamed/9780062265432-item.html) by Dan Harris.
This is a misconception as it is clear from the above cited passages.
The aim of sitting still is not to calm you down,
but to get rid of the resistance to reality at the moment.
I don't think sitting is the only way to achieve it,
but I do think that if we are a bit more acceptable about what happens to us,
paradoxically our lives will be a bit happier.

## Why

Why did Nonomura decide to go to Eiheiji in the first place?
This is only reviewed in the Afterword.

> For a long time, a breathtakingly long time, I walked aimlessly and alone through a
> season that was neither spring nor summer nor fall nor winter, an interstitial
> season.

> I wanted a firm reason to go on living, and then desire slowly changed to need. A
> way of life in which I could find no meaning was a threat to the value of my own
> existence.

> Sometimes in life you discover the importance of something only when you lose it.
> During my year in Eiheiji, when I was stripped of all I had and was, I came face to
> face with numerous questions.

Again, Nonomura's choice is perhaps neither the only
nor the easiest road toward finding a meaning in life.
But anything of any true value cannot be obtained without sweat and tears,
and the meaning of life is of the highest value of all.
I admire those who strive hard to find an answer.
I want to be one of them.


## Endnote

I *must* add that after the initial few months, 
the trainees treatment improves significantly.
The longer a person stays at Eiheiji,
the more freedom he has.
Most monks who stay at Eiheiji for long seem to live a pleasant life.

Nonomura thinks

> Who knows—perhaps real Zen discipline begins only when all restrictions have been
> taken away.  ... How each one responds is up to him, and the nature of his response
> changes the meaning of the remainder of his stay.

Enforced life style cannot last long.
True discipline must be choice people must make for themselves.
