---
pubdate: "2021-10-25"
tags:
- academic
- advice
- QA
title:  Why am I (still) an Academic?
---


Some students asked me why I decided to leave IT industry and become an academic.
I think the real question is whether I think it is a good idea to try to
become a professor. And the answer is a firm **no**.

I must emphasize that I am **not** advocating for abolishing PhD programs.
It is important for a society to push for advancement in science and humanity.
What I meant is that for the vast majority of people,
a career in academic is not a good choice.

I shall not repeat the arguments made countless times by others.
Here's a very good [article](https://80000hours.org/career-reviews/academic-research/) 
on why you should be very cautious in choosing a career in academics.

If I think that is the case, why did I get into academic and stayed until today?
The truth is that it is mostly accidental.
My original plan was just to do a one-year Master's degree.
There had been many times that I thought "This is it and I am done with academics".
But during these moments, fate had always pushed me to the other direction.
Most of my fellow PhD students have left or are planning to leave academics.
I am just a bit luckier than others.

That been said, I do think perhaps I have a somewhat unusual character
that suits academics a little bit better.
First, I like mathematics. It is an art form that I enjoys playing and sharing.
Then, there is the introvert in me who likes working alone for hours.
I am also a bit stubborn and hate quitting.
(This is often *not* a good thing.)
Last but not least, I do not care about my living conditions that much.
So it did not bother me to live for years on the meagre salary of a graduate
student. (A PhD student makes about the same money as someone who flips burgers in
McDonald in Canada.)

Another reason I have chosen to stay in academics is that 
I like teaching a lot.
Making my classes accessible and lively is a challenge which I enjoy.
Seeing young people learning things is a satisfying experience
which adds a sense of meaning to my life.

If you ask me that if I am happy being an academic now?
I will answer yes.
If you ask me that if I have would done it had I known what I was getting myself into?
Absolutely no.
