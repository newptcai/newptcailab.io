---
ShowToc: true
pubdate: "2021-05-02"
tags:
- book
- philosophy
- Peter-Singer
title: Five Books of Peter Singer which Changed My Life
---


[Peter Singer](https://en.wikipedia.org/wiki/Peter_Singer) is an Australian moral philosopher,
currently a professor at Princeton University.
He has written 17 books by himself, 
several of which I read since the beginning of the COVID-19 pandemic.
It is not an exaggeration to say that some of these books have changed my life for better.

## How Are We to Live?

![How Are We to Live?](live.jpg)

I am a mathematician, and a large part of my job is 
to write and publish research papers on mathematics.
This was a child-dream of mine 
and I do like studying and solving mathematics problems. 
However, after publishing enough papers, 
I came to realize that despite the large effort I put into them, 
there are very few people who have read what I wrote.
So, I had to ask myself, what is the point for all this, 
especially when the world is deeply in trouble?

In his book [How Are We to Live](https://en.wikipedia.org/wiki/How_Are_We_to_Live%3F), Singer
made the following observation,
which reminds me of my own life.

> ... as I became acquainted with a circle of New York professors and their spouses, I
> soon found that many of them were in daily psychoanalysis. Five days a week, eleven months of the
> year, they had an appointment for one hour, not to be broken under any circumstances short of a
> life or death emergency. ... I asked my friends why they were doing this. They said that they felt
> repressed, or had unresolved psychological tensions, or found life meaningless.

So what is the prescriptions Singer prescribes?

> ... [man] would see that to create a temple that lasts as long as the Parthenon has lasted, and is as
> justly admired for its beauty and the skill of its construction, is an accomplishment in which he
> could take justifiable pride. Reflecting more deeply on his place in the universe, he would resume
> his labours.

And what kind of a temple has Singer in his mind?

> If the temple *we* might build is a metaphor for all possible goals,
> then by making the world a better place,
> we will have made a small contribution to the beauty of the greatest of all temples.

I found this idea of building "a temple" beautiful and would like to give it a try. 
But where should I start?

## The Life You Can Save

![The Life You Can Save](life.jpg)

[The Life You Can Save](https://www.thelifeyoucansave.org/), 
the second book by Singer which I read, 
starts with a thought experiment--

> On your way to work, you pass a small pond. ... you are surprised to see a child splashing about
> in the pond. ... The child is unable to keep his head above the water for more than a few seconds
> at a time. If you don't wade in and pull him out, he seems likely to drown. Wading in is easy and
> safe, but you will ruin the new shoes ...  What should you do?

~~~
<center><p></p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/gGczdp0SE0c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
<p></p></center>
~~~

Most people will say that we should save the child. The value of a pair of new shoes cannot be
compared with the life of a child. 
But Singer then asks why is the case that each year 
5.4 million child die of preventable diseases worldwide,
while most of us looks away from it?
If each of us who live in wealthy countries is willing to give 
the amount of money comparable to a pair of new shoes,
we could have prevented a large portion of these meaningless death. 

There are many arguments against giving charities, e.g., 
the money may be wasted,
one should take care about one's family or countrymen first.
But in this book, 
Singer made a compelling argument that 
there *are* effective ways to help people living in extreme properties 
which costs us in rich countries very little.
It then follows that we have an obligation to do so.
Because otherwise, 
we must admit that we are the kind of people 
who do not want to ruin a pair of shoes to save a child's life.

It is for this reason, 
I begun to donate regularly to [effective charities](https://www.givewell.org/).
My contribution to the construction of the temple called "making the world better" is still very modest,
but I am making effort and it makes me happy that I begin to live a life of some purpose.

## The Most Good You Can Do

![The Most Good You Can Do](good.jpg)

The [Effective Altruistic (EA) Movement](https://en.wikipedia.org/wiki/Effective_altruism)
is a philosophy and social movement that advocates the use of evidence 
to determine the most effective ways to benefit others.
It has been influenced by Singer's early writings.
His recent book [The Most Good You Can Do](https://en.wikipedia.org/wiki/The_Most_Good_You_Can_Do),
is a direct argument in support of EA.

The theme of the book is similar to that of The Life You Can Save.
A difference is that this book also talks about how one can choose a career 
that has most positive impact on the world.
One such an option, called earn-to-give,
is to get a job that pays well 
so that you can donate much more to effective charities.
And this may be much more impactful than becoming a doctor in a rich country
where medical resources are already abundant.

Ironically, I first heard about earn-to-gave from an [essay](https://www.nytimes.com/2013/06/04/opinion/brooks-the-way-to-produce-a-person.html) 
in New York Times by David Brooks arguing against it.
Brooks was worried about
> turning yourself into a means rather than an end ... a machine for the redistribution of wealth.

However, I think Singer made an convincing counter argument in his book

> Those who earn to give are, to a greater extent than most people, living in accord with their
> values--that is, with their core conviction that we ought to live our lives so as to do the most
> good we can. It is hard to see any alienation or loss of integrity here. On the contrary, for
> people who share that conviction, integrity would be lost if they were to follow their passion to,
> let’s say, go to graduate school, write a thesis on Beowulf, and become a professor of medieval
> literature.

Before reading The Most Good You Can Do, 
I had not seen much of the point in acquiring money.
However, the book convinced me to take salary into serious account 
when I was searching for a new job last year.
Now I have a much better paid job and I plan significantly increase my annual donation.
And to be honest, being in a more financially secure position does take away some stress off.


## Animal Liberation

![Animal Liberation](animal.jpg)

Another pillar of the temple envisioned by Singer is to extend our concern to all sentient animals,
and that is why he argues for animal rights and veganism in [Animal Liberation](https://en.wikipedia.org/wiki/Animal_Liberation_(book)).
Although when I read it,
I was already a vegan as a result of reading [Eat Like You
Care](https://www.eatlikeyoucarebook.com/),
Singer's book gave me a strong push to stick with it.

For me, the most useful part of the book is Singer's description of the tremendous scale and cruelty
of animal suffering in laboratories and in intensive farming.
For example, I have never heard of "debeaking" before reading the book, 
which is a widespread practice of cutting off
the beaks of chicken to avoid them hurting each other.
The book writes

> today specially designed guillotinelike devices with hot blades are the preferred instrument. The
> infant chick’s beak is inserted into the instrument, and the hot blade cuts off the end of it. The
> procedure is carried out very quickly, about fifteen birds a minute. Such haste means that the
> temperature and sharpness of the blade can vary, resulting in sloppy cutting and serious injury to
> the bird ...

And that is just one paragraph in a long book.

For the past 12 months, I have kept such shocking images of cruelties in my mind,
and they helped me to overcome the occasional cravings for animal flesh.
Today, I am happy to report that I am healthy and enjoys my plant-based meals.
It also gives me a sense that I am standing for something right
and not just filling and emptying my belly every day.


## Practical Ethics

![Practical Ethics](ethics.jpg)

Unlike Singer's other books mentioned above, 
[Practical Ethics](https://en.wikipedia.org/wiki/Practical_Ethics) is more of an academic work.

This book first introduces an ethical principle called the Preference Utilitarianism,
which say the right thing to do is to

> ... weigh all these preferences [of other people] and adopt the course of action most likely to
> maximize the preferences of those affected.

It then applies this principle to answer questions such as "what is equality" and "why
is it wrong to kill".

In the end, I am not convinced, as Singer readily admits,
preference utilitarianism can offer satisfying answers to all questions in ethics.
However, it still has changed some of my long-held ideas about what is right and what is wrong.
I think the lesson I learned from this book is that 
we ought to examine our so-call "self-evident" beliefs 
before we can truly have firm opinions on many social issues.
Perhaps this is what Socrates means when he said, "an unexamined life is not worth living".
