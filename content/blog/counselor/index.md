---
pubdate: "2022-06-01"
tags:
- advice
- mental-health
title: Counselor
---


## About This Article

:warning:
This article discusses issues related to suicide.  
If you have thoughts of suicide, please seek professional help immediately:  
* DKU [Counseling Services](https://dukekunshan.edu.cn/en/caps)  
* [Suicide Crisis Hotline List](https://zhuanlan.zhihu.com/p/191355813)  
* [Resource Compilation | Free Psychological Counseling, Crisis Helplines, Psychiatric Hospital Appointment Services](https://zhuanlan.zhihu.com/p/343113502)

I came across an interview with Japanese monk Ittetsu Nemoto,
who has dedicated his life to preventing suicide.
Due to [personal reasons]({{< ref "/blog/suicide/" >}}),
I’ve translated it into Chinese in the hope that it may help more people.
The original article was published in the Spring of 2014 on [tricycle.org](https://tricycle.org/magazine/ittetsu-nemoto/),
and the author, [Winifred Bird](https://tricycle.org/author/winifredbird/),
kindly granted permission for me to translate and share it here.
See the translation [here]({{< ref path="/blog/counselor/" lang="zh" >}}).
