---
pubdate: "2022-06-01"
tags:
- 建议
- 心理健康
title: 辅导员
---


## 关于本文

⚠️
本文内容涉及自杀相关的问题。
如果你有自杀想法，请立即寻求专业的帮助 --
* DKU [心理咨询服务](https://dukekunshan.edu.cn/en/caps)
* [心理危机热线列表](https://zhuanlan.zhihu.com/p/191355813)
* [资源整理 | 免费心理咨询、心理热线、危机干预热线、精神科医院就医挂号](https://zhuanlan.zhihu.com/p/343113502)

本文系[温妮弗雷德·伯德(Winifred Bird)](https://tricycle.org/author/winifredbird/)对日本僧人根本一铁(Ittetsu Nemoto)的访谈。
原文于2014年春发表于 [tricycle.org](https://tricycle.org/magazine/ittetsu-nemoto/)。
因为一些[个人原因]({{< ref path="/blog/suicide/" lang="en">}})，
我将其翻译为中文，
以期望可以帮助到更多的人。

## 正文

![根本一铁](Nemoto.png)

题记：日本僧人根本一铁将预防自杀作为他毕生的工作。

日本的佛教僧侣一直以来都会处理与死亡相关的事务。他们是大多数人葬礼的主持者、悲伤者的辅导员，以及死后一系列追悼会的参与者。然而，很少有僧侣将直面自杀作为自己的工作(去年自杀在日本夺去了近 28,000人的生命)。根本一铁是个例外。 “如果一条道路通向自杀，我想尽我所能将人们引向相反的方向，”在岐阜县农村的稻田和森林丘陵之间的一座小寺庙大禅寺担任主持的根本说。

这位41岁的东京人在成长过程中与佛教没有特别的联系。他曾经是一个热爱冒险，喜欢骑摩托车，热衷于在迪斯科舞厅跳舞到深夜，并且喜欢打架的孩子。
在高中和大学他学习过西方哲学，然后从一份工作漂到另一份工作。
二十多岁时，他开始质疑自己的人生道路。
当他的母亲偶然指出报纸上的一个入门级僧侣工作的广告（广告说“招聘佛教僧侣”）时，
曾在空手道静修会中对坐禅产生兴趣的根本申请了。
几年后，为了更深入地接触佛教，他进入了岐阜山上一个僻静的临济禅寺。
那里的训练是极端的苦行僧：
僧侣们通过乞讨获取微薄的米饭和蔬菜作为饮食，长时间工作，并在严格的等级制度下进行冥想。
这些远远超出了他在外界所经历的生活，让根本以一种新的方式清晰的理解了他的思想和心灵的运作方式。
经过四年半的训练，他于2004年离开岐阜山，次年秋季成为大禅寺的僧侣。

曾经支撑他整夜跳舞的无尽能量现在使他能够为数千名处于困境中的人们提供咨询，
为那些自杀者的家人组织聚会，并举行无数的静修、朝圣和冥想课程。在这项工作引起了媒体的关注之后（去年夏天，他在《纽约客》上被报道），更多的求助请求如潮水般涌来。在这一切之间，他还要履行乡村僧人的日常职责，并在他寺庙旁边的一块地里种植有机水稻。即使对于根本来说，这也太过分了：到2009年，他患上了严重的心脏问题，接下来的几年里，他一直在医院里进进出出。

今天，根本继续他的工作，速度只是稍微慢了一点。然而，在一个秋天的早晨，在他和妻子照看的安静、干净的寺庙里，他似乎有世界上所有的时间可以说话，还有一百个故事要讲。

——温妮弗雷德·伯德（本文原作者）

**您是如何参与自杀预防和咨询工作的？**

我们周围也有人在烦恼，对吧？我不能忽视他们。这和我出家没有关系。我认为这是每个人都有的感觉，渴望帮助受苦的人。

但我们大多数人，包括我自己，都会忽略他们，除非他们是我们的朋友或家人。我的叔叔和我的几个朋友自杀了。自杀真的很难。凶手和被害人是同一个人，所以你不知道该怎么做。你不知道把你的愤怒导向哪里。伤口会伴随你很长时间。

自杀很难理解。比如，我的朋友，她看起来根本不是那种会自杀的人。她是一个好学生，擅长运动。她过着非常幸福的生活。我接到一个电话：她自杀了。我去看她，她完全变了——变成了皮肤和骨头。为什么？我那时有一种非常强烈的愿望，想了解为什么会发生这种事情。我现在仍然想知道——为什么一个人会停止生活？

**你写过关于自杀的社会根源的文章。你认为这是核心问题，还是与个人问题有更多关系？**

两者都有作用。在社会问题方面，（日本的）工作场所正在发生变化。日本曾经在大公司中实行终身雇佣。但这种系统已经分崩离析，取而代之的是合同工。人们每隔一两年就搬家，公司可以随意解雇他们。以前人们跟同事出去喝酒，聊聊天。现在一个人工作，一个人吃饭，一个人回家，一个人。真正交谈的机会正在减少。

随着互联网交流，尤其是社交媒体，变得越来越普遍，你无法展示自己的阴暗部分和你的痛苦。因为人们不会“赞”你的帖子。你只能展示自己有趣的部分，好的部分，并且戴上越来越多地面具。你的精神平衡开始瓦解。你的真实自我与你向他人展示的自我之间的差距越来越大。

为了解决社会问题，我创建了一个名为 https://ittetsu.net 的网站。如果人们感到沮丧或考虑自杀，有很多热线、心理咨询师和精神科医生可以求助。但你看看这些应对方式是否会引导人们过上更好的生活——好吧，人们回家了，他们又独自一人了，他们的环境没有改变。所以我想做的是创建一个网络，让许多不同的人，无论是否有自杀倾向，都可以聚在一起参加不同的活动，比如跳舞、瑜伽、唱歌或烹饪——重新开始生活。

**你如何帮助人们处理个人层面的问题？**

我会告诉你一个关于给我这个空调的人的故事。她是一名30岁的女性，在东京的一个政府机构工作。她变得非常焦虑，无法入睡。她在精神病院住了三个月，离开后继续看各种辅导员和医生。她很聪明，所以无论医生怎么说，她都比他们理解得更清楚。她把关于她自己的资料编成一大堆文件，并在她来看我时随身携带。这一切都很有趣，但都没有把她带向积极的方向。她的病情只会越来越差。

某一天，她开始看到异象。晚上她会看到一个人在她的床脚下，或者她会听到浴缸里的声音。最终，她通过一个纪录片节目了解了我的工作。我想她认为也许因为我是一名僧侣，我可以就她所看到的这些灵异显现给她一些建议。所以我听她谈论她的困难，但没有什么特别的问题彰显出来。由于我找不到她所执着的任何东西，我建议我们坐禅。坐禅是一种训练形式，可以减少对我们脑海中出现的思想和图像的执着，但很难让我们的思想进入空虚状态。当你静坐不动时，你会开始想各种各样的事情。这个想法是在你呼吸时观察思想经过。有时想法会消失片刻，然后你意识到你根本没有在想任何事情。

对于这个女人来说，练习坐禅是一次非常有趣的经历。她原以为自己总是很焦虑，但实际上焦虑的感觉一直在变化。它会出现，然后可能会出现一种安心的感觉。她已经很久没有这样的感觉了，没有任何顾虑。又或许担忧是存在的，但它们只是流逝而过。当她意识到这一点时，一个负担从她身上卸下了。

她一直承受着深深的痛苦。通过坐禅，她能够理解自己心中正在发生的事情——她自己一直在制造痛苦。她把那台空调给了我以表示感谢。

**您说即使您不是和尚，您也可能会做同样的工作，但听起来您能做的很多事情都是因为您是禅修者。你这样做是因为有需要，还是因为你的修行让你觉得你特别适合它？**

处于危机中并觉得自己想死的人有很多负面的经历。实际上，我认为他们已经积累了丰富的经验和思维方式，并处于突然转变的边缘。他们就像毛毛虫，要变成蝴蝶，要起飞了。但因为疼痛，他们试图用药物来压制疼痛，而且他们经常认为有不好的事情即将要发生。但我认为，把他们带到这个地步的那个自我正在消亡，一个新的自我，他们真正的自我，正在诞生。我想在转变和理解的那一刻在那里，因为通过它我也理解我自己。

禅修是极其困难的。 你承受着巨大的身体和情感压力。 处于压力之下，各种想法和感觉都会出现，但因为你经历了很多次，你才能克服它。 预防自杀是我训练或发现的元素之一。 大多数人不想做这种工作，因为听别人谈论自杀会让他们感到不舒服。 他们不知道该说什么，或者他们担心如果他们说错了人会死。但是我做这项工作已经很长时间了，我并不害怕。

**你劝告的人有没有自杀？**

有的。

**在我看来那很可怕，会产生很大的压力。 在那种情况下，你觉得你辜负了那个人吗？**

我认为在这种情况下不存在失败。 我与许多有自杀倾向的人交谈过——超过5,000人——但据我所知，只有一个人自杀了，尽管我与其他一些人失去了联系。 我第一次见到那个人是通过我大约十年前开始的一个针对自杀者的社交媒体网站。 他的妻子来自这个地区，所以我们在线下成为朋友。 我们三个人实际上一直在讨论如何降低他妻子在冰淇淋厂工作时监督的年轻女性的自杀率。 一天早上，她打电话给我，让我过来。 当我到达那里时，她的丈夫正躺在床上，就像在睡觉一样，但他已经死了。 这对我来说真的很震惊。 为什么他不能向我解释为什么他觉得他必须死？ 我以为他的情况一直在改善，但尽管他看起来很好，但他还是自杀了。

办完葬礼后，我非常沮丧。我觉得我做的事情并不好。我让他的家人知道他属于有自杀倾向的人群。我们谈到了他在生命即将结束时的想法和感受，他们感到了一些释然。如果他们不认识的僧人主持了这些仪式，他们就无法谈论发生了什么。通常，当发生自杀事件时，每个人都会试图隐藏它。他们会说这个人死于事故。有些家庭不为自杀的人举行葬礼，每个人都试图压制这些记忆。如果父亲自杀，孩子们会担心自己无法结婚或找不到工作。他们隐藏它，因此他们无法谈论他们的痛苦或悲伤。人们没有地方可以一起思考这些问题。有自杀倾向的人也避免谈论他们的感受，因为他们不想打扰他们所爱的人。

**你正在尽你所能防止自杀，但你相信人们有权选择死亡吗？ 它本质上是负面的吗？**

我想避免说自杀是坏的或错误的。 这样的说法真的能改变人们吗？ 我宁愿专注于问为什么人们认为自杀是最好的选择。

佛陀教导说，有两种痛苦：一种来自外部世界的痛苦，一种来自你内心的痛苦。 对于后者，只有您可以做任何事情。 这种痛苦从何而来？ 空无。 它不存在于婴儿身上——他们不会对未来感到焦虑，也不会担心自己长得丑。 观察空无所产生的思想和感受是佛教的信条之一。 为什么我们会受苦？ 根是什么？ 它从哪里开始？ 当我们看到这些问题的答案时，我们从空性中生起的痛苦又回到了空无。

**这些概念似乎自然而然地适用于自杀预防。但是，难道没有必要解决存在于外部世界的痛苦根源吗？**

我告诉人们，如果他们处于非常艰难的境地，他们不应该呆在那里。这可能与禅宗的思维方式不同，但如果你在一个你每天都被虐待的地方，或者一个对你来说真的不对的地方，你可以离开。然而，我确实从禅宗那里学到了一些东西。我们在寺院修行时，戴上草帽，挨家挨户念经和乞求食物——这是我们唯一可以吃的东西。有些人看到我们很高兴，但有些人很生气。当我修行时，我不得不乞求的地方之一是一家制作和服的服装店。我去的时候，老板娘往我身上泼水，很生气。我才刚刚开始修行，不明白去不受欢迎的地方是什么意思。于是我和一位高僧谈了话，他同意代替我去。

我在街对面看着会发生什么。但他只是做了我们一直做的事。这种情况发生了好几次，每次女人都会发脾气，叫我们滚。最后，我的同修对她说：“负面的关系也是关系。”她安静下来，然后开始抽泣。之后，她会将手工缝制的长袍送给这位和尚。我认为我们每天都来，低着头挨家挨户地走，不知何故激怒了她。但当我的同修没有以仇恨回应她时，她的内心突然发生了变化，以至于她想要支持我们。我对这种可能性非常感兴趣：直到某个时刻你一直在忍受的事情可以在瞬间转变为一种新的思维方式。在我的辅导过程中，我看到几个小时前还似乎濒临死亡的人变得精力充沛。看到这个，我觉得这个过程和我出家的时候很像。

**我通常认为禅僧只是坐着冥想，寻求自我理解。但是您非常积极地与社区互动。这是日本禅僧的传统角色，还是一种新思考你的角色的方式？**

在我们的修行中，我们切断了与外界的联系，以便集中地向内看。有些人为了成为禅师而继续这种训练多年，而更多的人继续成为佛教寺庙的僧侣，比如我。作为僧侣，我们参与创伤辅导，处理人的精神问题。在寺院里，我们在比喻意义上的黑暗中修行。但当我们在外面的世界工作时，我们也在修行。我们通过我们在黑暗中的漫长岁月中所理解的东西，并用它来引导那些正在受苦的人了解他们痛苦的根源。这两种训练形式没有太大区别。我认为两者都是必要的。

僧人的一个非常重要的任务是训练弟子。如果你不这样做，（佛教）传承就结束了。那些努力成为禅师的人成为这种训练的专家。另一方面，那些在外部世界工作的人正在与忙于工作或养家糊口的人打交道。我们并没有在完全的训练这些社区的成员，但我们可以在他们的日常生活中指出一些事情。所以从广义上讲，这些人也是弟子。

**你是否也以同样的方式看待你所劝导的那些有自杀倾向的人？**

我认为我们正在一起修行。有时我是老师，有时是学生。我们是寻找幸福的伙伴。
