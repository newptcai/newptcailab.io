---
pubdate: "2021-07-09"
tags:
- book
- effective-altruism
title: Lessons from Strangers Drowning by Larissa MacFarquhar
---


[Strangers Drowning : Impossible Idealism, Drastic Choices, and the Urge to
Help](https://www.goodreads.com/book/show/24611937-strangers-drowning) by 
[Larissa MacFarquhar](https://en.wikipedia.org/wiki/Larissa_MacFarquhar)
is a deeply inspiring and thought-provoking book.

It is mostly a collection of profiles for
**do-gooders** who go to extreme length to help strangers.
To various extent, they all have chosen to save "drowning strangers" 
instead of their own family,
which may be unsettling for many people.
Thus the book also discusses attacks mounted against do-gooders throughout history.

Many [good reviews](https://www.goodreads.com/review/show/1601365939) have been written about the book.
So here I will just list a few lessons which I learned from reading it.

## Absolute moral certainty is unattainable

Stephanie Wykstra (FROM THE POINT OF VIEW OF THE UNIVERSE) is a woman who takes
moral questions very seriously.

> Shortly after she turned thirty, Stephanie Wykstra decided that she no longer
> believed in unlimited altruism. ... She had grown up with a sense of utter moral
> clarity ... but this had gradually withered and rotted ... In her twenties, she
> embarked on a series of quests, looking for clarity of a different sort ... After
> the last quest failed, she accepted that she might never know again the unshakable
> moral certainty she had known as a child.

I have been fascinated by the question of "what is the right thing to
do" in recent years.  
Reading ancient and modern philosophical works on ethics
has changed my life in many ways.
However, I still do not have all the answers that I was looking for.
Eventually, I came to the conclusion, as Stephanie did, 
that absolute moral certainty is unattainable,
and that I will get nothing done if I keep searching.
It is better to do some good with unanswered questions than to do nothing at all.

## Helping people should not be the ultimate end

Ittetsu Nemoto (PLEASE REPLY TO ME AS SOON AS POSSIBLE) is Japanese Buddhist priest
who dedicated himself to help people with suicidal thoughts.

> Nemoto wanted to help suicidal people talk to each other without awkwardness, and
> so he created a suicide website. ...  People communicated with one another on the
> site, and they also wrote to him. ...
> He responded to everyone. He wrote back to all e-mails, and often, when he wrote, a
> reply would arrive within minutes, and he would reply to the reply. He answered all
> phone calls, day or night; many came in the night ... 

Such a heavy workload took a heavy toll on him.

> In the fall of 2009, he began to feel a heaviness in his chest. ...  his doctor
> told him he could die of a heart attack at any time. ...  All this time, the
> e-mails and the phone calls kept coming ... From the hospital, he wrote to his
> correspondents and told them he was sick. ... They didn’t care that he was sick ...
> Lying in the hospital, he spent a week crying. ... What was the point?

When he finally recovered, Ittetsu changed how he thinks about helping people 
which made his life easier and his work more efficient.

> He realized that, even if the people he spoke to felt nothing for him, he still
> wanted something from them. There was the intellectual excitement he felt when he
> succeeded in analyzing some problem a person had been stuck on. ... And then there
> was something harder to define, a kind of spiritual thrill in what felt to him ...
> ... Helping people should be nothing special, he thought. It should be like
> eating—just something that he did in the course of his life.

Ittetsu's story tells us that helping others 
itself should not be the end,
for the simple reason that it is impossible to fix every problem in the world.
If we single-mindedly aim at alleviate suffering,
we will only be overwhelmed and work ourselves to an early death.
To be an sustainable altruist,
there must be something in doing good that also helps us,
something that keeps us having a healthy mind and body.

## Be prepared for the bad results

Sue and Hector Badeau (THE CHILDREN OF STRANGERS) is a couple who have a lot of
compassion for children in distress. They tried the best to help.

> By the time they were four years out of college and four years married, they had
> had the two kids and adopted the two kids and thought their family was complete.
> ... and long before they adopted their last, twenty-second child, eleven years
> later, the four-child family they had imagined in high school was a distant
> memory ...

It took them a herculean effort to take care of so many kids.
However, when a family is this big, things will go wrong sometimes.

> Terrible, painful things happened over the years that they were not able to
> prevent—three children dead, two in jail, teenage pregnancies, divorces. ...

Reading their story, 
we may wonder if they have had fewer children,
some of these problems could have been avoided.
But we will never know for sure.

The lesson here, I think, is this -- 
Despite our best effort and intension,
helping other may have unexpected bad consequences.
In this case, we should not blame ourselves 
if this happens and take well-deserved pride in our effort.
That would be enough.
