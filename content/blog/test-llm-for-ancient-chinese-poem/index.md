---
cover:
  caption: Can LLM Write Poems?
  image: mountain-poem.webp
pubdate: "2024-10-26"
tags:
- Claude
- ChatGPT
- LLM
- AI
title: LLM and Tang Ying
---

For some random reason, I decided to test whether ChatGPT and Claude could write poems in an ancient Chinese style.
So, I sent Claude 18 poems by Tang Yin (唐寅, 6 March 1470 – 7 January 1524) and asked it to compose three new ones in Tang's style.
The initial results were impressive but sounded a bit modern, so I asked ChatGPT to revise them.
Afterward, I gave them back to Claude, asking it to change any words that sounded modern.
Below are four poems: one by Tang Yin and three by AI.
If you can guess which is which without Googling,
I would say you are an expert in ancient Chinese poetry.

《秋山怀古》  
山寺残灯夜未央，独倚栏杆望天长。  
一杯薄酒愁难散，几点寒星伴夜凉。  
古道萧萧风满院，秋山寂寂月疏廊。  
人间往事随流水，惟有青灯伴夜霜。  
  
《叹世》  
富贵荣华莫强求，强求不出反成羞。  
有伸脚处须伸脚，得缩头时且缩头。  
地宅方圆人不在，儿孙长大我难留。  
皇天老早安排定，不用忧煎不用愁。  
  
《闲游春村》  
晓雨新晴草色新，村南细路有人行。  
老翁担柴归径晚，幼妇浣纱临水明。  
野童牧牛青草岸，鸭队戏沙渡口汀。  
寻常百姓家居处，却是人间好境程。  
  
《夜坐》  
庭院深深月正明，独斟一盏酒微醺。  
竹影摇曳疑有客，蟋蟀啁啾似话心。  
年来往事堪回首，今夜清愁且慰身。  
不觉东方霜气重，衣衫微湿露华新。  
