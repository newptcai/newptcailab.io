---
ShowToc: true
pubdate: "2021-07-22"
tags:
- book
- philosophy
- Peter-Singer
- Stoicism
title: Why modern Stoics should read some Peter Singer
---


*I'd like to thank [Dr. Gregory Sadler](https://modernstoicism.com/author/gregory-sadler/) for helpful comments on this post.*

## Who is Peter Singer?

{{< figure src="../singer/Singer1.jpg" title="Peter Singer at The College of New Jersey, Oct 2009. Bbsrock, CC BY-SA 3.0, via Wikimedia Commons." >}}


I consider myself a follower of [Stoicism philosophy](https://iep.utm.edu/stoicism/). 
In recent years, I have read attentively ancient Roman Stoics as well as modern Stoic writers.
Their inspiring words have helped me tremendously in dealing with everyday challenges.

However, many of my questions about how to live a good do not have answers 
in either classic or modern Stoic literature.
Take, for example, what would be an ideal political system? 
As modern Stoic writer [Massimo Pigliucci](https://en.wikipedia.org/wiki/Massimo_Pigliucci) [pointed out](https://www.patreon.com/posts/stoic-ideal-or-22220796),
the ancient Stoics do not have a consensus.
Moreover, the world we are living in today is vastly 
more complicated than in ancient Rome.
To live a virtuous life in our time,
we must go beyond Stoicism and educate ourselves about how this world works.

Thus, I would like to recommend my fellow Stoics to read some [Peter Singer](https://en.wikipedia.org/wiki/Peter_Singer), 
an Australian moral philosopher,
currently a professor at Princeton University.
For nearly half a century, Singer has been writing about moral philosophy
for both academic and the general public.
Since the COVID-19 pandemic often confined me at home,
I read many of Singer's books in the past year, 
which I found easy to read and thought-provoking.
It is not an exaggeration to say that Singer has changed my life in many ways.

Singer is not a Stoic but a [utilitarian](https://iep.utm.edu/anci-mod/#SSH1cii). 
In other words, he believes that the right thing to do 
is always to maximize happiness for the largest number of people.
But we Stoics should keep an open mind and 
not to reject other people's ideas simply because they
belong to a different philosophical camp.
Consider what Roman Stoic [Seneca](https://en.wikipedia.org/wiki/Seneca_the_Younger) said --

> The thought for today is one which I discovered in Epicurus; for I am wont to cross
> over even into the enemy's camp — not as a deserter, but as a scout. (Letters to
> Lucilius, II.5)

([Epicurus](https://en.wikipedia.org/wiki/Epicurus) founded 
the philosophy [Epicureanism](https://en.wikipedia.org/wiki/Epicureanism), a rivalry
of Stoicism.)

Moreover, to convince more people, Singer often argues from our moral intuitions
instead of his utilitarianism. Thus, I found many of his ideas are compatible with
Stoicism.

What follows are my summaries of four books by Peter Singer 
that I think would benefit a Stoic striving to live a virtuous life.

## How Are We to Live?

{{< figure src="../singer/live.jpg" title="How Are We to Live?" >}}

I am a mathematician, and a large part of my job is to publish research papers on
mathematics. It is my child-dream and I do like solving mathematics
problems. However, despite the large effort I put into my papers, there are very few
people who have read what I wrote. 
So, one day I asked myself, what is the point of all
this, especially when the world is deeply in trouble?

In his book [How Are We to Live](https://en.wikipedia.org/wiki/How_Are_We_to_Live%3F), 
Singer made the following observation,
which reminds me of my own life.

> ... as I became acquainted with a circle of New York professors and their spouses, I
> soon found that many of them were in daily psychoanalysis. Five days a week, eleven months of the
> year, they had an appointment for one hour, not to be broken under any circumstances short of a
> life or death emergency. ... I asked my friends why they were doing this. They said that they felt
> repressed, or had unresolved psychological tensions, or found life meaningless.

So what is the medicine Singer prescribes for such an illness?

> ... [man] would see that to create a temple that lasts as long as the Parthenon has lasted, and is as
> justly admired for its beauty and the skill of its construction, is an accomplishment in which he
> could take justifiable pride. Reflecting more deeply on his place in the universe, he would resume
> his labours.

And what kind of a temple has Singer in mind?

> If the temple *we* might build is a metaphor for all possible goals,
> then by making the world a better place,
> we will have made a small contribution to the beauty of the greatest of all temples.

I found that the idea of building this long-lasting "temple" 
of relieving human suffering very beautiful and inspiring.
It is also very compatible with a crucial part of Stoic philosophy --
cosmopolitanism, 
the notion that all humankind belongs to a single ideal city.
As Seneca put it

> Let us grasp the idea that there are two commonwealths – the one, a vast and truly
> common state, which embraces alike gods and men, in which we look neither to this
> corner of earth nor to that, but measure the bounds of our citizenship by the path
> of the sun; the other, the one to which we have been assigned by the accident of
> birth. (On Leisure)

(See this [eassay](https://medium.com/stoicism-philosophy-as-a-way-of-life/stoic-cosmopolitanism-ideal-state-or-individual-ethics-9efbd6f35b19) 
by Massimo Pigliucci for more on Stoic cosmopolitanism.)

Thus, to practice the virtue of justice, 
we have to fulfil the duty of taking care not only our local community, but also all mankind,
since every human being is a fellow citizen of us.

But where should we start with the construction the temple?

## The Life You Can Save

{{< figure src="../singer/life.jpg" title="The Life You Can Save" >}}

The second book by Peter Singer which I read is [The Life You Can Save](https://www.thelifeyoucansave.org/).
It starts with a thought experiment.

> On your way to work, you pass a small pond. ... you are surprised to see a child splashing about
> in the pond. ... The child is unable to keep his head above the water for more than a few seconds
> at a time. If you don't wade in and pull him out, he seems likely to drown. Wading in is easy and
> safe, but you will ruin the new shoes ...  What should you do?

(*Drowning child* is now a somewhat famous argument. 
BBC made a [YouTube video](https://youtu.be/gGczdp0SE0c) to explain it.)

Most people will say that we should save the child. A pair of new shoes is much less
valuable than the life of a child.
But Singer then asks why each year 5.4 million
children die of preventable diseases worldwide, while most of us look away from it?
If we, who live in rich countries, donate a small percentage of our income
to charities can greatly help people living in extreme poverties around the world,
then we must do so.
Otherwise, we have to admit that we are the type of people 
who do not want to ruin a pair of shoes to save a child's life.

Singer has been making this argument for many years
and has received many ridicules (if YouTube comments are representative).
A few examples of more polite objections are --

1. People can believe whatever they want and we should not judge. 
2. People have the right to spend their money however they want.
3. People earned their money through hard working and talent. It's their fair share.

In the updated (2019) version of the book, Singer responded to these (and many other)
objections --
1. If we accept that people should believe whatever they want, 
   then why we feel outraged by people torturing dogs or cats? 
2. "You may have a right to spend your weekend playing video games, but it can still be true that you ought to visit your sick mother."
3. The American billionaire Warren Buffet once said 
   "If you stick me down in the middle of Bangladesh or Peru,
   you'll find out how much this talent is going to produce in the wrong kind of soil."

The book also discusses the importance of choosing effective charities to give,
as history is littered with well-intentioned charities wasting money for nothing.  
It tells the story of [GiveWell](https://www.givewell.org/), "a nonprofit dedicated to improving the transparency and
effectiveness of charitable giving."
What they found is that some charities 
can provide hundreds times greater benefit than others 
(in terms of lives saved per dollar).

So how much does it actually cost to save a life in the most effective way?
It is more expensive than a pair of shoes but still cheap comparing to a life.

> at present (2019), GiveWell estimates the median cost per death averted with
> Against Malaria Foundation's bed net program to be somewhere in the range of
> \$3,000–\$5,000. 

Thus, Singer concludes that the easiest way 
for many of us to add some bricks to the temple
of humanity is to simply to donate to effective charities. 
He also started an organization
[The Life You Can Save](https://www.thelifeyoucansave.org/) which helps people to
choose effective charities,
which I consult for my own donations.

I think from the cosmopolitanism point of view, 
a Stoic would agree with the above argument.
Moreover, for us Stoics, everything external, including money, fame, and health,
is just indifference, i.e., they do not have moral significance.
That means we should not cling to money obsessively but put them in virtuous use.
As Seneca put it --

> For the wise man does not consider himself unworthy of any gifts from Fortune's
> hands: he does not love wealth but he would rather have it; he does not admit into
> his heart but into his home; and what wealth is his he does not reject but keeps,
> wishing it to supply greater scope for him to practice his virtue. (On Happy Life)

What can be a more virtuous way to spend money than to do some good?

## The Most Good You Can Do

{{< figure src="../singer/good.jpg" title="The Most Good You Can Do" >}}

The [Effective Altruistic (EA) Movement](https://en.wikipedia.org/wiki/Effective_altruism)
is a philosophical and social movement that advocates using evidence 
to determine the most effective ways to benefit humanity.
Singer is considered one of its intellectual parents.
His 2015 book [The Most Good You Can Do](https://en.wikipedia.org/wiki/The_Most_Good_You_Can_Do)
is an argument in support of the movement.

The theme of the book is very similar to that of The Life You Can Save.
But one difference is that this book also talks about career choices.
Singer tells the story of Matt Wage, a former student of him.

> He was accepted by the University of Oxford for postgraduate study. ... but by then
> Matt had done a lot of thinking about and discussing with others what career would
> do the most good. This led him to a very different choice: he took a job on Wall
> Street ... One year after graduating, Matt was donating a six-figure sum—roughly
> half his annual earnings—to highly effective charities.  He was on the way to
> saving a hundred lives, not over his entire career but within the first year or two
> of his working life and every year thereafter.

Such a career path is called earn-to-give by the EA community.

Ironically, I first learned about earn-to-gave from an [essay](https://www.nytimes.com/2013/06/04/opinion/brooks-the-way-to-produce-a-person.html) 
in New York Times by David Brooks arguing against it.
Brooks is worried that this is

> turning yourself into a means rather than an end ... 
> a machine for the redistribution of wealth.

However, I think Singer made an convincing counter argument in this book.

> Those who earn to give are, to a greater extent than most people, living in accord with their
> values--that is, with their core conviction that we ought to live our lives so as to do the most
> good we can. It is hard to see any alienation or loss of integrity here. On the contrary, for
> people who share that conviction, integrity would be lost if they were to follow their passion to,
> let's say, go to graduate school, write a thesis on Beowulf, and become a professor of medieval
> literature.

Before reading the book, 
I had not seen much of the point in acquiring money.
However, the book convinced me to take salary into account 
when I was searching for a new job in 2020.
Now I have a better-paid job which I also like,
and I have significantly increased my monthly donations.
Moreover, to be honest, 
being more financially secured does take away some stress in life.

Massimo Pigliucci has [argued](https://howtobeastoic.wordpress.com/2017/07/05/stoic-advice-effective-altruism/) 
that a Stoic does not have a moral obligation to be an EA,
because when talking about "effectiveness" 
EA implicitly assumes a utilitarian ethics.
However, Stoicism is not compatible with utilitarianism --
the former thinks "virtue is the only good",
whereas the latter tries maximize the total happiness.

But I do not think EA and Stoics are rivalry sports teams from which we must choose one to join.
We can maintain that virtue is the only good and refuse to do
anything detrimental to our characters.
But at the same time, 
when we want to help others,
we should think carefully about how to use our
resources (time and money) to achieve the best outcome,
instead of just following our gut feelings.
In doing so, we are practising all four cardinal virtues -- 
1. Wisdom -- considering all data and arguments and ran the numbers
2. Justice -- caring about fellow humans wherever they are
3. Temperance -- stop wasting our time and money on superficial things
4. Courage -- to face the grim reality that billions of people are suffering unnecessarily everyday
How can this not be Stoic?

## Animal Liberation

{{< figure src="../singer/animal.jpg" title="Animal Liberation" >}}

Another pillar of the temple envisioned by Singer is to 
extend our circle of concern to all sentient animals.
That is why he argues for animal rights and veganism in [Animal Liberation](https://en.wikipedia.org/wiki/Animal_Liberation_(book)) (1975).
Although I was already a vegan when I read the book,
it strengthened my conviction to stick with it.

In the book Singer made a convincing philosophical argument for "All Animals Are Equal".
But the most useful part of the book for me
is the descriptions of the tremendous cruelty
animals are suffering in factory farming.
For example, I learned that debeaking  is a widespread practice in chicken farms.
--

> today specially designed guillotinelike devices with hot blades are the preferred instrument. The
> infant chick's beak is inserted into the instrument, and the hot blade cuts off the end of it. The
> procedure is carried out very quickly, about fifteen birds a minute. Such haste means that the
> temperature and sharpness of the blade can vary, resulting in sloppy cutting and serious injury to
> the bird ...

And that is just one paragraph in a long book full of such horror stories.

Ancient Stoics were not vegetarians (except [Musonius Rufus](https://iep.utm.edu/musonius/)).
As Jeremy Corter [wrote](https://modernstoicism.com/on-vegetarianism-and-stoicism-by-jeremy-corter/)

> Simply put, Stoicism and vegetarianism are two separate philosophies. Stoic
> teachings never denounced eating animals and, in fact, often stated that animals
> were there for us to use.

I agree that this is true if by Stoicism we mean exactly what Roman Stoics wrote.
However, we are living in a very different time now. Seneca wrote 

> Those who advanced these doctrines before us are not our masters but our guides.
> The truth lies open to all; it has not yet been taken over. Much is left also for
> those yet to come. (Letters from a Stoic, 33.11)

If the ancient Stoics could time travel to today 
and witness the cruelty in factory animal farming, 
I doubt they would say it is the just thing to consume animal flesh made in this way.
I implore my fellow modern Stoics to read the book and consider 
if eating meat for our gastronomical pleasure
at the price of so much suffering is not a gross indulgence.
(See also this [article](https://modernstoicism.com/should-a-modern-stoic-be-vegetarian-by-massimo-pigliucci/)
for Massimo Pigliucci's argument of why modern Stoics should be vegetarian.)

## What can we learn from Singer as a person?

Peter Singer is controversial figure.
Some people call a "murderer" 
because of his philosophical views on euthanasia and similar issues.
On the other extreme, 
people consider him as a secular saint who has done tremendous good,
although he denies that.

The more I read his books and learn about him, 
the more I see an exemplarily virtuous human being.
Not perfect, not a sage, but a decent role model.
We can learn some wisdom from his philosophical work,
some courage in his defence of unpopular ideas (animal rights),
some justice in his care for the poor and the suppressed (giving to charity),
and some temperance in his generosity -- he gives away about one third to one half of his income.

Virtue dost not belong only to Stoics.
We should seek it wherever we can find it.

## Further readings

* Unlike Singer's other books mentioned above, [Practical Ethics](https://en.wikipedia.org/wiki/Practical_Ethics) is more of an academic work. 
  It contains some of Singer's more controversial ideas.
* In [The Expanding Circle](https://en.wikipedia.org/wiki/The_Expanding_Circle),
  Singer tries to given an account of morality's organ from the point of view of
  evolutionary biology.
  I found the argument is fine, but also a bit speculative.
* Singer writes a [column](https://www.project-syndicate.org/columnist/peter-singer) on Project Syndicate
  on many issues. 
  [Ethics in the Real world](https://www.amazon.com/Ethics-Real-World-Essays-Things/dp/1543625789)
  is a collection of his essays.
* In a [2021 interview](https://www.newyorker.com/culture/the-new-yorker-interview/peter-singer-is-committed-to-controversial-ideas),
  Singer discusses some of the controversies around him.
