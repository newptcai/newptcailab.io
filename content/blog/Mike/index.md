---
pubdate: "2022-08-03"
tags:
- family
- memorial
title: Farewell, Mike!
---


My uncle [Mike (Michael J O'Connor)](https://www.cailegacy.net/old-photos/) 
passed away this week.

I have only met Mike once in person when I visited Atlanta a couple years ago.
It was a brief visit, but Mike's hospitality made me feel very welcomed.
He took me for lunch at Whole Food
and had a walk with me in a beautiful park.
The morning when I was having breakfast with him at his home,
he showed me deer in his backyard 
with the excitement of a child.
That was one of my fondest memories.

Mike, I will miss you and I wish you rest in peace!
