---
pubdate: "2022-08-28"
tags:
- advice
- book
title: A Book About Difficult Parents
---


The [Sixth Tone](https://www.sixthtone.com/) is an English news website covering stories in China.
A DKU (Duke Kunshan University) student just won a writing contest 
organized by them with an essay titled 
[Moxa, Receipt, Money, and Generations](https://www.sixthtone.com/news/1011055/moxa%2C-receipt%2C-money%2C-and-generations).
It is a candid and touching account on how her father's obsession with moxa
damaged their family.

This makes me think that perhaps many of our students also similar family issues.
If you are in such a situation, 
reading the memoir 
[The Glass Castle](https://www.goodreads.com/book/show/7445.The_Glass_Castle)
by Jeannette Walls
might be helpful.

The book is one of the most fascinating memoirs 
which I have ever read.
It recounts her extremely unusual upbringing in a dysfunctional family.
Her parents were both smart, creative, and loving.
But they were also utterly irresponsible.
Jeannette's father is an alcoholic.
And 
Often, it is the Jeannette and her siblings who took care of their parents.
The book made me feel sorry for Jeannette and angry for the parents.
But I could also see that it was them 
who brought Jeannette some of her happiest memories.
And these hardship Jeannette had to endure when she was young
made her the resilient and resourceful women she became.
The lesson I took from the book is this --

@@important
While we remember the many times our parents failed us, 
we should also remember the times when they helped us.
We cannot choose what kind of parents we have.
We cannot choose how they treat us.
But we can choose how we treat them.
And we can also decide how we face the challenges our parents brought us.
@@
