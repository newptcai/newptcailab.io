---
pubdate: "2021-06-06"
tags:
- Montreal
- travel
title: A Song of McGill Ghetto
---


([The Ghetto](https://en.wikipedia.org/wiki/Milton_Park,_Montreal) is a neighbourhood
next to McGill University where I had lived for many years. 
It is a bit messy, but also convenient, beautiful and lively.)

{{< figure src="01.jpg" caption="The shining building of lottery company." >}}

{{< figure src="02.jpg" caption="Cherry trees blossom every May." >}}

{{< figure src="04.jpg" caption="There are also other trees," >}}

{{< figure src="05.jpg" caption="as well as buildings with symetry." >}}

{{< figure src="06.jpg" caption="Pick up free things on your way." >}}

{{< figure src="07.jpg" caption="Squerals and" >}}

{{< figure src="08.jpg" caption="dogs are OK." >}}

{{< figure src="09.jpg" caption="PhDs and homelesses share a cafe," >}}

{{< figure src="10.jpg" caption="from where grocery store is not far away." >}}
