---
pubdate: "2024-12-18"
tags:
- book
- literature
- science-fiction
title: "The Mercy of Gods by James S.A. Corey"
---

I just finished [The Mercy of Gods](https://www.goodreads.com/book/show/201930181-the-mercy-of-gods)
by James S.A. Corey, best known for the *Expanse* series.

It's a space opera about a team of top biologists who find themselves taken
captive by brutal alien invaders.
Forced into servitude, they must decide how  to navigate life under these new,
hostile conditions.
The setup is rich with tension and moral dilemmas.

The first part of the book reveals certain dynamics in the academic world ---
competition for discoveries, power plays, and the undercurrent of personal ambitions.
Even in pursuit of something noble, like advancing knowledge,
human frailties often take centre stage.

The story also carries a poignant reminder of life's unpredictability.
One day, you're pursuing your research and winning awards;
the next, your world is burned, and you become a prisoner.
I felt for the characters as they mourned their lost loved ones
and former lives.
But I couldn't help noticing their stubborn attachment to old habits and grudges,
even as their entire species faced potential extinction.
It's deeply human, isn't it?

Reading this, I realized how much easier it is for us ---
authors, readers, all of us ---
to imagine an external apocalypse, like an alien invasion,
than to confront the slower, self-inflicted unravelling of civilization.
Still, who am I to judge?
I'm here, too, losing myself in a fast-paced space opera,
indulging
in its thrill, maybe as a way to escape reality for a little while.

Let's have some fun before the fire from the sky crashes down.
