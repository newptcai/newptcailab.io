---
pubdate: "2021-05-05"
tags:
- academic
- QA
- advice
title:  Should you do a PhD in science?
---

{{< figure src="math.jpg" title="Photo by ThisIsEngineering from Pexels" >}}

## The joke

A friend sent me an article titled 
[Is Science a Pyramid Scheme?](https://vixra.org/abs/2001.0595) 
posted on [viXra](https://vixra.org), 
an e-print archive.
The abstract is as follows --

> A grievance expressed by some PhD students and Postdocs is that science works like a pyramid
> scheme: Young scientists are encouraged to invest into building scientific careers although the
> chances at remaining in science are extremely slim.  ...
> The super-prolific authors at the top of the pyramid ... are usually heads of large institutes
> with many subgroups and large numbers of PhD students, while the bottom of the pyramid is
> populated by PhD students and Postdocs ...  A new index, the Ponzi factor, is proposed to
> quantify this phenomenon. 

The author of the article is named as Charleeze P. Ponzi, 
which is also the name of the person who invented [Ponzi
scheme](https://en.wikipedia.org/wiki/Charles_Ponzi).
Since it is unlikely that the ghost of Ponzi (died in 1949) who wrote the article,
perhaps the article is meant to be a joke.

## The fact

However, the author does have a point in saying that "the chances at remaining in science are
extremely slim".
Take mathematics as an example, 
according to American Mathematical Society's [latest annual survey](http://www.ams.org/profession/data/annual-survey/phds-awarded), 
during the 2017-2018 academic-year

> In mathematical and statistical sciences, 1,960 PhDs were awarded by the responding departments 
> ...

Meanwhile, during the same period,
[985 tenure-track positions in mathematical science](http://www.ams.org/profession/data/annual-survey/recruitment)  
were offered across USA,
among which 775 positions were filled.
However, among these positions, only 296 are from departments offering PhD degrees,
i.e., from places where you are supposed to do a lot of research besides teaching.
And these numbers have been relatively stable for years.
Thus, roughly speaking, 
a math PhD student in USA has at most 40% chance of becoming a tenured professor, 
but only 15% chance if you also want do a lot of research besides teaching.

15% does not sounds too bad.  
But I probably overestimated this number, since PhD of other
majors, like myself who studied Computer Science, applies jobs in math departments.
And PhD produced in other countries also compete for these positions.

These numbers only applies to USA. 
It is likely that in less affluent countries there are not as many positions.
For example, when I applied a position in Sweden,
there were around 40 applicants in total.

Moreover, the situation of the whole field of natural science may be bleaker.
By [one estimate](https://academia.stackexchange.com/a/17686/11584),
about 2.2% German PhD students in natural science become professors.

## The conclusion

I used to joke to PhD students that they should strongly recommend their friends to not to do a PhD, 
because 

1. research is indeed hard;
2. this reduces competitions they fill in future job market.

However, I do seriously think people should think twice before they apply for PhD positions.
They must realise that it is unlikely they will become professors.
So, before applying, instead of imagining themselves becoming the next Einstein,
people should think **very hard** about how doing a PhD can help them getting jobs outside the academic.
Otherwise, five years of hard work may lead to nowhere and the result can be devastating.

That being said, I think Sweden, where I did my postdoc, makes studying for a PhD less risky.
First, in Sweden, university only accepts PhD students whose funding has been guaranteed for fives
years.
This protects students from running into financial difficulty when they are close to finish line.
(When I studied in Canada, 
a fellow PhD student had to use GoFundMe to support himself during the last year of his study.)

Secondly, PhD students in Sweden earn about 30k SEK (3500 USD) per month, 
which is not much but enough to have a reasonably comfort life.
Thus, for most of students I met in Sweden,
graduate school is just a place where they can explore their intellectual interest
without worrying about their financial situation.
They enjoy their time and they get well-paid industry jobs in the end.
For me, that does not seem to be a bad deal at all.

Maybe other countries can learn a bit from Sweden.
And maybe you can consider doing a PhD there.

----

## Comments from others

### EA forum

This blog was posted on [Effective Altruism
Forum](https://forum.effectivealtruism.org/posts/5zedDETncHasvxGrr/should-you-do-a-phd-in-science).
You can see some discussions there.

### Brenhin Keller

*Brenhin Keller wrote the [following](https://julialang.slack.com/archives/C767M3VB3/p1620162579143500?thread_ts=1620154704.140400&cid=C767M3VB3)
comment which I think is thoughtful.  I repost it here with his permission.*

Yeah, seems like a mix of jokey elements with potentially legitimate grievances.

There are definitely still bad advisers out there who take credit for their students’ work, but overall authorship norms have changed hugely since, say, the early 1900s. Now it’s rare for a student to not be first-author on their papers, when in the past the advisor often took that spot.

I think the complaint about heads-of-institutes taking coauthorship on everything is also legitimate — though, as I understand it, this sort of practice is also becoming less common these days.

Along a similar line to the head-of-institutes issue, if you look at the very tail end of the histogram of # of publications per author, you’ll inevitably find a set of people with so many publications that it’s nearly impossible for them to have contributed meaningfully to each one. Many of these people probably are up to something sketchy — gaming the system in some way — but often are not actually very well respected by their peers, and are certainly not representative examples.

Excluding these above issues, the other components seem to mostly boil down to:

* (1) Scientists with more students (and students of students) have more publications.
* (2) Most PhD students cannot become faculty

Well, (1) is going to be true in any system where more established, more famous scientists with established, famous students of their own write more papers (due to e.g., more funding, more people wanting to collaborate with them, etc.).

This would be a problem if we take the incredibly cynical view that in general there is either a zero or negative correlation between scientists who are well regarded by their peers (and thus are successful in grants, paper review, recruiting students, and finding collaborators) and actually doing good science — but not necessarily so much of a problem otherwise.

Meanwhile (all PhD students are doing so to become tenured faculty, (B) that most PhD students have been misled into believing that they can all become tenured faculty, and (C) that if they are not successful this is all “wasted time and effort.” If all these things are true, that would indeed be fairly crummy. I did a PhD just because I thought research was fun and the PhD position paid enough to live. Doing it to get famous, or to make a lot of money, or under the impression that you’re guaranteed tenure (hopefully no one actually believes this one, given how obviously false it is!) are all bad ideas.

However, perhaps more to the point you could say the exact same thing about anything where people are competing for a limited number of spots.. e.g. sports, music, acting, whatever..

> Is [sports|music|acting] a pyramid scheme? A large number of aspiring junior [atheletes|musicians|actors] must enter the field in order to find the few top [NFL players | rock stars | SAG actors], and to maintain the underlying economy of [coaches, etc.]. This is sustainable only if the vast majority of them later lose their investment of time and effort.

Why is this seemingly often presented as a problem for science but not these other fields then? To me this seems to implying either a belief that (A) these other fields are more important to society than science or (B) that while some people may train to become better [atheletes | musicians | actors], everyone is always equally good at science, and there is no point competing for the latter. I have a hard time believing either of these.

Overall, these sorts of complaints seem to go hand in hand with an incredibly cynical view of science, and a view that doing pure research in itself is not actually very important — but is just a way to get a secure, semi-well-paying job. (edited) 
