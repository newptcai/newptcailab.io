---
pubdate: "2021-12-26"
tags:
- environmentalism
title: What Have I Done in 2021
---


In this year I moved from Sweden to Canada, and the back to Kunshan, China,
where I started working at Duke Kunshan University.
I also got a girlfriend recently. 😄
(2022 Update -- The relationship didn't last long. 😢)

Although occasionally I had a bit egg or diary products when there is no choice,
this year I am still mostly a vegan.
This has been one of the best decisions of my entire life.

And I have kept more or less my promise of donating.
Here's a breakup of my donation of this year.

| Category                     | Sum - Amount in CAD |
|------------------------------|---------------------|
| Animal Welfare               | 43.17               |
| Climate Change               | 914.54              |
| Gift                         | 50.41               |
| Global Health                | 914.78              |
| Open-source Software         | 99.3                |
| Total Result                 | 2022.2              |

So in total, I donated 2022 CAD, which is about 1386 EUR, or 1568 USD, or 9923 CNY.
My aim for 2022 is to increase that by 50%. 😉
