---
pubdate: "2021-04-20"
tags:
- literature
- book
title: The Remains of the Day (1989)
---


*(Spoiler Alert. Don't read this if you have not read the book.)*

Last week, I finished a 1989 novel 
[The Remains of the Day](https://en.wikipedia.org/wiki/The_Remains_of_the_Day) 
by [Kazuo Ishiguro](https://en.wikipedia.org/wiki/Kazuo_Ishiguro),
which I found both captivating and thoughtful.
[A film adaption](https://en.wikipedia.org/wiki/The_Remains_of_the_Day_(film)), 
staring Anthony Hopkins and Emma Thompson,
is also worth watching.

----

The protagonist Stevens is a [butler](https://en.wikipedia.org/wiki/Butler) 
(a servant who manges a big house). 
From 1930s to 1950s, 
he works in a mansion in England,
serving Lord Darlington, an influential British politician during the 30s.
The main reason that I like the book is that the story of Stevens 
offers both great inspirations and stern warnings regarding 
how one should live a life.

From the beginning, Stevens determines to be a great butler.
With unyielding loyalty to lord Darlington,
he has devoted his entire life to his job for decades.
To be able to keep doing his work,
he even gives up a budding romantic relationship with a colleague,
which he comes to regret in his later years.

However, for Stevens, being a *great* butler means something even more than just dedication. 
He believes
> If one looks at these persons we agree are ‘great' butlers, ..., it does seem to me that the
> factor which distinguishes them from those butlers who are merely extremely competent is most
> closely captured by this word ‘dignity'.
By this standard, Stevens no doubt can call himself a *great* butler.
Throughout the book,
he has maintained his dignity intact,
even under enormous emotional stress.
I found Stevens' this character quite remarkable.

What is more impressive is the reason 
that Stevens aspires to be a *great* butler.
He does not merely want to be excellent at doing something he likes --
as today's career consults often recommend to young people --
but to do something good for the mankind through his work.
In Stevens' option, his generation of butlers are

> ...ambitious, in a way that would have been unusual a generation before,
> to serve gentlemen who were, so to speak, furthering the progress of humanity. 

He chooses Lord Darlington as his master for exactly this reason --

> I cannot see that his lordship is doing anything other than that which is highest and noblest. He
> is doing what he can, after all, to ensure that peace will continue to prevail in Europe.

By helping his lord organizing conferences and meetings,
Stevens contributed in Lord Darlington's effort in keeping the peace of Europe.

----

Many readers may think that Stevens has wasted his life.
He himself wonders how things would have turned out to be, 
if he had showed Miss Kenton's some affections.
Yet the true tragedy of Stevens, in my option, 
is his ignorance and his blind trust in Lord Darlington,
who was manipulated by Nazi Germany to influence British foreign policy in their favour.
When he is asked by a friend that if Lord Darlington is been tricked,
Stevens replies

> I'm sorry, sir, I'm afraid I have not noticed any such development.

And his friend remarks
> But I suppose you wouldn't, Stevens, because you're not curious. You just let all this go on
> before you and you never think to look at it for what it is.

Later, Stevens' defence for himself is this --

> How can one possibly be held to blame in any sense because, say, the passage of time has shown
> that Lord Darlington's efforts were misguided, even foolish? Throughout the years I served him, it
> was he and he alone who weighed up evidence and judged it best to proceed in the way he did, while
> I simply confined myself, quite properly, to affairs within my own professional realm.

This attitude of "I simply confined myself, ..., to affairs within my own
professional realm", 
is as bad as giving up one's perfect eye sights.
The uttermost important thing for one's life, 
in my opinion,
is to learn what this world is really like 
and to form well-considered opinions about important issues.
Without such an effort, there is a huge risk of wasting one's life in pointless efforts 
despite the best intentions,
just like going south will take you a very long time to reach the north pole.

Of course, we do not have a crystal ball for the future 
and we may still be mistaken on a matter after carefully considerations.
But it is only then we can truly say that we cannot to "be held to blame in any sense".
And it is then that we can say we have lived an "examined life".
That is why I feel sorry for Stevens.
