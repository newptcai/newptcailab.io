---
pubdate: "2021-03-19"
tags:
- film
title: A Sun/阳光普照 (2019)
---


[A Sun/阳光普照 (2019)](https://www.imdb.com/title/tt10883506/) tells the story a Taiwanese family
with two late-teenage sons who are very different but equally troubling for their parents.

The film is gut-wrenching. (Tissues are advised for softhearted viewers.) It shows how difficult
situations can be for parents whose beloved children did something horrible. In the film, the mom
and dad face such problems quite differently. The mom offers carrots -- she is forgiving, patient,
tolerable and supportive, however bad things are. For her, whatever the boys have done, they are
always just her little boys. The dad, on the other hand, give sticks.  The motto he tried to drill
into heads of his children is "Seize the time and take the direction." When they failed doing so, he
was burnt with rage.  Yet, he also made great sacrifice for the boys.  These two characters fell way
to authentic. You must have must a few parents in real life.

The bright sun (the center of our solar system) is a symbol which appears in center of several
shots.  And often scenes are set under bright sunshine which gives the film a colorful and pleasant
ascetic.  However, the sun deeper meaning is lost on me.  All I can see is that 
1.  weather in Taiwan seems good;
2.  good weather does grantees good life;
3.  the title *A Sun* is a pun for English speaking audience. (Can you guess it?)

However, because the film tries to encapsulate too much in it, it becomes very long and a bit
unfocused. Half way through, you may begin to wonder where this all goes.  And it does deliver a
satisfying ending that justifies your patience. The story is probably more suitable for a TV mini
series than for a film.

A greater issue of the film is that it seems to glorify parents who does not mind crossing some
lines for their children. In reality, such parents perhaps do not know what is really good or bad
for their kids. Just think what [Cersei Lannister](https://en.wikipedia.org/wiki/Cersei_Lannister)
did make her sons on thrones and how they ended up.
