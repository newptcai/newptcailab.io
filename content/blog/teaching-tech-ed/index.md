---
pubdate: "2022-02-28"
tags:
- teaching
- tech
title: Why I don't reply emails (Or effective use of edstem.org in teaching)
---

![Screenshot of edstem.org](ed.png)

In the past two months when I taught Linear Algebra, 
I used the Q&A website [Ed (edstem.org)](https://edstem.org) 
to answer questions from students.
My main motivation for using Ed is to avoid answering the same questions repeatedly.
If a student post a question on Ed and I reply, everyone else can see my answers.
This goal is mostly achieved.
However, I also want to train students to write about mathematics formally,
and to foster a sense of a community among students.
These goals turned out to be a bit more difficult.

Here's an account of what I think that I did right,
and what I may want to change next time.

## I do not reply any email ✉️

From the very beginning I announced that *all* questions must be posted on Ed
and I will not answer any emails.
This did the trick and student usage of Ed had been stable from the beginning,
as can be seen from the statistics.

![Statsitics of Ed participation](ed-stats.png)

However, I also allowed students to write me emails when privacy is a concern.
This is a mistake 😩. 
Very often students simply email me regardless of it is actually a private matter.

In retrospect, it is much better to simply *not* to reply any email at all.
Ed allows [private](https://edstem.org/help/using-ed-discussion#options) questions 
which only staffs (me and my TA) can see.
Students can also post [anonymously](https://edstem.org/help/using-ed-discussion#options).
That should be enough to protect their privacy.
And if private a post is worth reading for other students,
you can turn it public.

Also, it turned out that instead of using Ed, 
many students ask the TA questions through WeChat,
which defeated the purpose of not replying emails.
I banned it later in the course,
but perhaps I should have done it from the beginning.

## Use bigger carrots 🥕

On Ed, you can ❤️ posts, comments, etc., just like any other social media.
To encourage students to participate in discussions,
I initially announced that the top 10 most hearted students will get 0.1 
bonus point each.
This turns out to be insufficient to motivate students and they complained.
So after the Chinese New Year holiday,
I increased this to 0.3 points, and I promised to also give bonus to top 10
askers and answerers.
This resulted in a significant increase in number of questions and answers on Ed, as shown below.

![Number of questions posted](ed-curve.png)

However, 7 students, about a quarter of all, never posted anything.
About half of them never answered any questions.
So the next time,
instead of giving bonus points, perhaps I will simply *require* each student to at least
post three questions and answer questions.

As a side note, about half of the top 10 answerers 
as well as half of the top 10 askers,
are girls.
Considering that they account for only 1/4 of my students,
this is extraordinary.

## Using also sticks 🏏

I also use Ed to make announcements, for which students will get email notifications.
One feature of Ed is that it allows you to see how many unique users have seen
it, as shown in the picture.

![An annoucement on Ed](ed-announcement.png)

This also tells me that four students never read this announcement.
So in the end, I decided to add a photo of random animals, like a raccoon, 
in my announcements.
Then in assignments, I give student a question about which animal did I chose.

![Random animal](ed-animal.png)

Once this has been announced, nearly every announcements has been read by everyone.


[Ed supports writing math with LaTeX](https://edstem.org/help/content-editor#latex-equation-editor), 
which is an important reason I use it.
However, students often don't use it.
My solution is simply --
If you don't use proper LaTeX,
I don't answer your question. 😜

You can pin 📌 a thread so it stays at the top.
This helps students to find the important stuff.
However, don't over use this.
Unpin old threads from time to time.

Instead of posting resources, such as slides, to Ed, 
put them in a folder in Box folder 📁 and share the link to it on Ed.
This saves you a lot of posts.

Finally, You can type emoji on Ed using the format of `:wink:`, which results in a 😉.
