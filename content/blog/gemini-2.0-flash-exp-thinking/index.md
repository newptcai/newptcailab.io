---
ShowToc: true
pubdate: "2024-12-20"
tags:
- Gemini
- ChatGPT
- LLM
- AI
title: "Gemini 2.0 Thinking"
---

I recently learned, from the ever-illuminating
[Simon Willison](https://simonwillison.net/2024/Dec/19/gemini-thinking-mode/),
that Google has released a new model, gemini-2-0-flash-thinking-exp.
So, naturally, I posed it a question, a little puzzle if you will.

> Given the following conditions, how many ways can Professor Y assign six different
> books to four different students?
>
> - The most expensive book must be assigned to student X.
> - Each student must receive at least one book.

It took the model some time, a moment of digital contemplation,
to complete the task.
But it did, correctly yielding the answer 390.
So now, alongside gpt-o1,
we have another model capable of solving this combinatorial problem,
a problem that, it must be said, eludes most of my students.
It seems that these ["inference scaling" based models](https://www.aisnakeoil.com/p/is-ai-progress-slowing-down)
will be able to solve more such problems,
each one a small victory for the silicon mind.
Another reason, perhaps, why I no longer set homework and only give quizzes,
a small act of surrender in the face of the inevitable.
