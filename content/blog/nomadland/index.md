---
pubdate: "2021-04-26"
tags:
- film
title: Nomadland (2020)
---


Last night, [Nomadland](https://en.wikipedia.org/wiki/Nomadland_%28film%29) won three Academy Awards,
including Best Picture, Best Director and Best Actress. 
It was directed by the [Chloé Zhao](https://en.wikipedia.org/wiki/Chlo%C3%A9_Zhao) 
and stars Frances McDormand.
I watched the film earlier this year
and thought it was a fine piece of art.
It does not surprise me at all that Nomadland won this many accolades.

There are already numerous reviews praising the film.
So, I will just mention a problem I have with the it.
As a film which tries to be a realistic depiction of Americans nomads, i.e., 
those who are house-less and live in a van while travelling around the country,
it has failed to be provocative.
It makes you sympathetic for all the predicaments which nomads have to face everyday,
while avoiding criticizing anyone for
[making their life so hard](https://www.nytimes.com/2021/04/24/opinion/nomadland-oscars.html).
For example, in one scene, 
Fern, the nomad played by McDormand, is invited by her sister,
who lives in a middle-class suburb,
to move in and settle down.
Fern says no, and her sister thinks 
that it is because Fern finds a life on the road is much interesting.
For me, that seems to tell the audience
"Van-life is really hard but these people choose it themselves.
So it is totally fine that we pretend that these people do not exist.
Let them suffer."

That been said, 
maybe making a film tame is a price worth paying for attracting a larger audience.
At least, now people will talk about nomads for a few days.
