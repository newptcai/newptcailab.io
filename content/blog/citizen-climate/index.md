---
pubdate: "2021-07-02"
tags:
- climate-crisis
- book
title: The Citizen's Guide to Climate Success by Mark Jaccard
---


This is a very informative and inspiring book on addressing climate change.
It is written by Canadian economist [Mark Jaccard](https://en.wikipedia.org/wiki/Mark_Jaccard), 
who has advised many regional and national governments on energy policies. 
The book is freely available [online](https://www.cambridge.org/core/books/citizens-guide-to-climate-success/49D99FBCBD6FCACD5F3D58A7ED80882D).

When people who care enough about humanity look into climate change, 
it is easy to feel despair.
Popular books sounding alarms on climate emergency such as 
[The Uninhabitable Earth](https://en.wikipedia.org/wiki/The_Uninhabitable_Earth_(book))
and
[Our Final Warning](https://www.goodreads.com/book/show/51471435-our-final-warning)
tell us an apocalyptic future will come soon
if there is no dramatic green house gases (GHG) emissions in the next ten years.
However, decades after climate scientists have reached consensus 
on the causality between human activity and global temperature rising,
we are still on a path of emitting more and more GHG.
It is difficult to see how things can change in the very limited time left.

However, Jaccard offers three reasons for optimism --

1. With more and more frequent climate-related disasters, it will be more and more
   difficult to deny the consequences of climate change.
2. Technology improvements in renewable energy and electric vehicles make de-carbonization of
   electricity and transportation feasible.
3. For developing countries, de-carbonizing these two sectors will
   significantly reduce air pollutions. 
   This is a strong incentive for them to act.

And how should governments proceed if they really want to address this emergency?
Based on his research and experience in advising governments, Jaccard suggests that

1. Carbon pricing (tax or trade) is strongly opposed by small number but determined
   voters. So it is politically impossible to to rely completely on carbon pricing
   for cutting emissions.
2. Regulating electricity generation and transportation is also strongly opposed, but
   less so compared with carbon pricing. This should be the main focus governments.
3. Consensus-based global GHG voluntary cut has failed and will never
   succeed. The only way to motivate every country to cut emission is for 
   climate-sincere countries to collectively impose tariffs on high-emission countries.

For me these all sound like good policies which actually have a chance to succeed.
But what does this has to do with me besides offering a bit comfort?
Quite a lot.
The key message Jaccard wants to convey is

> “So that’s it? There’s no hope? Nothing that might change their (politicians') minds before the calamity?”

> “There certainly is. Politicians will abandon a position if the political costs are excessive. Simply put, if the political costs exceed the political benefits.”

> “But how can that happen with the climate threat?”

> “We need to create the policy window. We need enough people to act in ways that catch
> the media’s attention and pressure politicians. Easiest is to engage politically.
> Citizens active in the political process are important, although the effect is
> impossible to measure...

I have become a vegan more than a year now partially because of climate change.
I have also vowed to live a low-carbon life such as forgo air-travelling for
recreation.
But reading this book makes me realizing that the most important and effective way
for me, a Canadian citizen, to help is to participate in Canada's political process.
Although I will be living abroad for a while,
I can still vote by mail in federal elections for the political party 
which has the best chance to implement practical policies addressing climate changes.
Will it make a difference?
I do not know.
But I am more hopeful now than before reading this book.
I highly recommend it to everyone who cares about the future of humanity.
