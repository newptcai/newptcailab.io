---
pubdate: "2022-05-04"
tags:
- podcast
- entertainment
title: What podcast am I listening to?
---


I have been staying at home and teaching online for nearly a month and half now.
During this time, I picked up my old hobby of listening to podcasts.
Don't get me wrong.
I never stopped listening to news.
But recent global events made me feel that it is probably not a good idea 
to consume too much news.
So I switched to listening to some more educational and entertaining podcasts.
Here are what I am listening to.

## People I Mostly Admire

[People I Mostly Admire](https://freakonomics.com/series/people-i-mostly-admire/)
is a podcast hosted by Steven Levitt, 
an economist of University of Chicago.
Levitt invites people, mostly those who he admires as the title suggests,
and talk about their careers, life lessons and random things.

I like this show mostly because guests of the show are often worthy of admiration,
and by that I don't mean they are only successful in their career.

Here's the introduction of a recent [episode](https://freakonomics.com/podcast/you-make-me-feel-like-a-natural-experiment/) --

>    Nobel Prize winner Joshua Angrist explains how the draft lottery, the Talmud, and
>    West Point let economists ask - and answer - tough questions.

The ideas shared by Angrist in the podcast are interesting.
But I found his personal story more fascinating. 
Angrist was not a good student in high school and 
turned down an opportunity to study at Princeton. 
However, after doing random things for a couple of years after graduation,
he found his calling and eventually became a successful economist.

Second chances in life are rare.
How can we not to admire someone who could grad it hard?

## Cautionary Tales

[Cautionary Tales](https://timharford.com/etc/more-or-less/) by Tim Hartford is
another show which I binged listened lately.
It is a collection of all sorts of disastrous mistakes people have made in recent
history.

The show is particular interesting for me because I am teaching statistics 
and many of these tales tells are about statistics.

In the episode [Catching a Killer Doctor](https://timharford.com/2021/03/cautionary-tales-catching-a-killer-doctor/),
Hartford tells the following story -- 

> Family doctor Harold Shipman got away with murdering his patients for decades. He was one of the most prolific serial killers in history – but his hundreds of crimes went largely unnoticed despite a vast paper trail of death certificates he himself signed.
> Why do we sometimes fail to see awful things happening right under our noses? And how can the systems that maintain quality control in cookie factories be employed to prevent another doctor like Shipman killing with impunity?

A murderous doctor who could have been caught by a statistician.
What story is more exciting than that?
