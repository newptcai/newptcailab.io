---
pubdate: "2021-05-06"
tags:
- philosophy
- Stoicism
title: Letter to a Young Friend --- The Course of the Universe
---


*Note*: I wrote this in May 2021. 
Things in the world seem to have taken another down turn.
That's why it is even more important that we focus on what is in our control.

{{< figure src="universe.jpg" title="Photo by Miriam Espacio from Pexels" >}}

My dear friend,

Today, we had talked about COVID-19, wars, 
oppression and, of course, climate change.
Naturally, you were saddened by our discussions.
That makes me think if I can write something to cheer you up.
Here are some ideas from [Epictetus](https://iep.utm.edu/epictetu/),
an ancient-Roman philosopher -- one of my favourite.
Whenever I am upset, Epictetus has always been helpful.
Hopefully he can also offer you some solace.

Epictetus was a slave in his youth 
and his name simply means "acquired".
With his owner's permission, 
Epictetus studied philosophy.
Eventually, he was freed and founded a school of philosophy in northern Greece.
Just as Socrates, Epictetus did not write down anything.
What we know about him mostly comes from some lecture notebooks by a student of him,
known as the [Discourses](https://iep.utm.edu/epictetu/#SH2a).
A more concise version is called the [Handbook](https://iep.utm.edu/epictetu/#SH2b).

The Handbook starts with following passage --

> Some things are within our power, 
> while others are not.
> Within our power are opinion, motivation, desire, aversion, 
> and, in a word, whatever is of our own doing;
> not within our power are our body, our property, reputation, 
> office, and, in a word, whatever is not of our own doing. 
> The things that are within our power are by nature free,
> and immune to hindrance and obstruction,
> while those that are not within our power 
> are weak, slavish, subject to hindrance, and not our
> own. -- [Handbook 1-2](http://classics.mit.edu/Epictetus/epicench.html)

What Epictetus means is that if we are honest with ourselves, 
we have to admit that what we truly have in control 
is simply our own thoughts, choices, preferences, and actions,
or in a word, our own efforts.
And what we do not have in control is everything else, 
including all the things which we care about deeply.
This idea is now often referred to as [the dichotomy of control](https://howtobeastoic.wordpress.com/2017/07/24/everything-you-need-to-know-about-the-dichotomy-of-control/).

[Cicero](https://en.wikipedia.org/wiki/Cicero), 
another ancient-Roman philosopher and politican,
explained this idea by the metaphor of an archer trying to hit a target.
What the archer has in control is to choose to shoot in a way 
that is most promising.
Whether the arrow actually hits the target is ultimately out of the archer's control.
For example, when the arrow is flying in the air, a sudden gust may blow it away.

You may feel this appears to be a very depressing idea.
You may even protest and ask 
"if we believe that don't have control of the outcome,
what is the point to even try?"
But Epictetus definitely was not arguing for nihilism and inaction.
He philosophy school was quite successful and 
it was said that he enjoyed more popularity in his day 
than Plato had in his.
That would have been impossible if Epictetus's philosophy 
made him depressed and staying in bed all day.

What Epictetus wishes his students to learn is that 
they should focus entirely on their own effort,
while avoid being upset however things turn out to be.
For example, despite the best effort of doctors,
a patient may still succumb to an illness.
In this case, Epictetus would say that 
the doctors should not blame themselves at all.

Of course, if after careful consideration,
which Epictetus would have agreed is in our control,
we do not see any possibility to achieve what we want,
then we should perhaps direct our effort to another cause
in which we may actually make some progress.
(I may really want to turn the moon into cheese to feed the world.
But I definitely should not try because it is 
impossible in practice if not impossible physically.)

You may remember that I agree with the moral philosopher 
[Peter Singer]({{< ref "/blog/singer" >}}) 
that we have responsibilities to help

* eliminating [global extreme poverty](https://newptcai.github.io/what-have-i-done-in-2020-part-3-donating-to-charities.html) still afflicting 1.4 billions of people,
* reducing the senseless slaughter of [56 billions farm animals each year](https://newptcai.github.io/what-have-i-done-in-2020-part-1-becoming-a-vegan.html),
* and avoiding the looming disaster of [climate change](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html).

Which reasonable people would not agree 
that these are some of the greatest achievements 
mankind has ever dreamed of?
However, as you know all too well, 
these problems are of tremendous in scale 
and the recent news are not encouraging.

Take climate change for an example. 
One of my favourite podcasts,
[How to Save a Planet](https://gimletmedia.com/shows/howtosaveaplanet)
often interviews people who are trying to reduce greenhouse gas emissions.
At the end of each episode, 
the guest is always asked the question "How screwed are we?"
And the answer is usually a long pause and "We are very screwed."
However, most of them would hastily add that 
they "refuse to sit there and do nothing".
I think had Epictetus still been alive,
he would give them a thumbs up.

Epictetus tells us, 
our responsibility is to give enough considerations to the best course of action,
and to develop the willpower to follow through.
It is this and only this for which we should be judged at the end of our days.
Whether our effort actually comes to fruition one day, 
that is out of our hands.
The universe can and will take its own course.
Since we cannot bend law of physics,
there is no point for us to **demand** the universe to go the way we want,
and we should take well-justified pride and satisfaction in 
our hard work to push the world to a better direction.

Hope this helps and wish to hear from you soon!

Yours truly,

Cai

----

PS, if you found Epictetus' philosophy interesting, I can recommend two short books

* [How to Be Free: An Ancient Guide to the Stoic Life](https://www.goodreads.com/book/show/39204065-how-to-be-free) by Epictetus, A.A. Long (Translation)
* [Lessons in Stoicism: What Ancient Philosophers Teach Us about How to Live](https://www.goodreads.com/book/show/43566091-lessons-in-stoicism) by John Sellars

The first one is a recent translation of Epictetus. 
The second one is a concise summary of the ideas of the [Stoic](https://iep.utm.edu/stoicism/) school of philosophy,
which Epictetus belongs to.
