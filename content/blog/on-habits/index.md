---
pubdate: "2022-02-01"
tags:
- advice
title: On Making New Habits
---


I came across an interesting article [A behavioral scientist's advice for changing your life](https://www.npr.org/2021/05/14/996939779/a-behavioral-scientists-advice-for-changing-your-life).

Here's an excerpt

> If you're trying to develop a habit like working out regularly or writing each day, letting yourself have a little leeway is the way to make that habit stick.

> If you know when you might procrastinate, impose a fine on your future self and commit to paying if you don't follow through with your goals.

> When we just look at when people choose to pursue healthy activities or start new goals, Mondays are a big motive

> Making accountability public can be a powerful motivator — for good! 

> Quitting smoking or eating right for a month won't magically make you healthy — lasting change requires lasting attention.

Basically, if you want to do better in a course, you can

* Study whenever and wherever you can. It does have to be at exactly 🕦  or in the classroom.
* Commit to flush some 💸  down the 🚽  if you don't study today.
* Change your 📆  so each day is a Monday. 😆 
* ✏️  a post on WeChat telling everyone that you will study hard. 🦾 
* Keep a 📓  of how you did everyday.
