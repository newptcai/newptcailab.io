---
pubdate: "2021-04-13"
tags:
- literature
- book
title: Flowers for Algernon (1966)
---


[Flowers for Algernon](https://en.wikipedia.org/wiki/Flowers_for_Algernon) is a classic
science-fiction novel by Daniel Keyes published in 1966.  
I finished reading it last week and was greatly impressed.
The story is touching and the characters are likable.
But most importantly, 
many issues raised by the book,
such as how we should treat people with mental problems,
are still quite important after more than half a century.

The main character, Charlie Gordon, is 32 years old with an IQ of only 68.
He receives an experimental treatment which turns him into a genius.
However, becoming smart does not always make Charlie happy as he expected.
At some point, he asks

> I don't know what's worse: 
> to not know what you are and be happy,
> or to become what you've always wanted to be, 
> and feel alone. 

(This quote is actually from a TV adaption.
The ordinal text is a bit more subtle.)

Unfortunately, I think that this is not a question we need to anguish over.
Intelligence, in the narrow sense of IQ or similar tests,
is by large [hereditary](https://www.scientificamerican.com/article/is-intelligence-hereditary/).
The magic operation in the book does not exist in the real world.
So-called [smart drugs](https://en.wikipedia.org/wiki/Modafinil) 
help people stay awake and focus better.
But they [do not have a long-lasting effect](https://www.nature.com/news/medication-the-smart-pill-oversell-1.14701).
And as we age, 
all of us will invariably experience a decline in brain efficiency.
That is to say, how smart we are, 
to a large extent is not under our control,
just like tomorrow's weather.
And there is no point in being upset about what we do not have control of,
just like there is no point to be upset if it rains tomorrow.

That being said, I do prefer to know more about this world,
even if that means experiencing some anxiety about important issues,
such as [climate change](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html),
[extreme property](https://newptcai.github.io/what-have-i-done-in-2020-part-3-donating-to-charities.html),
and the suffering of [animals](https://newptcai.github.io/what-have-i-done-in-2020-part-1-becoming-a-vegan.html).
Just like we need a map/GPS when we visit a foreign city on vacation,
we also need to know what kind of world we are living in
to decide how we are going to
spend our brief vacation,
also known as life,
on this planet earth.

On the other hand, it is doubtful how much does intelligence decide our happiness.
Being a mathematician myself,
I have met many brilliant academics.
Most of them are just like normal people.
Sometimes they are happy and sometimes they are not.

So my suggestion for Charlie Gordon would be -- 

> Whether your IQ is 68 or 180, or somewhere in between,
> there are always things you can do to help yourself today.
> If you keep trying, soon you will feel happier. 
