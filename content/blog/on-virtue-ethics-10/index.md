---
pubdate: "2021-07-19"
tags:
- philosophy
- book
- virtue-ethics
title: On Virtue Ethics by Rosalind Hursthouse -- Chapter 10 Naturalism for Rational
  Animals
---


A summary of Chapter 10 Naturalism for Rational Animals.

----

## What Difference Does Our Rationality Make?

Unlike animals, it is primarily our actions from reason that we are ethically good or
bad human beings.
In ethical naturalism, rationality makes a big difference.
And adding rationality does not need to add the fifth end to the four other describe
in the [previous chapter]({{< ref "/blog/on-virtue-ethics-09/" >}}).

What characteristics do human have? 
Comparing to animals, it is hard to summarize.
We enjoy and suffer from so many different things.
So it seems that naturalism cannot work for humans.

We cherish the variations in humans and resists the idea that nature can determine
how we should be.

Nature determines how animals live. They simply cannot do otherwise.
There is no point to say that they will flourish better if they change their
behaviour.
Our concept of living well as a member of *X* is entirely constrained by what *X*
actually does.

But rationality makes us different.
We cannot tell what *can* do from what we *do* do.
Feminists are right that it is *not* in our nature that
women are bound to do what they have done so far.
What is a good human being is far from been constrained 
by what a typical human does at the moment.

If we do not have a characteristic way of life, or we can change what we do, how can
naturalism work?
We do have a very generalized characteristic way of life, which is a rational way.
This pretty much implies the most humans are in fact ethically defective humans 
who do not live rationally.

But if this characterization of humans is decisively normative, 
is it still naturalism?
Yes, the four ends still constrains what a good human being is.
As an example of these constraints, 
consider the impersonal benevolence promoted by Peter Singer.
It probably does not affect personal survival and life enjoyment,
but does it serve the continuance of our species
and well functioning of social groups?
The burden is on those who promote it to prove so.

## Analogies and Disanalogies

To recap, naturalism evaluate character traits, not particular actions.
Vegetarianism might be good, because (i) temperance is a virtue 
and (ii) eating meat is greed and self-indulgence for most people.
Naturalism concerns most about (i), not (ii).

Is a gangster a bad human being? 
First we have to establish that charity, justice, etc., are virtues.
Then we have to add the premise that gangster do no have these virtues.
Finally, we can conclude that gangsters are bad humans.

Ethical evaluations (of humans) are not science,
but it is still objective in the sense that it does not depend 
on our interests.

## Ethical Evaluations as Necessarily Practical

The claim that ethics, or morality, is ‘necessarily practical’ means that
they must be concerned with actions.
The necessity comes from the observation that if a society has no inclination
to take ethical judgements into account in its daily practical reasoning.

How does our ethical evaluations take part in our practical reasoning?  This comes
from our claim that our 

> characteristic way of going on is in a rational way, that is, ‘in any way or ways
> that we can rightly see we have reason to do’, is that it connects ethical
> evaluations, in our own mouths, with our own views about what there is reason to
> do.

Then two examples are given -- would an atheist consider piety a virtue?
It is a character trait that may foster the four ends.
But it does so in an irrational (according to the atheist) way.
So it is not a virtue.

Would drifter who travels around the world without ever making long term connections
consider loyalty a virtue? Not initially. But if she reflects on it and see
how it helps to foster the functioning of social groups,
she may come to realise that people have *rational* reasons to be loyal to their
friends. Thus, she realizes that loyalty is a virtue and 
the reasons that people are loyal also applies to her.

> To recognize a character trait as a virtue, on the grounds of ethical naturalism,
> is to recognize the X reasons for acting people with that character trait
> characteristically have as reasons, to recognize them as reasons for oneself.

The chapter concludes with a claim that naturalism has withstood the objection that
is ethical naturalism is too similar to non-ethical naturalism.

----

## My comments

It is clear that a pure naturalist justification of virtues 
would not demand impersonal virtue or consideration for animals.
Is this a deficiency of virtue ethics in general?
Are virtues only limited to ours and our kinds?
That is a big bullet to swallow.
