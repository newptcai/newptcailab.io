---
pubdate: "2021-02-21"
tags:
- academic
- mathematics
title: Finding An Academic Job As a Mathematician
---


I started searching for a permanent position academic position since last July and eventually got an
offer in February. Here are some of my thoughts about the process.

## Tips

There is very high competitions in academics. Be ready to leave for the industry at any time.
Learning some practical skills that can help you get a job outside universities.

Apply early. The pandemic slows things down a lot.

Do not apply for too many jobs. It is a waste of time. Only apply for positions that you have an
obvious advantage.

Tell everyone you know that you are looking for jobs. It is the easiest way to know job openings.

Do not avoid liberal arts colleges. People working in these places told me very good things about
them.

Do not be upset when your application are rejected. There might be a 100 people who applied.

If you get an interview, think it as an opportunity to discussion of 

Be grateful for what life offers.

## Resources

Websites

* [Nordic Math Job](http://www.maths.lth.se/nordic/) -- Jobs in Nordic countries, in particular
  [Sweden](http://www.matematik.lu.se/nordic/sweden.html) where I was living.
* [mathjobs.org](https://www.mathjobs.org/jobs?joblist-0----40-p--) -- Math jobs around the world.
* [jobs.ac.uk](https://www.jobs.ac.uk/) -- Mostly UK jobs. But also quite many positions in China.
* [Computing Research Association](https://cra.org/ads/) -- Jobs in computer science departments.
* [Canadian Mathematica Society](https://cms.math.ca/careers/) -- Mostly professor positions.
* [Theoretical Computer Science Jobs](https://cstheory-jobs.org/) (Thanks Lianna Hambardzumyan for
  telling me about this one)

Email list

* [DMANET: Discrete Mathematics and Algorithms Network](http://www.zaik.uni-koeln.de/AFS/publications/dmanet/)
