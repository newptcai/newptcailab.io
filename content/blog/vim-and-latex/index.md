---
pubdate: "2021-02-18"
tags:
- programming
- vim
- LaTeX
title: Using VIM to Write LaTeX in 2021
---


I have been using VIM (actually GVIM) to write papers with LaTeX for almost a decade now. My setup
for this has not changed for most of the time. One advantage of using VIM is that once you have
learned it, you can expect to use it for a life time.

That been said, a decade seems to be a good time to have an update. So here's my new VIM setup for
LaTeX in 2021 and perhaps for the next decade.

## Install plugins

VIM 8 has quite a convenient plugin system [vim-plug](https://github.com/junegunn/vim-plug). Here
are the plugins which I have installed for LaTeX (all codes snippets are from my `.vimrc` file)
```vim
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'

Plug 'lervag/vimtex'

Plug 'ludovicchabant/vim-gutentags'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround' 
Plug 'Chiel92/vim-autoformat'
```

## COC

I installed [coc](https://github.com/neoclide/coc.nvim) for auto-completions. The example
configuration on the website works quite well. To make it work better with LaTeX, you should install
the extension [`coc-texlab`](https://github.com/fannheyward/coc-texlab).

coc-texlab also provides some extra functions such as basic syntax check. But by default, the parts
where there is a warning are marked by an underline. This is confusing as there are many "_" in a
math paper. So I chanced to curly underline by

```vim
hi default CocUnderline cterm=underline gui=undercurl
```

~~~
<img src="vim-and-latex/coc-texlab.png" style="width:100%; padding:0"></img>
~~~

Together with [vim-snippets](https://github.com/honza/vim-snippets), coc also provides code
snippets. To make it work, install [coc-snippets](https://github.com/neoclide/coc-snippets).  Again
the recommended setup works well.

## vimtex

[vimtex](https://github.com/lervag/vimtex) is package which provides some help in editing LaTeX and
compling.  Install [coc-vimtex](https://github.com/neoclide/coc-vimtex) to make it a source of
completion for coc.  A nice feature of vimtex is the auto-completion for citations. 

~~~
<img src="vim-and-latex/vimtex.png" style="width:100%; padding:0"></img>
~~~

## Other plugins

Two other plugins which I find useful are

* [vim-gutentags](https://github.com/ludovicchabant/vim-gutentags) -- Automatically update tags so
  you can jump to a label easily.
* [vim-gitgutter](https://github.com/airblade/vim-gitgutter) -- Some handy git functions.
* [vim-surround](https://github.com/tpope/vim-surround) -- This is very useful for editing TeX
  commands and environments.
* [vim-autoformat](https://github.com/Chiel92/vim-autoformat) -- Automatically format the whole file
  with [latexindent](https://github.com/cmhughes/latexindent.pl).

## Random labels

Some times I have many equations in a paper and I found it tedious to think names for all of them.
So I created a function to generate random labels

```vim
" Insert random labels
function! RandomLabel()
python3 << EOF
import random, string, vim
label = ''.join(random.choice(string.ascii_uppercase) for _ in range(4))
vim.command("let @z = '%s'" % label)
EOF
endfunction
vmap ,rl <Esc>:call RandomLabel()<CR>gv"zp
imap ,rl <Esc>:call RandomLabel()<CR>a<C-R>z
```

## More useful tips

The website [tex.stackexchange.com](https://tex.stackexchange.com) is a treasure for all things about LaTeX.
It also has some useful tips for using VIM to write LaTeX. Here are some of my favourites

* [Don't spell check TeX commands](https://tex.stackexchange.com/a/175286)
* [Better automatic formatting](https://tex.stackexchange.com/a/1550)
