---
ShowToc: true
date: "2024-12-06"
draft: false
images:
- sad-pig.jpg
tags:
- internet-excerpt
- factory-farming
- effective-altruism
- animal-welfare
- environmentalism
title: "Factory Farming as a Pressing World Problem
  by Benjamin Hilton"
---

## 🙏 Acknowledgement 

I recently came across a thought-provoking [post](https://forum.effectivealtruism.org/posts/goTRwb49riDvXGdy8/factory-farming-as-a-pressing-world-problem) by
[Benjamin Hilton](https://forum.effectivealtruism.org/users/benjamin-hilton-1) of [80,000 Hours](https://80000hours.org/) on the [Effective Altruism Forum](https://forum.effectivealtruism.org/).

The post provides a detailed overview of the immense harm caused by factory farming. Below is an excerpt highlighting some of the most shocking facts about the industry. For detailed references, please refer to the [original article](https://forum.effectivealtruism.org/posts/goTRwb49riDvXGdy8/factory-farming-as-a-pressing-world-problem).  

This excerpt is also posed on Plant Futures DKU's [website](https://dku-plant-futures.github.io/post/factory-farming-as-a-pressing-global-problem/).

{{< figure src="sad-pig.jpg" caption="A pig confined to a pen. Photo by [Matthias Zomer](https://www.pexels.com/@matthiaszomer/) on Pexels." >}}

## Summary

History is littered with moral mistakes --- things that once were common, but we now consider clearly morally wrong, for example: human sacrifice, gladiatorial combat, public executions, witch hunts, and slavery.

In my opinion, there's one clear candidate for the biggest moral mistake that humanity is currently making: factory farming.

The rough argument is:

- There are *trillions* of farmed animals, making the scale of the potential problem so large that it's hard to intuitively grasp.
- The vast majority (we estimate 97.5%) of farmed animals are in *factory farms*. The conditions in these farms are far worse than most people realise.
- Even if nonhuman animals don't morally matter as much as humans, there's good evidence that they are conscious and that they feel pain --- and, as a result, the poor conditions in factory farms are likely causing animals to experience severe suffering.

What's more, we think that this problem is [highly neglected](https://forum.effectivealtruism.org/posts/goTRwb49riDvXGdy8/factory-farming-as-a-pressing-world-problem#Work_on_factory_farming_is_highly_neglected) and that there are [clear ways to make progress](https://forum.effectivealtruism.org/posts/goTRwb49riDvXGdy8/factory-farming-as-a-pressing-world-problem#There_are_promising_ways_of_solving_this_problem) --- which makes factory farming a highly pressing problem overall.

## How many animals are in farms?

Every year, we kill somewhere between 400 billion and 3 trillion vertebrates
(e.g. cows, chickens, fish) --- some are killed for sport and some are dissected
for experiments, but the vast majority are either slaughtered for food or die in
farms before they're old enough to be purposefully
slaughtered.

That doesn't mean there are trillions of animals in farms at any given time, as
the lifespan of these animals is often less than a year. There are probably
around 120–210 billion vertebrates alive in farms at any given
time.

If we include *invertebrates* (e.g. octopuses, insects, crabs, snails, shrimp) ---
which is more controversial because we have less evidence of their ability to
feel pain --- it's more like 1.6–4.5 trillion farmed animals killed per year, and
around 350–700 billion animals alive in farms at any given time.

It's much harder to estimate how many of these animals are in *factory* farms.
In part, this is because 'factory farming' doesn't have a clear definition.
Generally, we'd say something is a *factory farm* if it has generally poor
welfare conditions and a large number of animals in a small space ('high
stocking density').

Rather than drawing a clear distinction, we'll go through how different animals
tend to be treated on farms and note any key variations, such as in standards
between countries or what it means for hens to be 'cage
free.'

We *don't* to focus on animals that aren't farmed ---
[wild animal welfare](https://80000hours.org/problem-profiles/wild-animal-welfare/) and
animals killed by fishing and hunting are whole separate
issues.

This table shows some estimates for various species. For vertebrates, we've only
included species where we farm more than 100 million animals at any one time
(so, for example, frogs and turtles but not snakes or salamanders). We've
included data for all vertebrate species we could find – we farm many other
invertebrates, like cephalopods (e.g. [squid](https://www.longdom.org/open-access/the-advantages-and-challenges-of-squid-farming-101283.html)),
and bivalves (e.g. clams, oysters), but we couldn't find any good estimates of
how many of these animals are in farms.

|     |     |     |
| --- | --- | --- |
| **Farmed animal** | **Alive in farms at any one time** | **Slaughtered per year** |
| Chickens | 24 billion (10 billion broilers – chickens bred for meat; 7–10 billion egg-laying hens) | 72–77 billion (69 billion for meat; 3-9 billion male chicks from the egg industry) |
| Pigs | 1 billion | 1.5 billion |
| Cattle (cows) | 1.5 billion | 0.3 billion |
| Sheep and goats | 2.2 billion | 1 billion |
| Ducks | 1 billion | 3 billion |
| Quail | 0.5 billion | 1.5–2.5 billion |
| Turkeys | 0.5 billion | 0.7 billion |
| Geese and guinea fowls | 0.4 billion | 0.7 billion |
| Fish | 100–180 billion | 100 billion |
| Frogs | 0.1–2.6 billion | 0.3–1 billion |
| Chinese softshell turtles | 0.3–2.1 billion | 0.2–0.9 billion |
| Crabs | --  | 5–15 billion |
| Crayfish and lobsters | --  | 40–60 billion |
| Shrimp | --  | 210–530 billion |
| Snails | --  | 3–8 billion |
| Crickets | 35–40 billion | 370–430 billion |
| Mealworms | 25–30 billion | 290–310 billion |
| Black soldier files | 8–16 billion | 190–300 billion |

## How do we treat these animals?

Let's go through some of the most commonly farmed animals and look at how they're treated.

In general, the main issues seem to be:

- Poor methods for pre-slaughter stunning, and for slaughter itself
- Extremely crowded and dirty living conditions
- High rates of disease and injury
- Particularly poor conditions during transport
- Occasional withdrawals of food and water for extended periods
- High rates of painful procedures, often carried out without anaesthetic

We think it's important to be sceptical of many of the claims of animal advocates --- like anyone advocating for a cause, they have incentives to exaggerate their claims. So we've relied as much as possible on academic, industry, and government sources, and given extensive detail in the footnotes. We tried hard not to cherry-pick the worst cases of animal abuse --- this just reflects how the *vast majority* of animals are treated.

We've carefully selected photographs of what we think represent ‘normal' farms. Photographs of factory farms can be disturbing, so we've haven't included them in this post. Instead, you can find the photographs [in the version of this article on our website](https://80000hours.org/problem-profiles/factory-farming/#treatment).

### Farmed chickens

We've genetically selected broiler chickens to grow very fast; they may reach the weight for slaughter in as little as five weeks. This fast development causes fluid to build up in the abdomen, which compresses organs, causing pain and breathing difficulties. This build-up of fluid is known as *ascites*.g These chickens' large sizes also cause difficulty moving. Much like injured humans, chickens change the way they move in response to their weight and injury --- which then causes muscle and bone pain.

Their movement is also very restricted, and even on free-range farms each bird often has only around 0.1m² of space. Long periods of standing and lying in waste causes painful lesions and chemical burns (specifically, 'hock burn' and 'footpad dermatitis').g Heat stress is common due to temperatures of 30 to 40°C in housing.

Diseases are common. One of the most common is necrotic enteritis (a bacterial infection that destroys the small intestine over the course of about 3–4 days). In developing countries, infectious diseases like *visceral gout* (kidney failure leading to poor appetite and uric acid build up on organs), *coccidiosis* (parasitic disease causing diarrhoea and vomiting), and *colibacillosis* (*E. coli* infection) are common.

Breeder chickens can live for up to a year, but because they're still broiler chickens, they're bred to want to eat a lot. Feeding breeders isn't that helpful for farmers (in fact, it can lead to injury, health issues, and fewer chicks being born), so instead they are kept on the edge of starvation.g

Before slaughter, chickens must be 'caught' and loaded into trucks for transport. This process involves humans or a machine picking up and loading chickens into crates. Often, this is done to half the flock, while the rest are left without food or water for days until they are also collected. This process is known as 'depopulation.'g

In some systems, like those in Europe, 90% of chickens are stunned before slaughter, mostly by having their heads dunked in water with an electric current. In other countries, like the US, there are no rules governing how chickens are killed. Many chickens experience severe pain when they are shackled upside down by their legs prior to stunning. Chickens then have their throats cut ('bleeding'), before being killed using hot water --- as a result, many chickens are boiled alive when the previous steps fail.

Caged laying hens tend to have around 0.07 m² of space, which prevents typical movement and behaviours, including standing, preening, turning, and wing flapping, and prevents them from engaging in natural behaviours like nesting or foraging. Osteoporosis leads to frequent fractures of the keel bone (the part of the sternum where the wings attach). Withdrawal of food and exposure to 24-hour bright lights is sometimes used to induce shedding and regrowth of feathers (known as moulting) to increase egg production in countries outside of Europe. Egg peritonitis --- an infection caused by yolks remaining within their body cavity from eggs that break before they are laid --- is quite common, causes substantial pain, and is sometimes fatal.

The lives of cage-free hens are marginally better. They are usually kept in indoor aviaries, and free-range hens go outside less than consumers expect. Cage-free hens have a little more space per bird, and are able to move around somewhat. Keel bone fractures are still common.

However, because of stress and the inability to socialise naturally with such a large flock, hens still end up wounding each other by pecking and sometimes even resort to cannibalism. As a result, it's common to remove the tip of both caged and cage-free hens' beaks using a hot blade, without anaesthesia. Beaks are highly sensitive and used by birds to interact with the world, similar to how humans use their hands. Male chicks are killed by maceration. Egg-laying hens are depopulated and slaughtered when they are old enough that their productivity drops, with similar issues to the slaughter or broiler chickens.

Other birds (ducks, quail, turkeys, geese, guinea fowls) are treated very similarly to chickens).

### Farmed pigs

Most farmed pigs are kept indoors on high-density farms. Heat stress is common, reaching temperatures of 30 to 40°C --- which is particularly bad as pigs do not sweat; instead they keep cool in the wild through wallowing and rooting.

Piglets go through the removal of parts of their canine teeth (known as tooth cutting) and their tails (known as tail docking) without anaesthesia. Most male piglets are also castrated (other than in the UK and some EU countries) to prevent the strong odour and taste of meat associated with uncastrated pigs (known as 'boar taint').

A small proportion of pigs are kept outdoors. However, these pigs usually have metal or wire rings inserted into their noses (nose ringing), which deliberately causes pain when pigs go to root in order to prevent the ground from being upturned.g

The worst conditions are experienced by breeding sows. In countries other than Sweden and the UK, pregnant sows are kept in gestation crates --- small pens so narrow that they cannot move or even turn around. And in countries other than Sweden, Norway, and Switzerland, farrowing crates are used after birth to prevent sows being able to move away from their piglets. Piglets are weaned at around 3–4 weeks, after which sows are inseminated again immediately, leaving them little time outside of these small pens.

At slaughter, pigs undergo electric stunning, controlled atmosphere stunning (gassing with carbon dioxide), or are not stunned for shackling (i.e. live-shackle slaughter), after which the major arteries near the heart are severed.g

### Farmed cattle

Generally, cattle are treated better than chickens and pigs, with 5–10 m² of space per cattle.g

The main issues are:

- Many dairy cows are kept in cubicle housing or tie-in stalls (where they are tethered to the stall by their neck) with limited access to pasture for half of the year. Beef cattle are often 'feedlot-finished,' meaning they are kept indoors for 100 to 600 days to increase weight gain.
- Calves have their early horns (known as "'buds'") removed by having their heads burned with a hot iron, or through chemical burns, often without anaesthesia. Older cattle sometimes go through a much more painful process where their grown horns are removed using tools like shears or saws.g
- Lameness (damage or weakness to limbs) and mastitis (inflammation of the udder) are common causes of pain in dairy cows. Indoor floors for beef cattle usually have gaps in them to allow excrement to fall through, but this makes injury much more common.
- Dairy cows are kept pregnant to ensure constant milk production. This creates an issue: dealing with the unwanted calves. All male calves and almost all female calves are separated from their mothers pretty much immediately --- both calves and mothers appear to cry for each other for days to weeks afterwards.g
- Male calves from the dairy industry are often killed shortly after they're born. This is because dairy cows and meat cows are different breeds bred for different traits. If they are kept for meat, it's usually veal. Veal calves are fed an iron-restricted diet to keep their flesh pale. Outside the EU, veal calves are usually raised in single-occupancy crates no bigger than the calves: these prevent almost all socialisation and movement for some or all of their (short) lives.
- Transportation to the slaughterhouse is stressful, with cattle spending long periods in cramped conditions without food or water. Dairy cows that are no longer producing milk at a profitable rate usually have some kind of injury or disease, making it harder to deal with these sorts of conditions.g
- At slaughter, the use of electric goads causes pain, and there are poor conditions in the resting areas at slaughterhouses. Cattle are usually stunned with a bolt gun or electric shock, which often fails, leaving cattle conscious when they are hung upside down by one leg while their throats are cut and their trachea removed. Throat-cutting sometimes intentionally occurs without stunning.g

### Farmed sheep and goats

Sheep and goats also tend to be treated better than chickens and pigs, and are largely kept in extensive farms.

The main issues are:

- Both sheep and goats are castrated. The usual method is by tying an elastic ring around the scrotum, restricting blood flow while the scrotum gradually dies and then falls off. In sheep, the same is also done to their tails, which take 4–6 weeks to fall off. This is often done without anaesthesia.g
- Goats have their early horns (known as 'buds') removed by having their heads burned with a hot iron, often without anaesthesia.g
- Lameness (damage or weakness to limbs) is a common cause of pain. In sheep, this is often caused by bacterial infections known as foot rot and foot scald.g
- Mastitis (inflammation of the udder) is relatively rare in sheep and goats kept for milk production, but can be very painful.g
- Around 15–20% of lambs die on farms before slaughter --- for example, from disease, extreme hot or cold temperatures, or starvation. Around half of these die soon after birth, often because of complications resulting from the birth itself.g
- Lambs are separated from their mothers well before they would naturally leave, which has a clear behavioural (and likely emotional) effect on both lambs and their mothers.g
- Transport to slaughter is often in overcrowded trucks without food or water. When trucks are stationary in hot weather, sheep and goats experience extreme heat.g
- While waiting for slaughter, sheep and goats are in cramped conditions with limited access to food and water. They are stunned with a blow to the head or by electrocuting their heads, although not all sheep and goats are stunned successfully --- and some are not stunned at all.g

### Farmed fish

We're more uncertain about what produces a poor welfare environment for fish, and there are so many different species of fish that it's difficult to say much about their welfare in general. For example, it seems like some fish *prefer* high stocking density environments. (There are a variety of reasons for this, including that high stocking density can prevent territorial and aggressive behaviour). Other fish species seem to prefer more turbid (murky, dirty-looking) water to clean water.

That said, there are almost no legal protections for fish welfare. Somewhere between 15 billion and one trillion fish die in farms before reaching slaughter age, and that high pre-slaughter mortality rate is a bad sign for their general welfare.

Common issues in fish farms include:

- Poor water quality, including insufficient dissolved oxygen; inappropriate temperature, pH, and salinity; and build ups of ammonia and nitrates from wasteg
- Disease and parasites, such as sea lice
- Other stressors related to the harsh environment --- like withdrawal of feed --- which lead to injury (e.g. fin erosion), and cannibalism

Since fish are smaller than most land-based farm animals, more fish are killed for the same amount of food. What's more, unlike herbivorous land-based farm animals, predatory fish such as tuna and salmon (and the lesser-known mandarin fish) must be fed other fish, which are often still alive, increasing the welfare concerns per animal sold.

Many issues occur at slaughter. Common slaughter methods include:g

- Leaving fish in air to slowly suffocate to death over the course of several minutesg
- Putting fish in baths of ice slurry where they gradually lose consciousnes. In theory, fish are left until they die from lack of oxygen before they're cut open and drained of blood. But sometimes, if fish aren't left in the ice slurry long enough, or if there isn't the right ratio of ice to water and fish, they can recover and regain brain function as they warm up, leaving them conscious when they're cut open.g
- Bubbling carbon dioxide into the fish's water to make it gradually acidic, which eventually stuns the. Fish will swim vigorously and try to escape the tank when this is done. Again, if fish are removed too early or the water doesn't become acidic enough, they'll be conscious when they are cut open.g
- Simply cutting off the gills of a fish, causing them to bleed out and suffocate to deathg

### Farmed frogs and turtles

Frogs and turtles are kept at such high density that they must stand on top of each other to have enough space. Tadpoles and froglets have extremely high rates of disease --- in particular, septicemia, which can frequently cause swelling of the whole body and for the stomach to hang out of the frog's mouth, as well as convulsions, paralysis, and death. Farmed Chinese softshell turtles have high rates of white-spot disease, which causes skin lesions, lack of eating, and eventually death.g

Frogs are stunned by being left in an ice bath for around 15 minutes or by being given a few-second-long electric shock, after which they are cut open --- but we're unsure about compliance or success rates. We're unsure how Chinese softshell turtles are slaughtered, but it's claimed that turtles are often beheaded or have their shells removed without stunning.g

### Farmed shrimp and crustaceans

The main welfare issues for shrimp (the vast majority of farmed decapod crustaceans) are:g

- Poor water quality --- which could include a lack of oxygen to breathe, or
  swimming around in their own waste
- Disease --- symptoms include spots, lethargy, body deformities, changes in
  colour, reduced food intake, and death
- Eyestalk ablation --- blinding shrimp by removing one or both of their
  eyestalks, usually by pinching, cutting, or burning them off
- Slaughter --- by being left in the air to suffocate or being crushed by other
  shrimp

Crustaceans more broadly are [routinely boiled
alive](https://www.tastingtable.com/998433/heres-why-crustaceans-are-usually-cooked-alive/)
just before being eaten --- and therefore travel extremely long distances while
alive.

### Other farmed invertebrates (snails and insects)

We're very uncertain about what produces a poor welfare environment for these
animals.

Welfare concerns for snails appear to include: high stocking densities, disease,
purging (starvation before transport to ensure there's no undigested food left
inside when they're eaten), live transport, and slaughter by boiling alive.g

The main welfare concerns for insects (including black soldier flies, mealworms,
and crickets) include: high stocking densities, disease and
parasites, cannibalism, injury, handling stress (such as vibrations that may be
perceived as predatory attack, or exposing light-averse insects to bright
lights), withdrawal of food before slaughter or from adults (in particular,
black soldier fly adults are never fed), live transport, and slaughter by
roasting, baking, microwaving, freezing in air or asphyxiation --- all without
anaesthesia.
