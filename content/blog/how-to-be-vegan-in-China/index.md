---
ShowToc: true
cover:
  caption: Be a happy vegan in China
  image: rabbit-in-restaurant-01.jpg
date: "2023-08-19"
tags:
- veganism
- China
- advice
title: How to Be a (Happy) Vegan in China
---

I've been a vegan for over three years, and I've lived in Kunshan, Jiangsu,
China, for the past two years. Adopting a vegan lifestyle is no trivial feat
anywhere in the world, but it feels even more challenging here compared to
Sweden, where I used to reside. Here's what I've learned about being a vegan in
China.

## Finding Vegan Food in Restaurants

### Recommended Restaurants

Restaurants that I recommend (see details below) include ---

* Jiangsu Province
    * Kunshan
        * [Zizai Ge/自在阁](https://surl.amap.com/hOhJ3w71iboQ) (vegetarian buffet)
        * [Ziran Feng/自然风](https://surl.amap.com/hYf3cozth1x) (vegetarian buffet)
        * [Shufeng Small Hotpot/蜀风小火锅](https://surl.amap.com/hUl5bwD13bzH) (hotpot)
        * [Xibei/西贝](https://surl.amap.com/hUQKRHJ1k8Nz) (north-western style)
        * [Kuafu Fried Skewers/夸父炸串](https://www.dianping.com/shop/H6YqznETyBZvv5Hs) (fried skewers)
        * [Yanhuo Makes Skewers/烟火成串](https://surl.amap.com/hKA0507KdDo) (BBQ skewers)
    * Suzhou
        * [Veggie Bar/兔蔬馆](https://surl.amap.com/hP2FKzNS8Mx) (vegetarian)
        * [Lotus Veg Cafe/水中莲](https://surl.amap.com/4dXihJ5u04z) (vegetarian)
        * [Sumanxiang/素满香](https://surl.amap.com/id0yTa3IaE0) (vegetarian buffet)
* Shanghai
    * [Duli/度粒](https://surl.amap.com/67EdeCj18cQo) (vegetarian)
* Sichuan Province
    * Chengdu
        * [Handou's Kitchen/憨豆厨房](https://surl.amap.com/hTfCiinfbGD) (vegan)

### Vegetarian Restaurants

Although Kunshan is a small city, it hosts two notable vegetarian buffets:
[Zizai Ge/自在阁](https://surl.amap.com/hOhJ3w71iboQ) and
[Ziran Feng/自然风](https://surl.amap.com/hYf3cozth1x).
A lunch buffet at Zizai Ge is priced at 38 RMB,
while Ziran Feng offers one for 25 RMB.
Additionally,
Zizai Ge provides a dinner buffet with a pay-as-you-wish pricing
model.

If you're open to exploring neighboring cities such as Suzhou and Shanghai,
you'll find an abundance of vegetarian restaurants. My personal favorites
include [Veggie Bar/兔蔬馆](https://surl.amap.com/hP2FKzNS8Mx)
and [Lotus Veg Cafe/水中莲](https://surl.amap.com/4dXihJ5u04z) in
Suzhou, as well as [Duli/度粒](https://surl.amap.com/67EdeCj18cQo)
in Shanghai. These dining options could add a delightful culinary twist to your
travels!

BTW, if you ever visit my hometown Chengdu, don't miss the opportunity to dine
at my favorite vegan restaurant in the city,
[Handou's Kitchen/憨豆厨房](https://surl.amap.com/hTfCiinfbGD).
It offers the best vegan Sichuan cuisine I've ever had.

A great way to discover vegetarian restaurants is to search for 素菜 (vegetarian
food) on [Dianping](https://www.dianping.com/) (大众点评), a Chinese website
similar to Yelp. Check out the results for
[Suzhou](https://www.dianping.com/search/keyword/6/0_%E7%B4%A0%E8%8F%9C) and
[Shanghai](https://www.dianping.com/search/keyword/1/0_%E7%B4%A0%E8%8F%9C) for a
wide variety of options.

### Noodles Restaurants

{{<figure src="suji-noodles.jpg" caption="Suji noodles (素鸡面)">}}

If you're in the mood for a quick bite, noodle shops can be an excellent choice.
The noodles are typically cooked in plain water and are therefore vegan. While
most of the toppings may not be vegan, there are usually a few vegan options
available, such as Veggie Chicken/素鸡, a type of firm-textured tofu.

Even if vegan options are not listed on the menu, don't hesitate to ask the
restaurant staff to prepare a bowl of veggie noodles. They are usually
accommodating, and this simple request can transform your dining experience. For
instance, during train travel, I used to be limited to a french fries-only diet.
Now, I simply visit a noodle shop at the station, request noodles without meat,
and am generally served exactly what I asked for.

### Hotpot Restaurants

{{<figure src="hotpot.jpg" caption="Hotpot (火锅)">}}

Hotpot (火锅) is a Chinese meal featuring a pot of broth simmering on a stove,
often an inductive stove in modern times. You place ingredients into the pot,
cook them for a few seconds to a few minutes, and then eat. Originally from my
home province of Sichuan, hotpot has spread globally, even to Canada and Europe.

The broth usually contains beef oil, but mushroom-based alternative is typically
available. As for ingredients, a wide variety of vegan options such as various
types of tofu and vegetables can be found.

My favourite hotpot restaurant in Kunshan is
[Shufeng Small Hotpot/蜀风小火锅](https://surl.amap.com/hUl5bwD13bzH).
It offers a hotpot buffet for about 40 RMB.
The unique aspect of this place is that you
get a small pot just for yourself,
unlike in standard hotpot restaurants,
and the food is delivered to you via a conveyor belt.

### North-Western Restaurants

{{<figure src="liangpi.jpg" caption="Liangpi (凉皮)">}}

Liangpi (凉皮) is a dish made of cold skin noodles crafted from wheat or rice
flour. Originating from north-western China, this specialty dish has now spread
throughout the entire country. Typically served with cucumber and purple cabbage
shreds, chili oil, vinegar, and soy sauce, Liangpi is almost always vegan. It's
a refreshing and flavorful option that can be found at north-western style
restaurants such as
[Xibei/西贝](https://surl.amap.com/hUQKRHJ1k8Nz).

### Breakfast Restaurants

{{<figure src="baozi.jpg" caption="Baozi (包子)">}}

Baozi (包子), or steamed buns with fillings, is a popular street food often
found in breakfast restaurants across China. Though most filings are meat-based,
most places offer a few vegan options, with fillings like vegetables and
mushrooms.  These delicious, fluffy buns are a wonderful choice for breakfast.

### Dumplings Restaurants

{{<figure src="dumplings.jpg" caption="Dumplings (饺子)">}}

Dumplings, a beloved staple in Chinese cuisine, are delicate pockets made from
dough, usually wheat, that are stuffed with various combinations of meat,
vegetables, and sometimes eggs. They can be boiled, steamed, or fried, offering
different textures and flavors. While vegetarian options are widely available,
it's essential for those following a strict vegan diet to be cautious, as
vegetarian dumplings in China usually contain eggs.

### Skewers Restaurants

{{<figure src="skewers.jpg" caption="BBQ Skwers (烤串)">}}

Chinese people enjoy skewers in a variety of ways,
including skewers grilled, fried, or boiled in hot sauce.
Usually, each skewer contains a single type of ingredient,
so if you choose skewers without meat,
you can enjoy a vegan skewer meal.
Some local fast-food places that offer
skewers include
[Kuafu Fried Skewers/夸父炸串](https://www.dianping.com/shop/H6YqznETyBZvv5Hs),
which specializes in fried options,
and [Yanhuo Makes Skewers/烟火成串](https://surl.amap.com/hKA0507KdDo),
known for their BBQ skewers.

### Other Restaurants

{{<figure src="mapo-tofu.jpg" caption="Mapo Tofu (麻婆豆腐)">}}

When dining at standard restaurants, you'll soon familiarize yourself with
common vegan dishes such as:

- Mapo Tofu (without ground pork) (素麻婆豆腐)
- Three Treasures of the Earth (地三鲜)
- Hot and Spicy Soup (麻辣烫)
- Dry Fried Beans (干煸四季豆)
- Garlic Broccoli (蒜蓉西蓝花)
- Sweet and Sour Cabbage (酸甜洋白菜)
- Sauteed Bok Choy (清炒小白菜)
- Chinese Scallion Pancakes (葱油饼)
- Shredded Potato Stir-Fry (土豆丝)

Please note, some dishes, such as Mapo Tofu, traditionally contain ground pork,
but it's often possible to request the chef to leave it out. Most restaurants
are accommodating in this regard.

## Cook Your Own Food

{{<figure src="vegan-cook-book.jpg" caption="Vegan cook book">}}

Another viable option, especially for those seeking control over ingredients and
flavors, is to cook for yourself. You can find an abundance of Chinese recipes
on websites like [Xiachufang](https://www.xiachufang.com/) (下厨房). I myself
have even shared a recipe for cooking the classic dish, Mapo Tofu, [here]({{<
ref "mapotofu.md" >}}).

More recently, I discovered a Korean vegan cookbook called "Yuanzhi Yuanwei
Jingxin Sushi" (原汁原味静心素食) this summer and have enjoyed several of the
recipes. This path allows for culinary exploration, creativity, and a mindful
connection with the food we eat.

## Order Vegan Meal Delivery

{{<figure src="bbq.jpg" caption="Vegan BBQ skewers">}}

Apps like Ele.me (饿了么) and Meituan (美团) have broadened the horizon for
those seeking vegetarian and vegan options in China. These platforms allow you
to order food deliveries from local restaurants, and although choices may be
limited, menus often do include a vegetarian section.
I often order BBQ skewers with vegetables and tofu.

## Buy Vegan Food Online

{{<figure src="burger.jpg" caption="Vegan Burger">}}

For those craving more specialized vegan delights, I've found that online
shopping sites such as Taobao or JD are a treasure trove. I personally recommend
exploring the plant-based products produced by the company "Whole Perfect Food"
(齐善食品), such as their smoked vegan sausages, known for their delightful
taste. I've enjoyed them myself!

As for Western-style vegan food, such as vegan burgers, there is Beyond Meat
burgers sold on JD. There's also a WeChat mini app called ButlerWhite that I've
used, which sells similar products like vegan burger, bread, spread, etc.

## Some Extra Tips

### Be Patient with People

{{<figure src="old-lady.jpg" caption="Understanding differing perspectives on veganism">}}

When I first returned to China and embraced veganism, my mother was perplexed,
as meat and seafood were once rare treats in her experience. However, my
consistent commitment to avoiding animal products led her to accept my choice,
and she even adopted a mostly vegan diet herself.

My advice to fellow vegans in China is to practice patience with those who may
not understand your choice. Stay true to your beliefs, and others will likely
respect your decision, even if they don't fully comprehend it.

### Remember Why You Became a Vegan

{{<figure src="chickens-in-a-battery-cage.jpg" caption="Navigating the challenges of veganism">}}

Before moving back to China, I optimistically believed that finding vegan food
would be relatively easy, given the absence of dairy in most traditional Chinese
dishes.  However, I soon discovered that the reality was more complex. Many
menus are dominated by meat and seafood, with only a handful of vegan options,
and even those may include eggs.

This challenge is accentuated when traveling on high-speed trains, where vegan
options are rarely available, often leaving fast-food french fries as the only
choice.

Despite these hurdles, it's essential to remember why you chose this path and
stay committed to your principles. Your reasons for being vegan can guide you
through the complexities and keep you grounded in your choices, even in a new
and different culinary landscape.

### Embrace Forgiveness and Keep Going

{{<figure src="rabbit-in-restaurant-02.jpg" caption="Every vegan meal is a win!">}}

Embracing veganism is a journey filled with challenges, especially in parts of
the world where it's not yet mainstream. Mistakes are inevitable, and it's
essential to remember that being vegan isn't about perfection; it's about
compassion for animals.

If you stumble along the way, don't be too hard on yourself. Acknowledge the
misstep, learn from it, and continue your journey with renewed determination.
Every effort, no matter how small, contributes to making the world a better
place. So celebrate your victories, like that plant-based lunch on Monday, and
keep moving forward with grace and understanding.
