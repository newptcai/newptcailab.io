---
date: "2021-02-11"
tags:
- 气候变化
title: 气候变化资源
---

在2020年的圣诞假期，我尝试让自己跟上气候变化的步伐。我所了解到的情况让我[深感忧虑](https://newptcai.github.io/what-have-i-done-in-2020-part-2-becoming-an-environmentalist.html)。情况比我想象的要严重得多。实际上，我决定回到中国工作的原因之一是避免每年飞行探望家人。

这个页面列出了让我意识到情况严峻的资源。希望它也能唤醒你。

## 书籍

如果你只有时间读一本关于气候变化的书，那么你应该读 [《我们的最后警告：六度气候紧急情况》](https://www.goodreads.com/book/show/51471435-our-final-warning)，作者是马克·林纳斯。它详细描述了不同变暖水平下的不同情景，并解释了我们今天的行动对人类延续的重要性。

![我们的最后警告](/assets/images/what-have-i-done-2020/final.jpg)

其他好书包括：

* [《热浪：在炙热星球上的生死挣扎》](https://www.goodreads.com/book/show/63251778-the-heat-will-kill-you-first)
* [《不可居住的地球：变暖后的生活》](https://www.goodreads.com/book/show/41552709-the-uninhabitable-earth)
* [《气候制造者：人类如何改变气候及其对地球生命的影响》](https://www.goodreads.com/book/show/48463.The_Weather_Makers)
* [《没有人微不足道》](https://www.goodreads.com/book/show/51764686-no-one-is-too-small-to-make-a-difference)
* [《第六次大灭绝：非自然的历史》](https://www.goodreads.com/book/show/17910054-the-sixth-extinction)

## 纪录片

我最新喜爱的气候变化纪录片是[《格蕾塔·桑伯格：改变世界的一年》](https://en.wikipedia.org/wiki/Greta_Thunberg:_A_Year_to_Change_the_World)。

* [BBC -- 气候变化：事实](https://www.bbc.co.uk/programmes/m00049b1)
* [BBC -- 灭绝：事实](https://www.bbc.co.uk/programmes/m000mn4n)
* [BBC -- Horizon -- 77亿人与生存之道](https://www.reddit.com/r/overpopulation/comments/eshih9/bbc_two_horizon_2020_chris_packham_77_billion/)
* [Netflix -- 大卫·爱登堡：地球上的一生](https://www.imdb.com/title/tt11989890/)
* [《2050终局》](https://www.endgame2050.com/)
* [BBC -- Horizon -- 我应该吃肉吗？如何养活地球](https://www.bbc.co.uk/programmes/b04fhbrt)
* [《牛鬼：可持续性的秘密》](https://www.cowspiracy.com/)

## 播客

* [Climate One Podcast](https://www.climateone.org/watch-and-listen/podcasts)
* [GIMLET -- 如何拯救地球](https://gimletmedia.com/shows/howtosaveaplanet)
* [BBC -- 气候问题](https://www.bbc.co.uk/programmes/w13xtvb6)

## 网站

* [SkepticalScience](https://skepticalscience.com/argument.php?f=taxonomy) -- 了解气候变化的科学证据。  
* 如果你想为气候变化做些事情，可以查看[GivingGreen](https://www.givinggreen.earth)、[这里](https://www.vox.com/future-perfect/2019/12/2/20976180/climate-change-best-charities-effective-philanthropy)和[这里](https://www.theatlantic.com/science/archive/2020/12/how-to-donate-to-fight-climate-change-effectively/617248/)。
