---
date: "2021-02-11"
tags:
- research
title: My Research
---

## Research Interest

{{<rawhtml>}}
<a style="color: black" href="https://doi.org/10/ggmxmf">
<img src="/assets/images/research/kademlia-digraph.png" class="heading"></img>
</a>
{{</rawhtml>}}

I am interested in probability and combinatorics, including randomized algorithms, random
graphs and random tree, in particular:

- [Galton-Watson trees](http://www.combinatorics.org/ojs/index.php/eljc/article/view/v25i3p40)
- [Binary search trees and split trees](http://drops.dagstuhl.de/opus/volltexte/2018/8908/)
- [Random directed graphs](https://onlinelibrary.wiley.com/doi/full/10.1002/rsa.20707)
- [Peer-to-peer computer networks](https://link.springer.com/chapter/10.1007%2F978-3-642-45030-3_66)
- [Graph coloring](https://onlinelibrary.wiley.com/doi/full/10.1002/rsa.20695)

I also like programming and using computer to experiment, prove, and
teach mathematics.

Occasionally I play with [special functions](https://arxiv.org/abs/1806.01122).

## Publications

You can find all my research papers on 
[Zotero](https://www.zotero.org/newptcai) (most up-to-date),
[Google Scholar](https://scholar.google.ca/citations?user=Zqh1PIEAAAAJ&hl=en) (slightly delayed), 
and
[arXiv](http://arxiv.org/a/cai_x_1).

Here are some of my open source software projects on  [GitHub](https://github.com/newptcai).

## My colleagues and friends

* [Cecilia Holmgren](http://katalog.uu.se/profile/?id=N5-824) 
* [Colin Desmarais](https://katalog.uu.se/profile/?id=N17-1633) 
- [Debleena Thacker](http://www2.math.uu.se/~debth221/) 
* [Fabian Burghart](https://katalog.uu.se/profile/?id=N18-1431) 
* [Fiona Skerman](http://www2.math.uu.se/~fiosk856/) 
* [Gabriel Berzunza Ojeda](http://www2.math.uu.se/~gabbe533/Gabo26.html) 
- [Guillem Perarnau](http://www-ma4.upc.edu/~guillem.perarnau/) 
- [Jan Volec](http://honza.ucw.cz/)
* [Johanna Strömberg](https://katalog.uu.se/profile/?id=N16-1207) 
- [Jonathan Noel](https://www.math.uvic.ca/~noelj/)
- [Jose Luis Lopez Garcia](https://www.unavarra.es/pdi?uid=2369)
- [Laura Eslava](http://sigma.iimas.unam.mx/laura/) 
- [Liana Yepremyan](https://scholar.google.com/citations?user=5HT4MmwAAAAJ&hl=en)
- [Lianna Hambardzumyan](https://www.cs.mcgill.ca/~lhamba/)
- [Luc Devroye](http://luc.devroye.org) 
* [Raazesh Sainudiin](https://lamastex.github.io/) 
* [Stephan Wagner](https://www.researchgate.net/profile/Stephan_Wagner) 
* [Svante Janson](http://www2.math.uu.se/~svante/papers/) 
- [Yelena Yuditsky](https://sites.google.com/view/yuditsky) 


## My talks

-  Rankings in directed configuration models with heavy tailed in-degrees.
   [Analysis of Algorithms 2021](https://www.math.aau.at/AofA2021/)
   [[slides]](/assets/doc/dmc-max.pdf)
-  Cutting resilient networks.  [[slides]](/assets/doc/cutting-slides.pdf)
-  Inversions in split trees and conditional Galton–Watson trees.  [Analysis of Algorithms
   2018](http://math.uu.se/aofa2018), [STAR Workshop on Random Graphs
   2018](http://www.math.ru.nl/~rkang/SWRG2018/).  [[slides]](/assets/doc/inversion-talk.pdf)
-  A study of large fringe and non-fringe subtrees in conditional Galton-Watson trees.  [Sweden
   Probabilistic Midwinter Meeting
   2017](http://www.math.umu.se/english/research/discrete-mathematics/workshop), [Meeting of the
   Catalan, Spanish, Swedish Math
   Societies](https://old.liu.se/mai/catspsw.math/abstracts/9-graphs-hypergraphs-and-set-systems/1.720559/9-Graphs-Hypergraphs-and-Set-Systems.pdf),
   [RS&A 2017](http://rsa2017.amu.edu.pl/abs/Cai.pdf).
   [[slides]](/assets/doc/fringe-subtree-slides.pdf)
-  The graph structure of a deterministic automaton chosen at random.  [RSA
   2015](http://rsa2015.amu.edu.pl/program).  [CanaDAM
   2015](https://canadam.math.ca/2015/program/abs/si2#xsc).  [[slides]](/assets/doc/rand-dfs.pdf)

   
## Conferences and workshops

The conferences and workshops which I have attended or plan to attend.

### 2021

* [Analysis of Algorithms 2021](https://www.math.aau.at/AofA2021/)
* [Entropy Compression and Related Methods](https://sparse-graphs.mimuw.edu.pl/doku.php?id=sessions:2021sessions:2021session2)
