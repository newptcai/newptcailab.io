---
ShowToc: true
date: "2021-02-11"
tags:
- teaching
title: My teachings
---

## Past Courses

Below are lists of courses which I taught or will teach.
Including, lecture slides, quizzes and exams.
You are free to use any material I created for teaching without any restrictions.
See links below.

💡 Materials for a course will only be made public once it has come to an
conclusion.

### Discrete Mathematics for Computer Science

* [2024-08](https://gitlab.com/compsci-203/2024-08)
* [2024-01](https://gitlab.com/compsci-203/2024-01)
* [2023-08](https://gitlab.com/compsci-203/2023-08)
* [2023-01](https://gitlab.com/compsci-203/2023-01)
* [2022-08](https://gitlab.com/compsci-203/2022-08)
* [2022-03](https://gitlab.com/compsci-203/2022-03/)
* [2021-08](https://gitlab.com/compsci-203/2021-08)

I also have a repository containing only lecture slides
(PDF and their LaTeX source code) here ---
[30 Lectures in Discrete Mathematics](https://gitlab.com/compsci-203/30-lectures-discrete-mathematics).

### Design and Analysis of Algorithms

* [2024-10](https://gitlab.com/compsci-308/2024-10)
* [2023-10](https://gitlab.com/compsci-308/2023-10)

### Linear Algebra

* [2024-03](https://gitlab.com/math-202/2024-03)
* [2023-03](https://gitlab.com/math-202/2023-03)
* [2022-01](https://gitlab.com/math-202/2022-01)

### Statistics

* [2022-10](https://gitlab.com/stats-301/2022-10)
* [2022-03](https://gitlab.com/stats-301/2022-03)

### Uppsala University (2016-2020)

For my teaching at Uppsala University, see [here](https://newptcai.github.io/pages/teaching.html).

## Advice

Each year, I am assigned to some new DKU (Duke Kunshan University) 
students as their academic advisor.
Here some advice I wrote for them:

1. [Welcome to DKU!]({{< ref "/blog/first-semester-at-dku/" >}})
2. [My own blog (general advice)]({{< ref path="/tags/advice/" >}})

Here are two websites I found very useful for any young student/people:

1. [80,000 hours](https://80000hours.org/career-reviews/) offers career advice.
   It has resources such as
    1. [Career Reviews](https://80000hours.org/career-reviews/) 
    2. [How to Be More Successful in Any Job](https://80000hours.org/career-guide/how-to-be-successful/)
    3. [Having a successful career with depression, anxiety, and imposter syndrome](https://80000hours.org/podcast/episodes/depression-anxiety-imposter-syndrome/)
2. [Effective Altruism (EA) Forum](https://forum.effectivealtruism.org/) mostly discusses [EA](https://www.effectivealtruism.org/). 
   But it also has many posts offering general advice, such as:
    1. [Many Undergrads Should Take Light Courseloads](https://forum.effectivealtruism.org/posts/hgiLaE3eL76ovcfdH/many-undergrads-should-take-light-courseloads)
    2. [Sleep: effective ways to improve it](https://forum.effectivealtruism.org/posts/Ei2uYbn2zrzmBjEsp/sleep-effective-ways-to-improve-it)
    3. [A guide to forming habits you'll keep](https://forum.effectivealtruism.org/posts/8aBiqvT9uDo56Cckx/esh-a-guide-to-forming-habits-you-ll-keep)
    4. [Stress - effective ways to reduce it](https://forum.effectivealtruism.org/posts/YKMav2pfmrpuEfqad/stress-effective-ways-to-reduce-it)

## FAQ

### Are there assignments in your courses?

Yes! The required reading and exercises in each lecture's slides are your assignments. 
While I don't collect or grade them,
they're crucial for your quiz preparation.

### Why are grades mostly based on quizzes?

With the rise of AI tools, short, 
closed-book quizzes are the best way to ensure students engage with the material.
Don't worry -- all questions come from the assignments in the slides.

### Can you increase the number of enrolled students if I'm on the waiting list?

Please contact registrar's office directly.
Starting from the fall semester of 2024,
all such decisions are made by the registrar's office.

### Isn't a weekly quiz too stressful?

Not if you've been working on the assignments! 
Also, we offer a lowest grade drop and retake opportunities to account for off days. 

### Are calculators allowed in quizzes?

No, but don't worry. 
Calculations are minimal,
and often an expression is sufficient instead of a numeric answer.

### Why are laptops discouraged in the classroom?

Research suggests screens can distract nearby students.
If you need a laptop, please sit in the third row or further back.

### Can I get special arrangements because I have an internship/I want to go to a conference/...?

Nope!  While internships and conferences are great,
they don’t quite qualify for special treatment.
Please refer to the undergraduate bulletin for more details.

However, if you're rescuing puppies from a burning building 
or performing emergency surgery on a mountain top, do let me know!

### Can you forgive me for not reading the email, ignoring instructions, and missing the deadline by two days?

Unfortunately, no.
You were given enough time to complete the task,
and meeting deadlines is an important part of your learning.
Developing good habits and avoiding procrastination
is just as crucial as mastering the course content.

### What is the workload for a course?

Student feedback varies significantly.
Some students spent less than 10 hours per week,
while others spent more than 25 hours per week.
This entirely depends on your mathematical maturity
and how much time you are willing to dedicate to it.
